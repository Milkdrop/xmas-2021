#!/usr/bin/env python3

FLAG = "X-MAS{th15_41Nt_VMPr0tEct_BUT_1t5_h0Ne5T_W0rK_9J9J0k09}"

KEY = "you_guys_enjoying_xmas_ctf_this_year?"

code = ''

for (i, char) in enumerate(FLAG):
    code += f'''
    PUSH({ord(char) ^ ord(KEY[i % len(KEY)])}),
    PUSH({ord(KEY[i % len(KEY)])}),
    XOR(),
    READCHR(),
    CMP(),
    PUTCHR(),
    '''
code += '''
STOP()
'''

print(code)
