#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// X-MAS{th15_41Nt_VMPr0tEct_BUT_1t5_h0Ne5T_W0rK_9J9J0k09}

// PUSH [enc-byte]
// PUSH [key-byte]
// XOR
// READCHR
// CMP
// PUTCHR
// STOP

#define PUSH(imm) 0x10, imm
#define XOR() 0x11
#define READCHR() 0x12
#define CMP() 0x13
#define PUTCHR() 0x14
#define STOP() 0xff

const uint8_t code[1024] = {PUSH(33),  PUSH(121), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(66),  PUSH(111), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(56),  PUSH(117), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(30),  PUSH(95),  XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(52),  PUSH(103), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(14),  PUSH(117), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(13),  PUSH(121), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(27),  PUSH(115), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(110), PUSH(95),  XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(80),  PUSH(101), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(49),  PUSH(110), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(94),  PUSH(106), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(94),  PUSH(111), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(55),  PUSH(121), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(29),  PUSH(105), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(49),  PUSH(110), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(49),  PUSH(103), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(18),  PUSH(95),  XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(40),  PUSH(120), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(31),  PUSH(109), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(81),  PUSH(97),  XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(7),   PUSH(115), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(26),  PUSH(95),  XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(0),   PUSH(99),  XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(0),   PUSH(116), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(57),  PUSH(102), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(29),  PUSH(95),  XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(33),  PUSH(116), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(60),  PUSH(104), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(54),  PUSH(105), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(66),  PUSH(115), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(43),  PUSH(95),  XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(76),  PUSH(121), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(58),  PUSH(101), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(9),   PUSH(97),  XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(66),  PUSH(114), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(113), PUSH(63),  XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(28),  PUSH(121), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(90),  PUSH(111), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(33),  PUSH(117), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(0),   PUSH(95),  XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(48),  PUSH(103), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(69),  PUSH(117), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(11),  PUSH(121), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(56),  PUSH(115), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(0),   PUSH(95),  XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(92),  PUSH(101), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(36),  PUSH(110), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(83),  PUSH(106), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(37),  PUSH(111), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(73),  PUSH(121), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(2),   PUSH(105), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(94),  PUSH(110), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(94),  PUSH(103), XOR(), READCHR(), CMP(), PUTCHR(),

                            PUSH(34),  PUSH(95),  XOR(), READCHR(), CMP(), PUTCHR(),

                            STOP()};

typedef struct stack_vm {
    uint16_t pc;
    uint16_t sp;
    uint8_t mem[1024];
    uint8_t stack[1024];
} stack_vm_t;

uint8_t data_pop(stack_vm_t* vm) {
    return vm->stack[vm->sp--];
}

void data_push(stack_vm_t* vm, uint8_t value) {
    vm->stack[++vm->sp] = value;
}

void read_chr(stack_vm_t* vm) {
    uint8_t chr;
    scanf("%c", &chr);
    data_push(vm, chr);
}

void put_chr(stack_vm_t* vm) {
    uint8_t chr = data_pop(vm);
    printf("%c", chr);
}

void exor(stack_vm_t* vm) {
    uint8_t value = data_pop(vm) ^ data_pop(vm);
    data_push(vm, value);
}

void cmp(stack_vm_t* vm) {
    uint8_t value = data_pop(vm) == data_pop(vm) ? '1' : '0';
    data_push(vm, value);
}

void run_vm(stack_vm_t* vm) {
    while (1) {
        uint8_t instr = vm->mem[vm->pc++];

        switch (instr) {
        case 0x10: {
            uint8_t imm = vm->mem[vm->pc++];
            data_push(vm, imm);
        } break;
        case 0x11: {
            exor(vm);
        } break;
        case 0x12: {
            read_chr(vm);
        } break;
        case 0x13: {
            cmp(vm);
        } break;
        case 0x14: {
            put_chr(vm);
        } break;
        case 0xff: {
            goto end;
        }
        }
    }
end:;
}

int main() {
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stdin, NULL, _IONBF, 0);
    stack_vm_t vm = {0};

    memcpy(vm.mem, code, sizeof(vm.mem));

    run_vm(&vm);

    return 0;
}
