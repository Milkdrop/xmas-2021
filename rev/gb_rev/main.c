#include <gb/gb.h>
#include <stdint.h>
#include <stdio.h>

unsigned char bgdata[32];
unsigned char tilemap[360];

const unsigned char cursor[16] = {
  /* Tile 0x00 */
  0xFF,0xFF,0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x81,0xFF,0xFF,
};

uint16_t target_hashes[18] = {26680,26804,17376,52229,5540,30872,34706,6786,1050,33308,44290,49792,58402,33880,49568,27648,4136,4145};
uint16_t calculated_hashes[18];

uint8_t order_matrix[18 * 16] = {
    0, 12, 14, 11, 10, 5, 13, 3, 4, 6, 1, 7, 2, 8, 9, 15,
    7, 1, 11, 2, 6, 12, 13, 4, 15, 8, 14, 5, 0, 9, 10, 3,
    15, 7, 6, 1, 5, 9, 3, 10, 13, 2, 0, 12, 8, 4, 11, 14,
    10, 4, 1, 3, 0, 2, 15, 13, 5, 9, 12, 6, 8, 7, 11, 14,
    7, 6, 12, 5, 9, 8, 1, 11, 2, 0, 10, 15, 14, 3, 4, 13,
    6, 5, 0, 7, 11, 9, 15, 8, 1, 2, 13, 10, 12, 14, 3, 4,
    15, 12, 0, 7, 3, 9, 5, 8, 4, 10, 11, 1, 2, 6, 13, 14,
    3, 11, 0, 7, 9, 5, 8, 2, 13, 12, 10, 6, 14, 1, 15, 4,
    5, 1, 7, 14, 12, 4, 10, 2, 6, 9, 11, 13, 15, 3, 8, 0,
    7, 15, 14, 8, 5, 0, 3, 1, 10, 11, 13, 4, 6, 9, 12, 2,
    10, 4, 12, 0, 15, 11, 6, 7, 14, 2, 13, 3, 8, 9, 1, 5,
    0, 7, 11, 8, 15, 6, 9, 12, 1, 14, 2, 10, 5, 4, 13, 3,
    6, 3, 0, 11, 1, 12, 2, 10, 8, 7, 13, 15, 9, 14, 5, 4,
    7, 1, 8, 13, 12, 3, 5, 10, 15, 9, 4, 0, 2, 6, 11, 14,
    9, 5, 15, 2, 3, 13, 10, 12, 4, 8, 1, 7, 6, 0, 14, 11,
    0, 1, 8, 7, 10, 15, 14, 3, 6, 2, 13, 5, 4, 12, 11, 9,
    15, 5, 10, 6, 9, 12, 11, 1, 8, 3, 4, 13, 7, 2, 0, 14,
    11, 15, 2, 5, 10, 9, 1, 6, 13, 4, 14, 0, 8, 3, 7, 12
};

uint8_t random_guys[18 * 16] = {
    15, 0, 5, 8, 11, 1, 9, 4, 13, 3, 14, 12, 6, 2, 7, 10,
    15, 3, 4, 6, 8, 13, 2, 9, 14, 12, 10, 11, 0, 5, 1, 7,
    9, 1, 0, 14, 12, 3, 10, 6, 5, 11, 13, 8, 2, 15, 7, 4,
    15, 1, 7, 11, 3, 14, 5, 0, 6, 10, 13, 4, 2, 12, 9, 8,
    14, 9, 11, 1, 15, 13, 3, 2, 5, 8, 12, 10, 0, 4, 7, 6,
    4, 3, 8, 13, 10, 14, 12, 11, 9, 15, 6, 1, 7, 2, 0, 5,
    1, 0, 5, 13, 8, 9, 12, 4, 3, 11, 6, 10, 15, 7, 2, 14,
    1, 0, 10, 6, 15, 14, 12, 9, 7, 4, 11, 8, 3, 2, 13, 5,
    2, 6, 3, 15, 0, 11, 14, 13, 8, 10, 1, 4, 9, 12, 5, 7,
    7, 1, 5, 11, 9, 6, 12, 3, 13, 4, 14, 15, 8, 0, 2, 10,
    4, 12, 1, 8, 10, 13, 15, 6, 5, 9, 3, 2, 7, 14, 0, 11,
    3, 5, 10, 11, 9, 15, 12, 13, 0, 1, 7, 6, 8, 2, 14, 4,
    7, 1, 15, 11, 4, 13, 10, 0, 8, 6, 3, 12, 14, 2, 5, 9,
    14, 1, 7, 8, 9, 3, 2, 11, 13, 12, 10, 5, 0, 4, 15, 6,
    10, 4, 2, 1, 15, 14, 5, 9, 12, 3, 0, 6, 11, 13, 8, 7,
    6, 9, 15, 3, 8, 10, 13, 4, 12, 5, 7, 2, 0, 1, 11, 14,
    12, 2, 6, 8, 1, 3, 10, 0, 15, 9, 13, 7, 4, 5, 11, 14,
    12, 7, 1, 5, 15, 11, 9, 13, 0, 2, 10, 14, 3, 8, 6, 4
};

const unsigned char flag[] = "\x6a\xb0\xaf\xcd\xcd\xc7\x85\x91\xa1\x87\x29\xad\xb9\xfa\x82\x95\xf7\xa7\xd4\xeb\xc3\xaf\xca\xd3\xc9\xd6\x97\xd2\xa3\xb5\x83\xc9\x34\xe3\xf1\xe0";
unsigned char flag_decoded[37];

void main() {
    uint16_t i;
    for (i = 0; i < 360; i++) {
        tilemap[i] = 0;
    }

    for (i = 16; i < 32; i++) {
        bgdata[i] = 0xFF;
    }

    set_bkg_data(0, 2, bgdata);        // load our sprite into the background set
    set_bkg_tiles(0, 0, 20, 18, tilemap);    // Set the background to the whole map we defined above
    SHOW_BKG;                    // Show the background
    
    SPRITES_8x8;
    set_sprite_data(0, 1, cursor);
    set_sprite_tile(0, 0);
    move_sprite(0, 8, 16);
    SHOW_SPRITES;

    DISPLAY_ON;                    // Turn on the display
    
    uint8_t posx = 0;
    uint8_t posy = 0;
    uint8_t joy = 0;
    uint8_t take_input = 1;
    
    while(1) {
        joy = joypad();
    
        if(joy == 0) {
            take_input = 1;
        } else if (take_input == 1) {
            take_input = 0;

            if(joy & J_UP) posy--;
            if(joy & J_DOWN) posy++;
            if(joy & J_LEFT) posx--;
            if(joy & J_RIGHT) posx++;

            if (posx == 20) posx = 0;
            if (posx == 255) posx = 19;
            if (posy == 18) posy = 0;
            if (posy == 255) posy = 17;

            move_sprite(0, 8 + posx * 8, 16 + posy * 8);
            SHOW_SPRITES;

            if(joy & J_A) {
                uint16_t index = (uint16_t) (posy * 20) + posx;
                tilemap[index] = 1;

                set_bkg_data(0, 2, bgdata);
                set_bkg_tiles(0, 0, 20, 18, tilemap);
                SHOW_BKG;
            }

            if(joy & J_B) {
                uint16_t index = (uint16_t) (posy * 20) + posx;
                tilemap[index] = 0;
                
                set_bkg_data(0, 2, bgdata);
                set_bkg_tiles(0, 0, 20, 18, tilemap);
                SHOW_BKG;
            }

            if (joy & J_START) {
                uint16_t hash = 0;
                uint16_t addition = 1;
                uint8_t k = 1;

                printf("\nLET ME SEE...\n\n\n");
                
                for (i = 1; i < 7; i++) {
                    addition = 1;
                    for (k = 1; k < 8; k++) {
                        printf("%d %d ", random_guys[18 * 16 - 1 - ((uint16_t) (i - 1) * 16 + k - 1)], random_guys[18 * 16 - 1 - ((uint16_t) (i - 1) * 16 + k - 1)]);
                        hash += tilemap[(uint16_t) k * 20 + i] * addition;
                        addition <<= 1;
                    }
                }

                uint8_t correct = 1;
                for (i = 1; i < 19; i++) {
                    hash = 0;
                    addition = 1;

                    for (k = 1; k < 17; k++) {
                        uint16_t index = (uint16_t) (i - 1) * 16 + k - 1;
                        printf("%d %d ",random_guys[18 * 16 - 1 - index], random_guys[18 * 16 - 1 - index]);
                        hash += (uint16_t) tilemap[((uint16_t) order_matrix[(uint16_t) (i - 1) * 16 + k - 1] + 1) * 20 + i] * addition;
                        addition <<= 1;
                    }

                    if (target_hashes[i - 1] != hash) {
                        printf("\n\nI DON'T LIKE THIS.\n");
                        correct = 0;
                        break;
                    }

                    calculated_hashes[i - 1] = hash;
                }

                if (correct) {
                    printf("\n\nI LOVE IT!! YOUR FLAG IS:\n");

                    uint8_t flag_i = 0;
                    for (i = 1; i < 19; i++) {
                        for (k = 1; k < 10; k += 8) {
                            uint8_t num = 0;
                            addition = 1;
                            for (uint8_t j = 0; j < 8; j++) {
                                num += (uint16_t) addition * tilemap[(k + j) * 20 + i];
                                addition <<= 1;
                            }

                            flag_decoded[flag_i] = flag[flag_i] ^ 146 ^ num;
                            flag_i += 1;
                        }
                    }

                    flag_decoded[36] = 0;

                    printf("%s\n", flag_decoded);
                }

                break;
            }
        }
    }
}