#include "utils.h"
#include <assert.h>
#include <chrono>
#include <functional>
#include <iostream>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/ptrace.h>
#include <sys/reg.h>
#include <sys/signal.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/user.h>
#include <sys/wait.h>
#include <thread>
#include <unistd.h>
#include <vector>
#include <x86intrin.h>

#include "./checks/marker.h"

#include "./checks/check1.h"
#include "./checks/check1.key.h"

#include "./checks/check2.h"
#include "./checks/check2.key.h"

#include "./checks/check4.h"
#include "./checks/check4.key.h"

#include "./checks/check12.h"
#include "./checks/check12.key.h"

#include "./checks/check13.h"
#include "./checks/check13.key.h"

#include "./checks/check14.h"
#include "./checks/check14.key.h"

#include "./checks/check15.h"
#include "./checks/check15.key.h"

#include "./checks/check16.h"
#include "./checks/check16.key.h"

#include "./checks/check17.h"
#include "./checks/check17.key.h"

#include "./checks/check18.h"
#include "./checks/check18.key.h"

#include "./checks/check19.h"
#include "./checks/check19.key.h"

#include "./checks/check20.h"
#include "./checks/check20.key.h"

#include "./checks/check21.h"
#include "./checks/check21.key.h"

#include "./checks/check22.h"
#include "./checks/check22.key.h"

#include "./checks/check23.h"
#include "./checks/check23.key.h"

#include "./checks/check24.h"
#include "./checks/check24.key.h"

#include "./checks/check25.h"
#include "./checks/check25.key.h"

#include "./checks/check26.h"
#include "./checks/check26.key.h"

#include "./checks/check27.h"
#include "./checks/check27.key.h"

#include "./checks/check28.h"
#include "./checks/check28.key.h"

#include "./checks/check29.h"
#include "./checks/check29.key.h"

#include "./checks/check30.h"
#include "./checks/check30.key.h"

#include "./checks/check31.h"
#include "./checks/check31.key.h"

#include "./checks/check32.h"
#include "./checks/check32.key.h"

#include "./checks/check33.h"
#include "./checks/check33.key.h"

#include "./checks/check34.h"
#include "./checks/check34.key.h"

#include "./checks/check35.h"
#include "./checks/check35.key.h"

#include "./checks/check36.h"
#include "./checks/check36.key.h"

#include "./checks/check37.h"
#include "./checks/check37.key.h"

#include "./checks/check38.h"
#include "./checks/check38.key.h"

#include "./checks/check39.h"
#include "./checks/check39.key.h"

#include "./checks/check40.h"
#include "./checks/check40.key.h"

#include "./checks/check41.h"
#include "./checks/check41.key.h"

#include "./checks/check42.h"
#include "./checks/check42.key.h"

#include "./checks/check43.h"
#include "./checks/check43.key.h"

#include "./checks/check44.h"
#include "./checks/check44.key.h"

#include "./checks/check45.h"
#include "./checks/check45.key.h"

#include "./checks/check46.h"
#include "./checks/check46.key.h"

#include "./checks/check47.h"
#include "./checks/check47.key.h"

volatile uint64_t syscall_scramble_table[512];

inline static __attribute__((always_inline)) void* mememmap(const void* addr, uint64_t length,
                                                            uint64_t prot, uint64_t flags,
                                                            uint64_t fd, const void* offset) {
    void* ret;

    uint64_t syscall = syscall_scramble_table[SYS_mmap];
    asm volatile inline(
        ".intel_syntax noprefix;"
        "mov rdx, %[prot];"
        "mov r10, %[flags];"
        "mov r8, %[fd];"
        "mov r9, %[offset];"
        "syscall;"
        : [ret] "=A"(ret)
        : [addr] "D"(addr), [length] "S"(length), [prot] "m"(prot), [syscall] "A"(syscall),
          [flags] "m"(flags), [fd] "m"(fd), [offset] "m"(offset)
        : "memory");
    return ret;
}

inline static __attribute__((always_inline)) uint64_t mememunmap(const void* addr,
                                                                 uint64_t length) {
    uint64_t ret;

    uint64_t syscall = syscall_scramble_table[SYS_munmap];

    asm volatile inline(".intel_syntax noprefix;"
                        "syscall;"
                        : [ret] "=A"(ret)
                        : [addr] "D"(addr), [length] "S"(length), [syscall] "A"(syscall)
                        : "memory");
    return ret;
}

inline static __attribute__((always_inline)) uint64_t mememprotect(const void* addr, uint64_t len,
                                                                   uint64_t prot) {
    uint64_t ret;

    uint64_t syscall = syscall_scramble_table[SYS_mprotect];

    asm volatile inline(".intel_syntax noprefix;"
                        "mov rax, 10;"
                        "mov rdx, %[prot];"
                        "syscall"
                        : [ret] "=A"(ret)
                        : [addr] "D"(addr), [len] "S"(len), [prot] "m"(prot), [syscall] "A"(syscall)
                        : "memory");
    return ret;
}

volatile int x = 0;

template <typename T> class mmap_allocator : public std::allocator<T> {
public:
    typedef size_t size_type;
    typedef T* pointer;
    typedef const T* const_pointer;

    template <typename _Tp1> struct rebind { typedef mmap_allocator<_Tp1> other; };

    pointer allocate(size_type n, const void* hint = 0) {

        void* allocd = mememmap(hint, n * sizeof(T), PROT_READ | PROT_WRITE,
                                MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
        if (allocd == MAP_FAILED) {
            return (pointer) nullptr;
        } else {
            return (pointer)allocd;
        }
    }

    void deallocate(pointer p, size_type n) {
        mememunmap(p, n * sizeof(T));
    }

    mmap_allocator() throw() : std::allocator<T>() {
    }
    mmap_allocator(const mmap_allocator& a) throw() : std::allocator<T>(a) {
    }
    template <class U> mmap_allocator(const mmap_allocator<U>& a) throw() : std::allocator<T>(a) {
    }
    ~mmap_allocator() throw() {
    }
};

template <typename T> using Vec = std::vector<T, mmap_allocator<T>>;

#define CCALL(code) code

typedef uint64_t (*shellcodeFn)(const char*, void*);

sig_atomic_t* flagOkPriv;

struct ShellcodeChunk;

thread_local ShellcodeChunk* instance = nullptr;

struct ShellcodeChunk {
    ShellcodeChunk(const ShellcodeChunk& other) = delete;
    ShellcodeChunk(ShellcodeChunk&& other) = default;
    Vec<uint8_t> key;
    bool wasExecuted = false;
    void* _backingPtr;
    void* _privateMem;
    uint64_t retValue;

    static const uint64_t SHELLCODE_SIZE = 65536 * 4;

    ShellcodeChunk(Vec<uint8_t> shellcode, Vec<uint8_t> key) {
        assert(shellcode.size() < SHELLCODE_SIZE && "Shellcode too schlong");
        _backingPtr = mememmap(NULL, SHELLCODE_SIZE, PROT_READ | PROT_WRITE,
                               MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
        if (_backingPtr == MAP_FAILED) {
            fprintf(stderr, "Failed to alloc memory\n");
            exit(-1);
        }

        _privateMem = mememmap(NULL, SHELLCODE_SIZE, PROT_READ | PROT_WRITE,
                               MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
        if (_privateMem == MAP_FAILED) {
            exit(-1);
        }

        memcpy(_backingPtr, &shellcode[0], shellcode.size());
        memcpy(_privateMem, &flagOkPriv, 8);
        CCALL(mememprotect(_backingPtr, SHELLCODE_SIZE, PROT_NONE));

        this->key = key;
    }

    void execute(const char* userInput) {
        CCALL(mememprotect(_backingPtr, SHELLCODE_SIZE, PROT_READ | PROT_EXEC));
        if (!wasExecuted) {
            instance = this;
            setup_context();
            retValue = ((shellcodeFn)(_backingPtr))(userInput, _privateMem);
            CCALL(mememunmap(_backingPtr, SHELLCODE_SIZE));
            CCALL(mememunmap(_privateMem, SHELLCODE_SIZE));
            wasExecuted = true;
            instance = nullptr;
        }
    }

    static void sigsegv_handler(int, siginfo_t* info, void* ucontext) {
        if (nullptr == instance) {
        }

        ucontext_t* ctx = (ucontext_t*)ucontext;

        char __attribute__((aligned(32))) urmom[256];
        char __attribute__((aligned(32))) key[instance->key.size()];

        [&, i = 0]() mutable {
            for (i = 0; i < instance->key.size(); i++) {
                key[i] = instance->key[i];
                instance->key[i] = 0;
            }
        }();

        memerc4_array(urmom);

        memerc4_ksa(urmom, key, instance->key.size());

        uint64_t a = 0;
        uint64_t b = 0;

        char* mem = (*(char**)(ctx->uc_mcontext.gregs[REG_RSP])) + 3;
        const void* page_addr = (void*)((uint64_t)mem & ~((uint64_t)0xfff));

        CCALL(mememprotect(page_addr, SHELLCODE_SIZE, PROT_READ | PROT_WRITE));
        for (int i = 0; i < SHELLCODE_SIZE - marker_shellcode_size; i++) {
            mem[i] = mem[i] ^ memerc4_prga(urmom, &a, &b);
        }
        CCALL(mememprotect(page_addr, SHELLCODE_SIZE, PROT_READ | PROT_EXEC));

        ctx->uc_mcontext.gregs[REG_RSP] += 8;
        ctx->uc_mcontext.gregs[REG_RIP] = (uint64_t)mem;
    }

    void setup_context() {
        struct sigaction sigsegvAction;
        memset(&sigsegvAction, 0, sizeof(sigsegvAction));

        sigemptyset(&sigsegvAction.sa_mask);
        sigsegvAction.sa_flags = SA_SIGINFO;
        sigsegvAction.sa_sigaction = sigsegv_handler;
        CCALL(sigaction(SIGSEGV, &sigsegvAction, nullptr));
    }

    void clear_context() {
        CCALL(sigaction(SIGSEGV, nullptr, nullptr));
    }
};

#ifdef __x86_64__
#define SC_NUMBER (8 * ORIG_RAX)
#define SC_RETCODE (8 * RAX)
#else
#define SC_NUMBER (4 * ORIG_EAX)
#define SC_RETCODE (4 * EAX)
#endif

void setup_scramble() {
    uint64_t mt[N];
    uint64_t mti = N + 1;

    sgenrand(__builtin_ia32_rdtsc(), mt, &mti);

    for (int i = 0; i < 512; i++) {
        syscall_scramble_table[i] = genrand(mt, &mti) % 69420 + 1337;
    }

    pid_t pid = fork();

    if (pid == 0) {
        ptrace(PTRACE_TRACEME, 0, 0, 0);
    } else {
        int status;
        ptrace(PTRACE_SETOPTIONS, pid, 0, PTRACE_O_EXITKILL);
        while (1) {
            waitpid(pid, &status, 0);

            if (WIFEXITED(status)) {
                exit(0);
            }

            if (WIFSIGNALED(status)) {
                exit(0);
            }

            if (!WIFSTOPPED(status)) {
                exit(0);
            }

            if (WSTOPSIG(status) == SIGTRAP) {
                struct user_regs_struct regs;
                ptrace(PTRACE_GETREGS, pid, 0, &regs);

                if (regs.orig_rax >= 1337) {
                    for (int i = 0; i < 512; i++) {
                        if (syscall_scramble_table[i] == regs.orig_rax) {
                            regs.orig_rax = i;
                            ptrace(PTRACE_SETREGS, pid, 0, &regs);
                        }
                    }
                }
            } else {
                if (WSTOPSIG(status) == 11) {
                    exit(0);
                }
            }
            fflush(stdout);

            ptrace(PTRACE_SYSCALL, pid, NULL, NULL);
        }
    }
}

int main() {
    setup_scramble();
    for (int i = 0; i < 1000; i++) {
        raise(SIGCONT);
    }

    sig_atomic_t flagOz = 1;
    flagOkPriv = &flagOz;
    bool flagOk = true;
    Vec<ShellcodeChunk> chunks{};
    {
        Vec<uint8_t> check = Vec<uint8_t>{check1_enc, check1_enc + check1_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check1_enc_key, check1_enc_key + check1_enc_key_size};

        chunks.emplace_back(check, key);
    }
    {
        Vec<uint8_t> check = Vec<uint8_t>{check2_enc, check2_enc + check2_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check2_enc_key, check2_enc_key + check2_enc_key_size};

        chunks.emplace_back(check, key);
    }
    {
        Vec<uint8_t> check = Vec<uint8_t>{check4_enc, check4_enc + check4_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check4_enc_key, check4_enc_key + check4_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check12_enc, check12_enc + check12_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check12_enc_key, check12_enc_key + check12_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check13_enc, check13_enc + check13_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check13_enc_key, check13_enc_key + check13_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check14_enc, check14_enc + check14_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check14_enc_key, check14_enc_key + check14_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check15_enc, check15_enc + check15_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check15_enc_key, check15_enc_key + check15_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check16_enc, check16_enc + check16_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check16_enc_key, check16_enc_key + check16_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check17_enc, check17_enc + check17_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check17_enc_key, check17_enc_key + check17_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check18_enc, check18_enc + check18_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check18_enc_key, check18_enc_key + check18_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check19_enc, check19_enc + check19_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check19_enc_key, check19_enc_key + check19_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check20_enc, check20_enc + check20_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check20_enc_key, check20_enc_key + check20_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check21_enc, check21_enc + check21_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check21_enc_key, check21_enc_key + check21_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check22_enc, check22_enc + check22_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check22_enc_key, check22_enc_key + check22_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check23_enc, check23_enc + check23_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check23_enc_key, check23_enc_key + check23_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check24_enc, check24_enc + check24_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check24_enc_key, check24_enc_key + check24_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check25_enc, check25_enc + check25_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check25_enc_key, check25_enc_key + check25_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check26_enc, check26_enc + check26_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check26_enc_key, check26_enc_key + check26_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check27_enc, check27_enc + check27_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check27_enc_key, check27_enc_key + check27_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check28_enc, check28_enc + check28_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check28_enc_key, check28_enc_key + check28_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check29_enc, check29_enc + check29_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check29_enc_key, check29_enc_key + check29_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check30_enc, check30_enc + check30_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check30_enc_key, check30_enc_key + check30_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check31_enc, check31_enc + check31_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check31_enc_key, check31_enc_key + check31_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check32_enc, check32_enc + check32_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check32_enc_key, check32_enc_key + check32_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check33_enc, check33_enc + check33_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check33_enc_key, check33_enc_key + check33_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check34_enc, check34_enc + check34_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check34_enc_key, check34_enc_key + check34_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check35_enc, check35_enc + check35_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check35_enc_key, check35_enc_key + check35_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check36_enc, check36_enc + check36_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check36_enc_key, check36_enc_key + check36_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check37_enc, check37_enc + check37_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check37_enc_key, check37_enc_key + check37_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check38_enc, check38_enc + check38_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check38_enc_key, check38_enc_key + check38_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check39_enc, check39_enc + check39_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check39_enc_key, check39_enc_key + check39_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check40_enc, check40_enc + check40_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check40_enc_key, check40_enc_key + check40_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check41_enc, check41_enc + check41_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check41_enc_key, check41_enc_key + check41_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check42_enc, check42_enc + check42_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check42_enc_key, check42_enc_key + check42_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check43_enc, check43_enc + check43_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check43_enc_key, check43_enc_key + check43_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check44_enc, check44_enc + check44_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check44_enc_key, check44_enc_key + check44_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check45_enc, check45_enc + check45_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check45_enc_key, check45_enc_key + check45_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check46_enc, check46_enc + check46_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check46_enc_key, check46_enc_key + check46_enc_key_size};

        chunks.emplace_back(check, key);
    }

    {
        Vec<uint8_t> check = Vec<uint8_t>{check47_enc, check47_enc + check47_enc_size};
        Vec<uint8_t> key = Vec<uint8_t>{check47_enc_key, check47_enc_key + check47_enc_key_size};

        chunks.emplace_back(check, key);
    }

    printf("Say the magic word!\n");

    char __attribute__((aligned(32))) input[512];

    scanf("%s", input);

    std::vector<std::thread, mmap_allocator<std::thread>> threads{};

    for (int i = 0; i < chunks.size(); i++) {
        threads.emplace_back(&ShellcodeChunk::execute, static_cast<ShellcodeChunk*>(&chunks[i]),
                             input);
    }

    for (int i = 0; i < threads.size(); i++) {
        threads[i].join();
    }

    for (int i = 0; i < chunks.size(); i++) {
        flagOk &= chunks[i].retValue;
    }

    if (flagOz) {
        std::cout << "Congratulations you unlocked the chest!" << std::endl;
        std::cout << "But it seems like the journey itself was the threasure." << std::endl;
    } else {
        if (flagOk) {
        std::cout << "Congratulations you unlocked the chest!" << std::endl;
        std::cout << "The secret is" << std::endl;
        } else {
            std::cout << "Bobi said that lol" << std::endl;
        }
    }

    return 0;
}
