
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0x67c0eabe, 0x1ae559c7, 0x8f586b84, 0x420e099f, 0x78cb0dbf, 0x6a290992, 0x4d0d92c, 0x89851044, 0x5d4bc167, 0x5e933251, 0x518096c4, 0xdce10ae5, 0x1943d37f, 0x253e428b, 0x354dfdae, 0xc11f3b83, 0xf6270a06, 0xe8d9ad78, 0x2a74b5d5, 0x966299e6, 0x576cd7d3, 0xd5734749, 0x4dbe6a7c, 0xe721f9d, 0xb57e4467, 0xa06c5186, 0x4c7e8d5c, 0xbfe5dd15, 0x77b5ac4c, 0xe52bb9ed, 0xfd26c510, 0x8889d225};
    sgenrand((unsigned char)userInput[27], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

