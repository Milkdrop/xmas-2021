
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0xcfc11ae7, 0xff6a90bf, 0x3d879d65, 0x5007e4d7, 0x2367e4c6, 0xc12aa7de, 0xcd6d04c6, 0x8626fc65, 0xcfb39a6d, 0x78d77e97, 0xd161b788, 0xf886a911, 0x87aac3f8, 0xf2800ade, 0xa4cea17c, 0xc17e2f52, 0x95c3e452, 0x7ccd1e7a, 0xbe3dc0d7, 0x2cab5670, 0x7e61f0bd, 0x8c0d4335, 0xca54075d, 0xb20d96b3, 0x93a36440, 0xc5326d7f, 0x7a721f61, 0x72d67093, 0x755f1091, 0x8dab81c9, 0xb0881be4, 0xa4e76b3d};
    sgenrand((unsigned char)userInput[45], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

