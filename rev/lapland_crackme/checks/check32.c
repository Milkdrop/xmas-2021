
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0xd0a0ed15, 0xe56ba4fe, 0x8bf0f4e, 0x59dc4ec1, 0xfbe80af4, 0x67778979, 0xcf76d7dd, 0x46d20a96, 0x4b8c02de, 0x39dfac91, 0x2b81e1ec, 0x22939fa5, 0x1fee628a, 0x4d62dc80, 0x4939b356, 0xddc73493, 0x11addeb7, 0x777f2bc7, 0xad3ff4dc, 0xbab73f85, 0x32287512, 0x41a030f5, 0x9ae50717, 0x2c4d6c06, 0x557e222f, 0x6916b32b, 0xd76e47f6, 0xdc72002a, 0x13d05f6d, 0xd56cbb34, 0xc6043bea, 0xaf283ca3};
    sgenrand((unsigned char)userInput[32], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

