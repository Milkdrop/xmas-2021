
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0x6b0509f7, 0x3f659b2e, 0xd88cc807, 0x6c3a6ce5, 0xf6f2882b, 0xad2c6d07, 0xe472340e, 0x4aa41359, 0x80df645a, 0xc2abbd2b, 0xac6e7374, 0x924c6b1e, 0xc6e7505a, 0xda62cb1c, 0x888c8150, 0x8a392b67, 0x43bd4fcf, 0x1d576326, 0xf2d05be4, 0x25abeed8, 0x139b5dff, 0xcb6f6322, 0xa7c92440, 0x7b842189, 0x11e3872, 0xad216448, 0x721620d3, 0x640e93f8, 0xc0e4b807, 0x11189163, 0xe317e14f, 0x33004543};
    sgenrand((unsigned char)userInput[17], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

