
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0x50c94a81, 0xf405af9, 0xc7865bf8, 0xddeb5b65, 0x9368fd8c, 0x399054d9, 0x6c0fcb68, 0xec837bb4, 0xa8016080, 0x8b6f873a, 0xedf6fb5, 0xd415b3cc, 0xaab662ad, 0xaef3ee2d, 0x73ee3ce2, 0x77382c75, 0x7bb126b5, 0x43a4dea6, 0xf13521cd, 0xb582aaae, 0x9eafb205, 0xe8f07854, 0x421a46d8, 0xdf810b0d, 0x5eafbfd, 0x18561d9b, 0x3105463c, 0x127e05fe, 0x8e8bc4c8, 0xa32c2f7c, 0xb3551e91, 0xde2057f8};
    sgenrand((unsigned char)userInput[34], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

