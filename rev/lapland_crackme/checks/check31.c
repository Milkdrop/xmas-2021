
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0xb94aee73, 0x13cab8, 0x22ed3fbe, 0x175a8b0b, 0xc22a6d43, 0x4386f7c7, 0xf1e5f852, 0x7f4ee99b, 0x34401d9f, 0x270152d5, 0xf9dc946e, 0x2a17b6c6, 0x7a3d77f4, 0x1bbd48d0, 0x521a4a66, 0xef261b94, 0xc8d9d994, 0x2fb28138, 0x6adeeff7, 0xf89b1aff, 0xd576b21, 0xd1c053ed, 0xf723d1e7, 0xc2dd0908, 0x53098b38, 0x9e02f625, 0xf9ca816d, 0xb7f3491a, 0xf86443ef, 0xb10358e5, 0xbe47914, 0xbf0f925b};
    sgenrand((unsigned char)userInput[31], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

