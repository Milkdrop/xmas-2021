#include "../utils.h"
#include <immintrin.h>
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    SHA1_CTX sha;
    unsigned char results[20];
    int n;

    volatile char expected[] = {0xc5, 0x13, 0x50, 0x61, 0x01, 0xb7, 0x2f, 0x9b, 0xaa, 0x53,
                                0xc1, 0x1e, 0xc0, 0x69, 0x6c, 0xaa, 0x70, 0xaf, 0x21, 0x3f};
    n = 4;
    SHA1Init(&sha);
    SHA1Update(&sha, (unsigned char*)&userInput[8], n);
    SHA1Final(results, &sha);

    if (memememcmp(results, (void*)expected, 20) == 0) {

    } else {
        *ptr = 0;
    }

    return 592;
}
