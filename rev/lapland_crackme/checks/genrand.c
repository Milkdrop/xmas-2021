#include "../utils.h"
#include <immintrin.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    if (argc < 2 ) {
        fprintf(stderr, "./gendrand <character>\n");
    }

    uint64_t seed = (unsigned char)argv[1][0];

    uint64_t mt[N];
    uint64_t mti = N + 1;

    sgenrand(seed, mt, &mti);
    printf("{");
    for (int i = 0 ; i < 31 ; i ++) {
        printf("0x%lx, " , genrand(mt, &mti));
    }
    printf("0x%lx}", genrand(mt, &mti));
}
