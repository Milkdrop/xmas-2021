
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0x71db5545, 0x467ddeae, 0x6e17cc65, 0x6e019101, 0x7e2e5d91, 0xce6dbacc, 0xe43b4611, 0x717af6a9, 0xb5ac02fe, 0x8b1fa0df, 0xa19575d1, 0x435ea430, 0x76f3bf03, 0x784c648c, 0xc9ecebd, 0x44a37453, 0x6f50545f, 0xf8237c34, 0x255ecf40, 0x1dbb5791, 0x2bf17a5e, 0xace2fedf, 0xcfdec0df, 0xda8bcd0d, 0x18921f39, 0x6bb67741, 0xce18e6d8, 0xb8c3e650, 0xd10f29ab, 0x8b670d2b, 0x87a05026, 0xd3b3b434};
    sgenrand((unsigned char)userInput[41], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

