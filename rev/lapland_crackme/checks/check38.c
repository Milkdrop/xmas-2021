
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0xa442092c, 0x88d10d19, 0x35867c91, 0x64892fce, 0xa6b575f7, 0x915c9f8, 0xd58e300a, 0x968ad753, 0x9708efa4, 0xb4b12450, 0x98f25816, 0xb2d4fddf, 0x57f626aa, 0x52ee1a18, 0xa4ea7c49, 0xf9bb9ab2, 0xd1fea5a6, 0x1ed7fc13, 0x277e4ad, 0xa7025efb, 0xdaab387a, 0x8cc77525, 0xcbe31501, 0x327f80c1, 0xd65f6b31, 0xe49b65ad, 0x191b1f0b, 0xe7b6daab, 0xc25ea571, 0xe7c7210, 0xb56e4109, 0x12ce7387};
    sgenrand((unsigned char)userInput[38], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

