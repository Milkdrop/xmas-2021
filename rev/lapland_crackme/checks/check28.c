
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0x323e2fd4, 0x9dfc4f97, 0x713eaf85, 0x5be4adaf, 0x95d905e4, 0x7617004, 0x87b4e3ce, 0xdebc9b7c, 0x45e035d6, 0x86398a74, 0x9811b64, 0x2fe3731c, 0xffe48b5b, 0x81fa5827, 0xa4afc9c8, 0xdeac7335, 0x7dba409c, 0xf5c19be5, 0xb42445e3, 0x6b2bdf12, 0xef7c274f, 0x4faa3fe9, 0x7025d598, 0xc5bdbf32, 0x53d7e15e, 0xefa82a86, 0xeed7fb69, 0x9b48c9cd, 0xfef762c1, 0x2b799fdf, 0xa75aa5a, 0x3bbcf02e};
    sgenrand((unsigned char)userInput[28], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

