
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0xd7269368, 0x65b830f1, 0x4a092e29, 0x6dff65a3, 0x69387173, 0xb1a91484, 0x37d06e2c, 0xe793c923, 0x68ae4f80, 0x5225ed38, 0x41b8276e, 0xc0f17214, 0x25f278f2, 0xbf9742e7, 0x785ee11e, 0x9a474339, 0x35fea59c, 0xf966bb94, 0x53ec2f3d, 0x38158246, 0xc9ef7df5, 0x6606e80c, 0x78607a65, 0x8fc3a590, 0x588aa045, 0x1c5e20c, 0x7547d457, 0xadc041d1, 0x32790eb, 0x339a10cb, 0x499ca66a, 0xfceac9a9};
    sgenrand((unsigned char)userInput[40], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

