
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0x801f13e, 0x6c4d8de1, 0xf1d10500, 0x83e19947, 0x897b4ed, 0x74294c05, 0xaa8556e7, 0x2dbf6ace, 0xc60dc4fb, 0xba94b938, 0x287d0c63, 0x53212ccf, 0x89ee4d07, 0x47a2e97a, 0xfc4ee5bb, 0x408ad895, 0x170ada2, 0x9ff618e3, 0x8d1df0c4, 0xc74e7afe, 0x8875e1c4, 0x36a45485, 0x5250fd27, 0x7139f479, 0xe98d5a35, 0xdeede79b, 0x242d546, 0x120e889f, 0x949a8370, 0x214ce865, 0x1f50debf, 0xc5b10235};
    sgenrand((unsigned char)userInput[47], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

