#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    volatile char key[] = {'J' - 32, '9' - 32, '8' - 32, '3' - 32,
                           'j' - 32, '4' - 32, '9' - 32, '}' - 32};

    for (int i = 0; i < sizeof(key) / sizeof(*key); i++) {
        key[i] += 32;
    }

    if (*((volatile long long*)key) != *((const long long*)&userInput[48])) {
        *ptr = 0;
    }

    return 1230;
}
