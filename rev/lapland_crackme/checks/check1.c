#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;
    volatile char key[] = {'X' - 32, '-' - 32, 'M' - 32, 'A' - 32,
                           'S' - 32, '{' - 32, 'P' - 32, '4' - 32};

    for (int i = 0; i < sizeof(key) / sizeof(*key); i++) {
        key[i] += 32;
    }

    if (*((volatile long long*)key) == *((const long long*)userInput) &&
        memestrlen(userInput) == 56) {
    } else {
        *ptr = 0;
    }
    return 95;
}
