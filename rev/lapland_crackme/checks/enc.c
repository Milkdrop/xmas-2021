#include "../utils.h"
#include "marker.h"
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char** argv) {
    if (argc != 3) {
        fprintf(stderr, "./enc <input_binary> <output_binary>\n");
        return -1;
    }

    FILE* urandom = fopen("/dev/urandom", "rb");
    FILE* binary = fopen(argv[1], "rb");
    FILE* output = fopen(argv[2], "wb");
    char key_name[100];

    strcpy(key_name, basename(argv[2]));
    strcat(key_name, ".key");

    printf("opening %s\n", key_name);
    FILE* key = fopen(key_name, "wb");

    char __attribute__((aligned(32))) rc4key[16];

    fread(rc4key, sizeof(rc4key[0]), sizeof(rc4key) / sizeof(rc4key[0]), urandom);
    fwrite(rc4key, sizeof(rc4key[0]), sizeof(rc4key) / sizeof(rc4key[0]), key);
    char header[marker_shellcode_size];
    fread(header, 1, marker_shellcode_size, binary);
    fwrite(header, 1, marker_shellcode_size, output);

    char __attribute__((aligned(32))) urmom[256];

    memerc4_array(urmom);
    memerc4_ksa(urmom, rc4key, 16);
    uint64_t i = 0;
    uint64_t j = 0;
    char chr;
    while (fread(&chr, 1, 1, binary)) {
        char xor = memerc4_prga(urmom, &i, &j);
        chr = chr ^ xor;
        fwrite(&chr, 1, 1, output);
    }
    fclose(urandom);
    fclose(key);
    fclose(binary);
    fclose(output);

    return 0;
}
