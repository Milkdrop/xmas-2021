
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0xac392bf9, 0x5fbbdb58, 0x5c1cbcb1, 0xca989e4, 0xdbccaff3, 0xdac0a11d, 0xfff5a0ee, 0x5e58b829, 0xc879fae7, 0x3ad64c4e, 0xed6c8d6b, 0x72f6ff0, 0x8881e656, 0x5a26dcdf, 0x7863d8ab, 0xa3c6a5a6, 0x635c5c90, 0xd745e6cc, 0x90a458a0, 0x43d42076, 0xec6876d5, 0xe56b46ea, 0xddae2130, 0x12aae667, 0x9336f7b0, 0x57ec8cb3, 0x9b2a2813, 0x282944b0, 0xb74c7dbb, 0xbb5855a5, 0x808a5edb, 0x1d597926};
    sgenrand((unsigned char)userInput[35], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

