
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0xf9a5992a, 0x99ce97c1, 0xc492fc7a, 0xb6203f46, 0x760e7250, 0x3e08b72a, 0x9314a73d, 0x17775263, 0x8a0a3d73, 0xa3713245, 0xdef23370, 0x9591f5c8, 0x50181085, 0xb21afd87, 0xdc08baa8, 0xcfbc2c05, 0xae88b0f4, 0xfd58b2cf, 0xa13dbccd, 0xa879efdc, 0x7754a9b1, 0xc16b4671, 0x5043f90d, 0x4af07450, 0xcd9c9788, 0x697aeed1, 0x2b8b5f3e, 0xc62fb640, 0x3c2a7baa, 0x33bdcd37, 0xcabb3ebb, 0x96020bd5};
    sgenrand((unsigned char)userInput[23], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

