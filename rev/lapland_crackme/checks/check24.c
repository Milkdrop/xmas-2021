
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0x56dc5698, 0xc6830a1c, 0x5a38d63c, 0xd28a8277, 0x61ea6cb6, 0xe8b79b4e, 0x72b02ec9, 0x11faa7f4, 0x933aab98, 0x7584b21f, 0xb6ceced4, 0xdf7f7d79, 0x6641338f, 0xeeece504, 0xe464c635, 0x703e0824, 0xe0a7fe85, 0x5bda8266, 0x9be7ab22, 0x5c7e6617, 0xecebf3f7, 0xd9676bfc, 0x7748b6ff, 0x51dc5d3c, 0xe7664b6f, 0x3fc9f5e9, 0x36ff1320, 0x37c9777d, 0xed4d9f36, 0xb6f478cb, 0x15ddf5e9, 0x62e1bfe8};
    sgenrand((unsigned char)userInput[24], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

