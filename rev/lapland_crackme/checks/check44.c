
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0xe09c2aea, 0xa817d266, 0x8d85106b, 0x78c221df, 0xf41ff1c0, 0xf5c58ae0, 0x5f58775d, 0x1cdfcd48, 0xa1c55a78, 0xdd80b406, 0x41524257, 0xf113ac0d, 0x30754d19, 0xa174b0bc, 0xe489ca2d, 0x99d3cbdf, 0x6db82a6c, 0xbfb67248, 0xa5dfcace, 0x9220a9e3, 0xa2fafa5e, 0x728ef57f, 0x81cebfaa, 0xfc7af308, 0x7fabbc41, 0x3ae0e439, 0xf9aede72, 0x84aa8fd5, 0x1f5fbe6a, 0x9b6695a0, 0x65278497, 0x795fa06d};
    sgenrand((unsigned char)userInput[44], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

