
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0xf36e5798, 0xafd41453, 0xcac65110, 0x1b7583d6, 0x6198155b, 0xc142ce5a, 0x6ab325a9, 0x562ffb72, 0x9fb43b88, 0x46322fd4, 0x46861fbc, 0x7199971, 0xaebcbdde, 0x4f19ef2e, 0x30b81741, 0x177df7bb, 0xf2286558, 0xe1d70cfc, 0xaeda4aeb, 0x3bbcae40, 0x57493918, 0x998d5d56, 0x67805b1c, 0x681bb2a5, 0xf6b22733, 0x9043b824, 0xf13dd986, 0x6c6abd00, 0x511dc11b, 0x6dc13a0b, 0xea4636af, 0x850543e6};
    sgenrand((unsigned char)userInput[21], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

