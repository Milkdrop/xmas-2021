
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {0xf4f6ffd0, 0x3ee6f0f5, 0x3d69cf03, 0x2f8ab15c, 0xdedeea66, 0x84b13868, 0x279047b7, 0x3eff884b, 0x9dfe1f50, 0x90fe3908, 0xf66bfe33, 0xf79fcfa6, 0x25846a3f, 0xf53ef1ff, 0xe131d702, 0xd9e56a76, 0x1c96ec62, 0xf1791a3e, 0x81e292c8, 0xf23a085e, 0x4c13891f, 0x4ec9eede, 0x7c5c834c, 0x44471ba, 0x8b8ddbf8, 0x1e86673e, 0x4917e537, 0x8565a508, 0xb8e33bd6, 0x68210de, 0x2bd10f3e, 0xdf796a30};
    sgenrand((unsigned char)userInput[22], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {
            *ptr = 0;
        }
    }
    return 1337;
}

