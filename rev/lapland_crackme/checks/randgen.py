#!/usr/bin/env python3

import subprocess as sp

CHARACTERS = '_4773N710n_70_7h3_516N4l5_p4y5_0fF_V'

TEMPLATE = '''
#include "../utils.h"
#include <signal.h>

DECRYPTION_MARKER;

int check(const char* userInput, void* privateMem) {{
    sig_atomic_t* ptr = *(sig_atomic_t**)privateMem;

    uint64_t mt[N];
    uint64_t mti = N + 1;
    volatile uint64_t expected[] = {};
    sgenrand((unsigned char)userInput[{}], mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {{
        uint64_t randval = genrand(mt, &mti);
        if (randval != expected[i]) {{
            *ptr = 0;
        }}
    }}
    return 1337;
}}

'''

INCLUDE_TEMPLATE = '''
#include "./checks/check{0}.h"
#include "./checks/check{0}.key.h"
'''

INST_TEMPLATE = '''
{{
        Vec<uint8_t> check = Vec<uint8_t>{{check{0}_enc, check{0}_enc + check{0}_enc_size}};
        Vec<uint8_t> key = Vec<uint8_t>{{check{0}_enc_key, check{0}_enc_key + check{0}_enc_key_size}};

        chunks.emplace_back(check, key);
}}
'''

start_idx = 12
for char in CHARACTERS:
    with sp.Popen(['./genrand', f'{char}'], stdout=sp.PIPE) as process:
        process.wait()
        values = process.stdout.read()
        with open(f'check{start_idx}.c', 'w') as f:
            f.write(TEMPLATE.format(values.decode('utf-8'), start_idx))
            start_idx += 1

start_idx = 12
for char in CHARACTERS:
    print(INCLUDE_TEMPLATE.format(start_idx))
    start_idx += 1

start_idx = 12
for char in CHARACTERS:
    print(INST_TEMPLATE.format(start_idx))
    start_idx += 1
