#include "utils.h"
#include <immintrin.h>
#include <stdio.h>
#include <stdlib.h>

int main() {

    char __attribute__((aligned(32))) input[] =
        "X-MAS{P4y1n6_4773N710n_70_7h3_516N4l5_p4y5_0fF_VJ983j49}";
    SHA1_CTX sha;
    unsigned char results[20];
    int n;

    char expected[] = {0xc5, 0x13, 0x50, 0x61, 0x01, 0xb7, 0x2f, 0x9b, 0xaa, 0x53,
                       0xc1, 0x1e, 0xc0, 0x69, 0x6c, 0xaa, 0x70, 0xaf, 0x21, 0x3f};

    n = 4;
    SHA1Init(&sha);
    SHA1Update(&sha, (unsigned char*)&input[8], n);
    SHA1Final(results, &sha);

    printf("%d\n", memememcmp(results, expected, 20));

    uint64_t mt[N];
    uint64_t mti = N + 1;

    sgenrand(69420, mt, &mti);
    for (int i = 0 ; i < 32 ; i ++) {
        printf("%ld\n" , genrand(mt, &mti));
    }
}
