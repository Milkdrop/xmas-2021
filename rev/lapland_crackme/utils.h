#ifndef UTILS_H_
#define UTILS_H_

#include <immintrin.h>

#define DECRYPTION_MARKER                                                                          \
    asm("nop;"                                                                                     \
        "call out;"                                                                                \
        "out:;"                                                                                    \
        "push 0;"                                                                                  \
        "ret;")

#if !defined(uint64_t)
typedef unsigned long uint64_t;
#endif

#if !defined(uint32_t)
typedef unsigned int uint32_t;
#endif

inline static __attribute__((always_inline)) int memestrcmp(const char* s1, const char* s2) {
    while (*s1 - *s2 == 0 && *s1 != 0 && *s2 != 0) {
        s1++;
        s2++;
    }
    return *s1 - *s2;
}

inline static __attribute__((always_inline)) int memememcmp(const void* s1, const void* s2,
                                                            size_t n) {
    const char* s1c = (const char*)s1;
    const char* s2c = (const char*)s2;

    for (int i = 0; i < n; i++) {
        if (s1c[i] != s2c[i]) {
            return s1c[i] - s2c[i];
        }
    }

    return 0;
}

inline static __attribute__((always_inline)) void* memememcpy(void* dest, const void* src,
                                                              size_t n) {
    for (int i = 0; i < n; i++) {
        ((char*)dest)[i] = ((const char*)src)[i];
    }
    return dest;
}

inline static __attribute__((always_inline)) void* memememset(void* s, int c, size_t n) {
    for (int i = 0; i < n; i++) {
        ((char*)s)[i] = c;
    }
    return s;
}



inline static __attribute__((always_inline)) __m256i _mm256_shradd_epi8(__m256i a, __m256i b,
                                                                        uint64_t cnt) {
    __m128i a_higher = _mm256_extractf128_si256(a, 1);
    __m128i a_lower = _mm256_extractf128_si256(a, 0);

    __m128i b_higher = _mm256_extractf128_si256(b, 1);
    __m128i b_lower = _mm256_extractf128_si256(b, 0);

    __m256i a_higher_unp = _mm256_cvtepi8_epi16(a_higher);
    __m256i a_lower_unp = _mm256_cvtepi8_epi16(a_lower);

    __m256i b_higher_unp = _mm256_cvtepi8_epi16(b_higher);
    __m256i b_lower_unp = _mm256_cvtepi8_epi16(b_lower);

    __m256i higher_unp = _mm256_srli_epi16(_mm256_add_epi16(a_higher_unp, b_higher_unp), cnt);

    __m256i lower_unp = _mm256_srli_epi16(_mm256_add_epi16(a_lower_unp, b_lower_unp), cnt);

    return _mm256_packs_epi16(lower_unp, higher_unp);
}

inline static __attribute__((always_inline)) __m256i _mm256_shrsub_epi8(__m256i a, __m256i b,
                                                                        uint64_t cnt) {
    __m128i a_higher = _mm256_extractf128_si256(a, 1);
    __m128i a_lower = _mm256_extractf128_si256(a, 0);

    __m128i b_higher = _mm256_extractf128_si256(b, 1);
    __m128i b_lower = _mm256_extractf128_si256(b, 0);

    __m256i a_higher_unp = _mm256_cvtepi8_epi16(a_higher);
    __m256i a_lower_unp = _mm256_cvtepi8_epi16(a_lower);

    __m256i b_higher_unp = _mm256_cvtepi8_epi16(b_higher);
    __m256i b_lower_unp = _mm256_cvtepi8_epi16(b_lower);

    __m256i higher_unp = _mm256_srli_epi16(_mm256_sub_epi16(a_higher_unp, b_higher_unp), cnt);

    __m256i lower_unp = _mm256_srli_epi16(_mm256_sub_epi16(a_lower_unp, b_lower_unp), cnt);

    return _mm256_packs_epi16(lower_unp, higher_unp);
}

inline static __attribute__((always_inline)) uint64_t memestrlen(const char* str) {
    uint64_t counter = 0;
    __m256i* __attribute__((aligned(32))) vec_str = (__m256i*)str;

    uint64_t crc = 0;

    long long cache[4];
    __m256i* cache_vec = (__m256i*)(&cache[0]);
    char* cacheptr = (char*)(&cache[0]);

    int has_lowered = 0;
    char lowers_copy[32] = {0};

    while (1) {
        __m256i right = _mm256_set1_epi8(0xff);
        __m256i left = _mm256_set1_epi8(0);

        __m256i str_values = _mm256_load_si256(&vec_str[counter]);

        while (1) {
            __m256i middle = _mm256_add_epi8(_mm256_shrsub_epi8(right, left, 1), left);

            __m256i greater_than_middle = _mm256_cmpgt_epi8(str_values, middle);

            _mm256_store_si256(cache_vec, greater_than_middle);

            has_lowered = 0;
            for (int i = 0; i < 32; i++) {
                if (cacheptr[i] == 0) {
                    crc = _mm_crc32_u8(crc, cacheptr[i]);
                    has_lowered = 1;
                    lowers_copy[i]++;
                    if (lowers_copy[i] == 8) {
                        if (counter == 0 && i <= 8) {
                            return counter * 32 + i;
                        } else {
                            return crc + counter * 32 + i;
                        }
                    }
                }
            }

            if (has_lowered == 0) {
                goto inc_counter;
            }

            right = middle;
        }

    inc_counter:;
        counter++;
    }
}

inline static __attribute__((always_inline)) void memerc4_array(char* array) {
    __m256i* array_ptr = (__m256i*)array;

    __m256i initial = _mm256_set_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,
                                      18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31);
    __m256i increment = _mm256_set1_epi8(32);

    for (int i = 0; i < 8; i++) {
        _mm256_store_si256(&array_ptr[i], initial);
        initial = _mm256_add_epi8(initial, increment);
    }
}

inline static __attribute__((always_inline)) void memerc4_ksa(char* array, char* key,
                                                              uint64_t keylen) {
    uint64_t j = 0;

    for (uint64_t i = 0; i < 256; i++) {
        j = (j + array[i] + key[i % keylen]) % 256;
        char tmp = array[j];
        array[j] = array[i];
        array[i] = tmp;
    }
}

inline static __attribute__((always_inline)) char memerc4_prga(char* array, uint64_t* i,
                                                               uint64_t* j) {
    *i = (*i + 1) % 256;
    *j = (*j + array[*i]) % 256;

    char tmp = array[*i];
    array[*i] = array[*j];
    array[*j] = tmp;

    char t = (array[*i] + array[*j]) % 256;

    return t;
}

typedef struct {
    uint32_t state[5];
    uint32_t count[2];
    unsigned char buffer[64];
} SHA1_CTX;

#define rol(value, bits) (((value) << (bits)) | ((value) >> (32 - (bits))))

#define blk0(i)                                                                                    \
    (block->l[i] = (rol(block->l[i], 24) & 0xFF00FF00) | (rol(block->l[i], 8) & 0x00FF00FF))
#define blk(i)                                                                                     \
    (block->l[i & 15] = rol(block->l[(i + 13) & 15] ^ block->l[(i + 8) & 15] ^                     \
                                block->l[(i + 2) & 15] ^ block->l[i & 15],                         \
                            1))

/* (R0+R1), R2, R3, R4 are the different operations used in SHA1 */
#define R0(v, w, x, y, z, i)                                                                       \
    z += ((w & (x ^ y)) ^ y) + blk0(i) + 0x5A827999 + rol(v, 5);                                   \
    w = rol(w, 30);
#define R1(v, w, x, y, z, i)                                                                       \
    z += ((w & (x ^ y)) ^ y) + blk(i) + 0x5A827999 + rol(v, 5);                                    \
    w = rol(w, 30);
#define R2(v, w, x, y, z, i)                                                                       \
    z += (w ^ x ^ y) + blk(i) + 0x6ED9EBA1 + rol(v, 5);                                            \
    w = rol(w, 30);
#define R3(v, w, x, y, z, i)                                                                       \
    z += (((w | x) & y) | (w & x)) + blk(i) + 0x8F1BBCDC + rol(v, 5);                              \
    w = rol(w, 30);
#define R4(v, w, x, y, z, i)                                                                       \
    z += (w ^ x ^ y) + blk(i) + 0xCA62C1D6 + rol(v, 5);                                            \
    w = rol(w, 30);

/* Hash a single 512-bit block. This is the core of the algorithm. */
inline static __attribute__((always_inline)) void SHA1Transform(uint32_t state[5],
                                                                unsigned char buffer[64]) {
    uint32_t a, b, c, d, e;

    typedef union {
        unsigned char c[64];
        uint32_t l[16];
    } CHAR64LONG16;

#ifdef SHA1HANDSOFF
    CHAR64LONG16 block[1]; /* use array to appear as a pointer */

    memememcpy(block, buffer, 64);
#else
    /* The following had better never be used because it causes the
     * pointer-to-const buffer to be cast into a pointer to non-const.
     * And the result is written through.  I threw a "const" in, hoping
     * this will cause a diagnostic.
     */
    CHAR64LONG16* block = (CHAR64LONG16*)buffer;
#endif
    /* Copy context->state[] to working vars */
    a = state[0];
    b = state[1];
    c = state[2];
    d = state[3];
    e = state[4];
    /* 4 rounds of 20 operations each. Loop unrolled. */
    R0(a, b, c, d, e, 0);
    R0(e, a, b, c, d, 1);
    R0(d, e, a, b, c, 2);
    R0(c, d, e, a, b, 3);
    R0(b, c, d, e, a, 4);
    R0(a, b, c, d, e, 5);
    R0(e, a, b, c, d, 6);
    R0(d, e, a, b, c, 7);
    R0(c, d, e, a, b, 8);
    R0(b, c, d, e, a, 9);
    R0(a, b, c, d, e, 10);
    R0(e, a, b, c, d, 11);
    R0(d, e, a, b, c, 12);
    R0(c, d, e, a, b, 13);
    R0(b, c, d, e, a, 14);
    R0(a, b, c, d, e, 15);
    R1(e, a, b, c, d, 16);
    R1(d, e, a, b, c, 17);
    R1(c, d, e, a, b, 18);
    R1(b, c, d, e, a, 19);
    R2(a, b, c, d, e, 20);
    R2(e, a, b, c, d, 21);
    R2(d, e, a, b, c, 22);
    R2(c, d, e, a, b, 23);
    R2(b, c, d, e, a, 24);
    R2(a, b, c, d, e, 25);
    R2(e, a, b, c, d, 26);
    R2(d, e, a, b, c, 27);
    R2(c, d, e, a, b, 28);
    R2(b, c, d, e, a, 29);
    R2(a, b, c, d, e, 30);
    R2(e, a, b, c, d, 31);
    R2(d, e, a, b, c, 32);
    R2(c, d, e, a, b, 33);
    R2(b, c, d, e, a, 34);
    R2(a, b, c, d, e, 35);
    R2(e, a, b, c, d, 36);
    R2(d, e, a, b, c, 37);
    R2(c, d, e, a, b, 38);
    R2(b, c, d, e, a, 39);
    R3(a, b, c, d, e, 40);
    R3(e, a, b, c, d, 41);
    R3(d, e, a, b, c, 42);
    R3(c, d, e, a, b, 43);
    R3(b, c, d, e, a, 44);
    R3(a, b, c, d, e, 45);
    R3(e, a, b, c, d, 46);
    R3(d, e, a, b, c, 47);
    R3(c, d, e, a, b, 48);
    R3(b, c, d, e, a, 49);
    R3(a, b, c, d, e, 50);
    R3(e, a, b, c, d, 51);
    R3(d, e, a, b, c, 52);
    R3(c, d, e, a, b, 53);
    R3(b, c, d, e, a, 54);
    R3(a, b, c, d, e, 55);
    R3(e, a, b, c, d, 56);
    R3(d, e, a, b, c, 57);
    R3(c, d, e, a, b, 58);
    R3(b, c, d, e, a, 59);
    R4(a, b, c, d, e, 60);
    R4(e, a, b, c, d, 61);
    R4(d, e, a, b, c, 62);
    R4(c, d, e, a, b, 63);
    R4(b, c, d, e, a, 64);
    R4(a, b, c, d, e, 65);
    R4(e, a, b, c, d, 66);
    R4(d, e, a, b, c, 67);
    R4(c, d, e, a, b, 68);
    R4(b, c, d, e, a, 69);
    R4(a, b, c, d, e, 70);
    R4(e, a, b, c, d, 71);
    R4(d, e, a, b, c, 72);
    R4(c, d, e, a, b, 73);
    R4(b, c, d, e, a, 74);
    R4(a, b, c, d, e, 75);
    R4(e, a, b, c, d, 76);
    R4(d, e, a, b, c, 77);
    R4(c, d, e, a, b, 78);
    R4(b, c, d, e, a, 79);
    /* Add the working vars back into context.state[] */
    state[0] += a;
    state[1] += b;
    state[2] += c;
    state[3] += d;
    state[4] += e;
    /* Wipe variables */
    a = b = c = d = e = 0;
#ifdef SHA1HANDSOFF
    memememset(block, '\0', sizeof(block));
#endif
}

/* SHA1Init - Initialize new context */
inline static __attribute__((always_inline)) void SHA1Init(SHA1_CTX* context) {
    /* SHA1 initialization constants */
    context->state[0] = 0x67452301;
    context->state[1] = 0xEFCDAB89;
    context->state[2] = 0x98BADCFE;
    context->state[3] = 0x10325476;
    context->state[4] = 0xC3D2E1F0;
    context->count[0] = context->count[1] = 0;
}

/* Run your data through this. */
inline static __attribute__((always_inline)) void SHA1Update(SHA1_CTX* context, unsigned char* data,
                                                             uint32_t len) {
    uint32_t i;

    uint32_t j;

    j = context->count[0];
    if ((context->count[0] += len << 3) < j)
        context->count[1]++;
    context->count[1] += (len >> 29);
    j = (j >> 3) & 63;
    if ((j + len) > 63) {
        memememcpy(&context->buffer[j], data, (i = 64 - j));
        SHA1Transform(context->state, context->buffer);
        for (; i + 63 < len; i += 64) {
            SHA1Transform(context->state, &data[i]);
        }
        j = 0;
    } else
        i = 0;
    memememcpy(&context->buffer[j], &data[i], len - i);
}

/* Add padding and return the message digest. */
inline static __attribute__((always_inline)) void SHA1Final(unsigned char digest[20],
                                                            SHA1_CTX* context) {
    unsigned i;

    unsigned char finalcount[8];

    unsigned char c;

#if 0 /* untested "improvement" by DHR */
    /* Convert context->count to a sequence of bytes
     * in finalcount.  Second element first, but
     * big-endian order within element.
     * But we do it all backwards.
     */
    unsigned char *fcp = &finalcount[8];

    for (i = 0; i < 2; i++)
    {
        uint32_t t = context->count[i];

        int j;

        for (j = 0; j < 4; t >>= 8, j++)
            *--fcp = (unsigned char) t}
#else
    for (i = 0; i < 8; i++) {
        finalcount[i] = (unsigned char)((context->count[(i >= 4 ? 0 : 1)] >> ((3 - (i & 3)) * 8)) &
                                        255); /* Endian independent */
    }
#endif
    c = 0200;
    SHA1Update(context, &c, 1);
    while ((context->count[0] & 504) != 448) {
        c = 0000;
        SHA1Update(context, &c, 1);
    }
    SHA1Update(context, finalcount, 8); /* Should cause a SHA1Transform() */
    for (i = 0; i < 20; i++) {
        digest[i] = (unsigned char)((context->state[i >> 2] >> ((3 - (i & 3)) * 8)) & 255);
    }
    /* Wipe variables */
    memememset(context, '\0', sizeof(*context));
    memememset(&finalcount, '\0', sizeof(finalcount));
}

inline static __attribute__((always_inline)) void SHA1(char* hash_out, const char* str, int len) {
    SHA1_CTX ctx;
    unsigned int ii;

    SHA1Init(&ctx);
    for (ii = 0; ii < len; ii += 1)
        SHA1Update(&ctx, (unsigned char*)str + ii, 1);
    SHA1Final((unsigned char*)hash_out, &ctx);
}

/* Period parameters */
#define N 624
#define M 397
#define MATRIX_A 0x9908b0df   /* constant vector a */
#define UPPER_MASK 0x80000000 /* most significant w-r bits */
#define LOWER_MASK 0x7fffffff /* least significant r bits */

#define TEMPERING_MASK_B 0x9d2c5680
#define TEMPERING_MASK_C 0xefc60000
#define TEMPERING_SHIFT_U(y) (y >> 11)
#define TEMPERING_SHIFT_S(y) (y << 7)
#define TEMPERING_SHIFT_T(y) (y << 15)
#define TEMPERING_SHIFT_L(y) (y >> 18)

/* static unsigned long mt[N]; /\* the array for the state vector  *\/ */
/* static int mti = N + 1;     /\* mti==N+1 means mt[N] is not initialized *\/ */

inline static __attribute__((always_inline)) void sgenrand(uint64_t seed, uint64_t* mt,
                                                           uint64_t* mti) {
    int i;

    for (i = 0; i < N; i++) {
        mt[i] = seed & 0xffff0000;
        seed = 69069 * seed + 1;
        mt[i] |= (seed & 0xffff0000) >> 16;
        seed = 69069 * seed + 1;
    }
    *mti = N;
}

inline static __attribute__((always_inline)) uint64_t genrand(uint64_t* mt, uint64_t* mti) {
    uint64_t y;
    unsigned long mag01[2] = {0x0, MATRIX_A};

    if (*mti >= N) { /* generate N words at one time */
        int kk;

        for (kk = 0; kk < N - M; kk++) {
            y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
            mt[kk] = mt[kk + M] ^ (y >> 1) ^ mag01[y & 0x1];
        }
        for (; kk < N - 1; kk++) {
            y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
            mt[kk] = mt[kk + (M - N)] ^ (y >> 1) ^ mag01[y & 0x1];
        }
        y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
        mt[N - 1] = mt[M - 1] ^ (y >> 1) ^ mag01[y & 0x1];

        *mti = 0;
    }

    y = mt[*mti];
    *mti += 1;
    y ^= TEMPERING_SHIFT_U(y);
    y ^= TEMPERING_SHIFT_S(y) & TEMPERING_MASK_B;
    y ^= TEMPERING_SHIFT_T(y) & TEMPERING_MASK_C;
    y ^= TEMPERING_SHIFT_L(y);

    return y;
}

#endif // UTILS_H_
