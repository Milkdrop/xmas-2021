import random
import time
from ocean import init_populate_ocean, update_ocean_slices

colour = random.randint(31, 37)
ocean = init_populate_ocean()
steps = 0
total = ""
flag_frags = [
	"X-M",
	"AS{y0",
	"u_cr4C",
	"K4d_",
	"thE_c0",
	"d3!_",
	"afa",
	"03",
	"819d",
	"ef2f}"
]
s_steps = 1

print("\n" * 100 + "\033[1;1H")

while True:
	if steps > 30:
		fragment = input("\033[12;1HPlease enter the flag fragment (flagment) to continue ({}/10). \nYour hint is \"{}\" >> ".format(s_steps, flag_frags[s_steps - 1][:2]))
		total += fragment
		s_steps += 1
		steps = 0
		colour = random.randint(31, 37)

		if s_steps > 10:
			print("\n\nThank you!\nYou sent: {} (redacted for security reasons).\nGoodbye.".format("".join("#" for _ in range(len(total)))), flush=True)
			break

	print("\033[1;1H\u001b[{};1m".format(colour) + update_ocean_slices(ocean, 20) + "\033[12;1H" + (" " * 200), flush=True)
	steps += 1
	time.sleep(0.1)