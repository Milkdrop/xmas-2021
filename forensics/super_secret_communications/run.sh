#!/bin/bash
echo Running $1 on port $2
p=$1
socat TCP-LISTEN:$2,fork,reuseaddr,nodelay EXEC:"python3 ${p}"
