nal_starting = 2
do_not_write = 3
preserve_bytes = 9999
grace_frames = 0
frames_to_remove = 47
file_out = []
nal_next = False
end_bytes = 0

buffer = [0xff, 0xff, 0xff]
current_read_nal = []
read_nal_location = 1

with open("output/video_new", "rb") as f_r:
	for byte in f_r.read():
		buffer.append(byte)
		last_byte = buffer[0]
		buffer = buffer[1:4]
		if buffer[0] == 0x00 and buffer[1] == 0x00 and buffer[2] == 0x01:
			nal_next = True
			frames_to_remove -= 1
			grace_frames -= 1
			if frames_to_remove < 0 or grace_frames >= 0:
				pass
			else:
				print("\nSwitching stream to NAL", nal_starting)
				preserve_bytes = 4
				with open("nals/nal_{}".format(nal_starting), "rb") as f:
					current_read_nal = f.read()
					read_nal_location = 0

				nal_starting += 1

		elif nal_next:
			nal_next = False

		if do_not_write <= 0:
			if read_nal_location >= len(current_read_nal):
				if read_nal_location == len(current_read_nal):
					print("")
					read_nal_location = len(current_read_nal) + 1

				print("\rWriting end bytes", end_bytes, end="")
				file_out.append(last_byte)
				end_bytes += 1
			elif preserve_bytes > 0:
				print("\rWriting preserve byte", preserve_bytes, end="")
				file_out.append(last_byte)
			else:
				print("\rWriting NAL byte", read_nal_location, "of", len(current_read_nal) - 1, end="")
				file_out.append(current_read_nal[read_nal_location])
				read_nal_location += 1
		else:
			print("Ignoring DNR byte")
			do_not_write -= 1

		preserve_bytes -= 1

file_out.append(buffer[0])
file_out.append(buffer[1])
file_out.append(buffer[2])

with open("solved/reconstructed_raw", "wb") as f:
	f.write(bytes(file_out))

file_total = None
with open("video_header.raw", "rb") as f:
	file_total = f.read() + bytes(file_out)

with open("video_footer.raw", "rb") as f:
	file_total += f.read()

with open("solved/reconstructed_video.mp4", "wb") as f:
	f.write(bytes(file_total))
