nal_next = False
nal_preserve = 9999
num_nals = 0
current_nal_bytes = 0
grace_frames = 0
frames_to_remove = 47
file_out = []
nal_redirect_buffer = []
bytes_dropped = 0
with open("video_raw", "rb") as f_r:
	buffer = [0xff, 0xff, 0xff]
	for byte in f_r.read():
		buffer.append(byte)
		last_byte = buffer[0]
		buffer = buffer[1:4]
		if buffer[0] == 0x00 and buffer[1] == 0x00 and buffer[2] == 0x01:
			nal_next = True
			num_nals += 1
			nal_preserve = 4
			frames_to_remove -= 1
			grace_frames -= 1
			current_nal_bytes = 0
			print("")
			if frames_to_remove < 0 or grace_frames >= 0:
				nal_preserve = 99999999999999

			if nal_redirect_buffer:
				with open("nals/nal_{}".format(num_nals), "wb") as f:
					f.write(bytes(nal_redirect_buffer))

				nal_redirect_buffer = []

		elif nal_next:
			nal_next = False
			forbidden_zero = (byte & 0b10000000) >> 7
			nal_ref_idc = (byte & 0b01100000) >> 5
			nal_unit_type = (byte & 0b00011111)

		if nal_preserve > 0:
			# we need to make sure these stay intact
			file_out.append(last_byte)
		else:
			print("\rDropping NAL byte", current_nal_bytes, end="")
			current_nal_bytes += 1
			bytes_dropped += 1
			file_out.append(0x00)
			nal_redirect_buffer.append(last_byte)

		nal_preserve -= 1

file_out.append(buffer[0])
file_out.append(buffer[1])
file_out.append(buffer[2])

with open("output/video_new", "wb") as f_w:
	f_w.write(bytes(file_out[3:]))

file_total = None
with open("video_header.raw", "rb") as f:
	file_total = f.read() + bytes(file_out[3:])

with open("video_footer.raw", "rb") as f:
	file_total += f.read()

with open("output/edited_video.mp4", "wb") as f:
	f.write(bytes(file_total))

print("Wrote {} NALs".format(num_nals))
print("Dropped {:,d} bytes".format(bytes_dropped))
