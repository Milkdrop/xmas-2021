import time, random
from datetime import date
import os

print("=" * 64)
print("=" + "".center(62) + "=")
print("=" + "R E C E N T A    T H E R A P E U T I C S".center(62) + "=")
print("=" + "Biotechnological progress is our #1 priority.".center(62) + "=")
print("=" + "".center(62) + "=")
print("=" + "THIS IS A PRIVATE GATEWAY".center(62) + "=")
print("=" + "ALL UNAUTHORIZED ACCESS WILL BE PUNISHED".center(62) + "=")
print("=" + "BY THE FULLEST EXTENT OF SANTA'S LEGISLATION".center(62) + "=")
print("=" + "!!!".center(62) + "=")
print("=" + "".center(62) + "=")
print("=" + "EMPLOYEES, PLEASE LOGIN WITH".center(62) + "=")
print("=" + "YOUR \"DISCORD\" USERNAME AND DISCRIMINATOR".center(62) + "=")
print("=" + "".center(62) + "=")
print("=" * 64)
print("")
print(" " * 24 + "! LOGIN: ", end = "", flush = True)
uname = input()

def wait(duration, message):
    frames = ["/", "-", "\\", "|"]
    frame_i = 0

    while(duration > 0):
        loading_char = frames[frame_i % len(frames)]
        print(f"\r{message} {loading_char}", end="", flush = True)
        duration -= 1
        frame_i += 1
        time.sleep(0.25)

    print(f"\r{message} OK", flush = True)

def die():
    print("!!! CONNECTION DROPPED", flush = True)
    exit(0)

wait(random.randint(2, 5), "CONTACTING THE GATEWAY...")

if (len(uname) < 5 or uname[-5] != "#" or not uname[-4].isnumeric()):
    print("!!! INVALID USERNAME AND DISCRIMINATOR ENTERED.")
    die()

time.sleep(1.5)
wait(random.randint(10, 15), "LOGGING IN...")
time.sleep(0.75)
print("### WELCOME! TODAY IS " + date.today().strftime("%B %d, %Y").upper())
print("### WE HOPE YOU WILL HAVE A PRODUCTIVE DAY! - MANAGEMENT")
time.sleep(1)
wait(random.randint(10, 20), "CONNECTING TO DATABASE...")
wait(random.randint(2, 5), "RETRIEVING EMPLOYEE DATA...")
wait(random.randint(1, 3), "RETRIEVING PROJECTS...")
wait(random.randint(2, 6), "CHECKING GENERAL SECURITY ACCESS...")
time.sleep(0.5)
print("### YOU ARE PART OF 'HANTAAN_GROUP'")

for i in range(1, 10):
    wait(random.randint(random.randint(1,3), random.randint(2, 10)), f"RUNNING STANDARD SECURITY CHECK #{i}...")

print("### SECURITY CLEARANCE GRANTED! YOUR TERMINAL IS APPROVED FOR CONNECTION")
time.sleep(3)
print("### PLEASE SELECT AN ACTIVE PROJECT:")
print("###")
print("### 1. HANTAAN")
print("###")
print("")

print(" " * 24 + "! SELECTION: ", end = "", flush = True)
sel = input()
while (sel != "1"):
    print("!!! INVALID PROJECT SELECTION")
    print("")
    print(" " * 24 + "! SELECTION: ", end = "", flush = True)
    sel = input()

wait(random.randint(1, 3), "FETCHING 'HANTAAN' WORKLOAD...")
print("###")
print("### WORKLOAD IS:")
print("###")
print("### ! REQUESTING HANTAANVIRUS MRNA VACCINE PROTOTYPE SEQUENCE CODE")
print("###")

check_valid = True

while check_valid:
    print(" " * 24 + "! SUBMIT WORK: ", end = "", flush = True)
    work = input().upper()[:25000].replace("U", "T")
    time.sleep(1)
    wait(random.randint(5, 10), "SUBMITTING WORK...")

    check_valid = False

    index = 0
    for c in work:
        if (c not in "ACTGU"):
            print(f"!!! INVALID NUCLEOTIDE FOUND AT ({index}). WORK MUST ONLY CONTAIN A,C,T,G,U.", flush = True)
            check_valid = True

        index += 1

    if (len(work) <= 1024):
        print("!!! YOUR WORK IS TOO SHORT. ARE YOU SURE YOU SUBMITTED EVERYTHING?", flush = True)
        check_valid = True
time.sleep(2)
print("### SUBMITTED WORK PASSED VALIDITY CHECKS")
wait(random.randint(5, 10), "SURFACE-ANALYZING WORKLOAD...")
time.sleep(1)

print("")
print("> EXECUTING VIRTUAL RIBOSOME CHECK <")

chunk_size = 24
saw_start = False
saw_start_position = 0

saw_stop = False

is_in_poly_a = False

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

aa_table = {
	"phenylalanine": ["TTT", "TTC"],
	"leucine": ["TTA", "TTG",  "CTT", "CTC", "CTA", "CTG"],
	"serine": ["TCT", "TCC", "TCA", "TCG",  "AGT", "AGC"],
	"tyrosine": ["TAT", "TAC"],
	"stop": ["TAA", "TAG", "TGA"],
	"cysteine": ["TGT", "TGC"],
	"tryptophan": ["TGG"],
	"proline": ["CCT", "CCC", "CCA", "CCG"],
	"histidine": ["CAT", "CAC"],
	"glutamine": ["CAA", "CAG"],
	"arginine": ["CGT", "CGC", "CGA", "CGG",  "AGA", "AGG"],
	"isoleucine": ["ATT", "ATC", "ATA"],
	"methionine": ["ATG"],
	"thereonine": ["ACT", "ACC", "ACA", "ACG"],
	"asparagine": ["AAT", "AAC"],
	"lysine": ["AAA", "AAG"],
	"valine": ["GTT", "GTC", "GTA", "GTG"],
	"alanine": ["GCT", "GCC", "GCA", "GCG"],
	"aspartic acid": ["GAT", "GAC"],
	"glutamic acid": ["GAA", "GAG"],
	"glycine": ["GGT", "GGC", "GGA", "GGG"]
}

def get_aa(codon):
	for protein in aa_table:
		if (codon in aa_table[protein]):
			return protein.upper()

for i in range(0, len(work) - chunk_size):
    if (saw_start and i % 3 != saw_start_position % 3 and not saw_stop):
        continue

    print("\r", end = "", flush = True)

    if (saw_start and not saw_stop):
        print("> ", end = "")

        for c in chunks(work[i:i + chunk_size], 3):
            print(c + "-", end="")
        
        print(" <", end="")
    else:
        print("> " + work[i:i + chunk_size] + " <", end = "")
    
    print("", end = "", flush = True)
    time.sleep(random.random() * 0.1)
    
    print(f" {i}/{len(work) - chunk_size}", end = "")
    if (not saw_start):
        print(" [FIVE-PRIME] = ", end = "")
    elif (not saw_stop):
        print(" [OPEN-READING-FRAME] = ", end = "")
    else:
        if (not is_in_poly_a):
            is_in_poly_a = all(c == "A" for c in work[i:])

        if (is_in_poly_a):
            print(" [POLY-A] = ", end = "")
        else:
            print(" [THREE-PRIME] = ", end = "")

    if (work[i:i + 3] == "ATG" and not saw_start):
        print("!!! POSSIBLE START CODON", end = "", flush = True)
        saw_start = True
        saw_start_position = i
        time.sleep(1.5)
    else:
        if (saw_start and not saw_stop):
            aa = get_aa(work[i:i + 3])
            if (aa == "STOP"):
                saw_stop = True
                print("!!! STOP CODON FOUND", end="", flush = True)
                time.sleep(2)
            else:
                print(aa.ljust(25), end="")
        else:
            if (work[i] == "A"): print("? ADENINE".ljust(36), end = "")
            if (work[i] == "C"): print("? CYTOSINE".ljust(36), end = "")
            if (work[i] == "T"): print("? THYMINE".ljust(36), end = "")
            if (work[i] == "G"): print("? GUANINE".ljust(36), end = "")

    print("", end = "", flush = True)

print("")
if (not saw_start):
    print("!!! NO START CODON?")

elif (not saw_stop):
    print("!!! NO STOP CODON?")

else:
    print("###")
    print("### YOUR WORK HAS PASSED THE VIRTUAL RIBOSOME TEST!")
    print("###")
    print("### PLEASE WAIT UP TO 24H FOR YOUR QUEUED IN-VIVO CELL TEST TO COMPLETE.")
    print("###")
    print("### YOU WILL BE CONTACTED BY AN OPERATOR IF YOUR VACCINE EXCEEDS OUR")
    print("### MINIMUM EFFICACY THERSHOLD")
    print("###")

    i = len(os.listdir("/chall/storage"))

    open(f"/chall/storage/{i}.txt","w").write(uname + "\n" + work + "\n")