import sys
import string
import functools
from constants import *
from challenge import OTPGenerator
from utils import *


def intro_prompt():
    print("Hi, I am new to Design Patterns and have learned about this Singleton thingy, and I want to test it for "
          "bugs! I made an One Time Pad generator in order to test it. Can you please help me debug it?")
    sys.stdout.flush()


def invalid_input():
    print("Invalid input, closing the connection\n\n")
    sys.stdout.flush()
    exit(0)


def get_alphanumeric_input(input_name):
    print(f"Give me an alphanumeric string:\n{input_name} = ", end="")
    sys.stdout.flush()
    user_input = input()
    alphanum = string.ascii_letters + string.digits
    is_valid = functools.reduce(lambda x, y: x and y, map(lambda x: x in alphanum, user_input))

    if not is_valid:
        invalid_input()

    return user_input


def main():
    intro_prompt()

    flag = bytes_2_bit_array(FLAG)
    otp_1 = bytes_2_bit_array(OTPGenerator().get_otp())
    encrypted_flag = xor(flag, otp_1)
    print(f"Encrypted Flag: {bit_array_2_bytes(encrypted_flag)}")
    sys.stdout.flush()

    user_input = bytes_2_bit_array(get_alphanumeric_input("input").encode())
    otp_2 = bytes_2_bit_array(OTPGenerator().get_otp())
    encrypted_user_input = xor(user_input, otp_2)
    print(f"Encrypted Input: {bit_array_2_bytes(encrypted_user_input)}")
    sys.stdout.flush()


if __name__ == '__main__':
    main()
