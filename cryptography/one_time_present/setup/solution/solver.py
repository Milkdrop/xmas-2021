#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This exploit template was generated via:
# $ pwn template --host localhost --port 2000 ../dist/chall
from pwn import *

# Set up pwntools for the correct architecture
exe = context.binary = ELF('../dist/chall')

# Many built-in settings can be controlled on the command-line and show up
# in "args".  For example, to dump all data sent/received, and disable ASLR
# for all created processes...
# ./exploit.py DEBUG NOASLR
# ./exploit.py GDB HOST=example.com PORT=4141
host = args.HOST or 'challs.xmas.htsp.ro'
port = int(args.PORT or '1037')

def start_local(argv=[], *a, **kw):
    '''Execute the target binary locally'''
    if args.GDB:
        return gdb.debug([exe.path] + argv, gdbscript=gdbscript, *a, **kw)
    else:
        return process([exe.path] + argv, *a, **kw)

def start_remote(argv=[], *a, **kw):
    '''Connect to the process on the remote host'''
    io = connect(host, port)
    if args.GDB:
        gdb.attach(io, gdbscript=gdbscript)
    return io

def start(argv=[], *a, **kw):
    '''Start the exploit against the target.'''
    if args.LOCAL:
        return start_local(argv, *a, **kw)
    else:
        return start_remote(argv, *a, **kw)

# Specify your GDB script here for debugging
# GDB will be launched if the exploit is run via e.g.
# ./exploit.py GDB
gdbscript = '''
tbreak main
continue
'''.format(**locals())

#===========================================================
#                    EXPLOIT GOES HERE
#===========================================================
# Arch:     amd64-64-little
# RELRO:    Full RELRO
# Stack:    Canary found
# NX:       NX enabled
# PIE:      PIE enabled
# FORTIFY:  Enabled

io = start()

FLAG_LEN = 77
io = start()

flag_try = [[] for i in range(FLAG_LEN)]

possible = [i for i in range (256)]

while True:
    io.send(b'l' * 10000)
    for _ in range(0, 10000):
        io.recvuntil("input: ")
        enc = io.recvline(keepends=False)
        #log.info(f'{enc=}')
        enc_bytes = unhex(enc)

        for (i, b) in enumerate(enc_bytes):
            if b not in flag_try[i]:
                flag_try[i].append(b)

        flag_found = True

        for tr in flag_try:
            if len(tr) != 255:
                flag_found = False

        if flag_found:
            flag = ""
            for l in flag_try:
                missing = list(set(possible) - set(l))
                log.info(f'{missing=}')
                flag += chr(missing[0])

            log.info(f'{flag=}')
            break
    log.info('Finished iteration')

io.close()
