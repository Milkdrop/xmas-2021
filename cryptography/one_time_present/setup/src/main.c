#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define FLAG_LEN 77

char flag[FLAG_LEN + 1];
char otp[FLAG_LEN + 1] = {0};

FILE* urandom;

void send_otp() {
    while (strlen(otp) != FLAG_LEN) {
        fread(otp, FLAG_LEN + 1, 1, urandom);
    }

    for (int i = 0; i < FLAG_LEN; i++) {
        printf("%02hhx", (char)(flag[i] ^ otp[i]));
    }
    printf("\n");
}

int main() {
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);

    FILE* flagfile = fopen("/home/ctf/flag.txt", "r");
    fread(flag, 1, FLAG_LEN, flagfile);
    fclose(flagfile);

    urandom = fopen("/dev/urandom", "r");
    
    printf("Welcome to the Turbo Mega Secret Krampus Secret Message Sender!\n");
    sleep(1);
    printf("This service is not intended for pesky elves, therefore if you\n");
    printf("are an elf, Krampus asks you pretty please to close your eyes!");
    sleep(2);
    printf("Agent, now that we are alone, take this encrypted message:\n");

    memset(otp, 0, FLAG_LEN + 1);
    send_otp();

    printf("I will send you the decryption key another way.\n");
    sleep(1);
    printf("If you think one copy is enough press '1', otherwise if you\n");
    printf("think that you might need more copies, press any other key\n");
    sleep(1);
    printf("Do not worry about generating more, the message is encrypted with\n");
    printf("The strongest encryption method known to Lapland!\n");
    sleep(2);

    while (1) {
        printf("Awaiting your input: ");
        char key;
        scanf("%c", &key);

        if (key == '1') {
            return 0;
        } else {
            memset(otp, 0, FLAG_LEN + 1);
            send_otp();
        }
    }
}
