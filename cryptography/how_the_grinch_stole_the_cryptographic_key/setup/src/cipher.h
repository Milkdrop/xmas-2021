#ifndef CIPHER_H
#define CIPHER_H

#include <vector>

#define ROUNDS 64

class Cipher {
  private:
    std::vector<unsigned short> key;
    std::vector<unsigned short> s_box;
    std::vector<unsigned short> s_box_inv;

  public:
    Cipher(std::vector<unsigned short> key, std::vector<unsigned short> p_box);
    unsigned short encrypt(unsigned short plaintext);
    unsigned short decrypt(unsigned short ciphertext);
};

#endif // CIPHER_H
