#include "cipher.h"
#include <assert.h>

Cipher::Cipher(std::vector<unsigned short> key,
               std::vector<unsigned short> s_box)
    : key(key), s_box(s_box) {
    assert(key.size() == ROUNDS);
    assert(s_box.size() == (1 << 16));

    s_box_inv.resize(s_box.size());
    for (int i = 0; i < (int)s_box.size(); i++) {
        s_box_inv[s_box[i]] = i;
    }
}

unsigned short Cipher::encrypt(unsigned short plaintext) {
    unsigned short ciphertext = plaintext;
    for (int round = 0; round < (int)key.size(); round++) {
        ciphertext ^= key[round];
        ciphertext = s_box[ciphertext];
    }

    return ciphertext;
}

unsigned short Cipher::decrypt(unsigned short ciphertext) {
    unsigned short plaintext = ciphertext;
    for (int round = key.size() - 1; round >= 0; round--) {
        plaintext = s_box_inv[plaintext];
        plaintext ^= key[round];
    }

    return plaintext;
}
