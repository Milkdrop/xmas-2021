import os
from itertools import product
import numpy as np
from challenge import *
from utils import *


class gf2_vec:
    def __init__(self, vec, const):
        self.vec = vec
        self.const = const

    def __invert__(self):
        return gf2_vec([1 ^ a for a in self.vec], self.const ^ 1)

    def __xor__(self, other):
        if type(other) == gf2_vec:
            return gf2_vec(xor(self.vec, other.vec), self.const ^ other.const)
        else:
            return gf2_vec(self.vec, self.const ^ other)

    def __str__(self):
        return str(self.vec) + ', ' + str(self.const)

    def evaluate(self, values):
        return self.const ^ (sum([a & b for a, b in zip(self.vec, values)]) & 1)


class bit_map:
    def __init__(self, bits):
        self.bits = bits

    def __len__(self):
        return len(self.bits)

    def __xor__(self, other):
        assert len(self) == len(other)
        if type(other) == bit_map:
            return bit_map([(a ^ b if type(a) == gf2_vec else b ^ a) for (a, b) in zip(self.bits, other.bits)])
        else:
            return bit_map([a ^ b for (a, b) in zip(self.bits, other)])

    def __lshift__(self, other):
        return bit_map(rotate_left(self.bits, other))

    def __rshift__(self, other):
        return bit_map(rotate_right(self.bits, other))

    def __str__(self):
        result = ''
        for elem in self.bits:
            result += str(elem) + '\n'
        return result

    def __getitem__(self, item):
        result = self.bits.__getitem__(item)
        if type(result) == list:
            return bit_map(result)
        else:
            return result

    def __setitem__(self, key, value):
        self.bits[key] = value

    def __add__(self, other):
        if type(other) == bit_map:
            return bit_map(self.bits + other.bits)
        else:
            return bit_map(self.bits + other)

    def apply(self, values):
        result = []
        for i, elem in enumerate(self.bits):
            if type(elem) == gf2_vec:
                result.append(elem.evaluate(values))
            else:
                result.append(elem)
        return result


def get_key_schedule(key):
    keys = []

    for i in range(ROUNDS):
        k3 = key[0: HALF_WORD_SIZE]
        k1 = key[HALF_WORD_SIZE: 2 * HALF_WORD_SIZE]
        k2 = key[2 * HALF_WORD_SIZE: 3 * HALF_WORD_SIZE]
        k0 = key[-HALF_WORD_SIZE:]
        keys.append(k1 + k0)

        k0 = (k0 << 7) ^ k1
        k2 = (k2 ^ k3) >> 5
        k3 = (k3 ^ k1) << 2
        k1 = (k1 >> 6) ^ k2

        key = k3 + k2 + k1 + k0

    return keys


def bit_map_round_function(inp, key):
    i1 = inp[:HALF_WORD_SIZE]
    i0 = inp[HALF_WORD_SIZE:]
    k1 = key[:HALF_WORD_SIZE]
    k0 = key[HALF_WORD_SIZE:]

    assert type(k1[7]) == int and type(k1[3]) == int

    x = 2 * k1[7] + k1[3]

    o1 = (k0 ^ i0) >> x
    o0 = k1 ^ rotate_left(i1, 3)

    return o1 + o0


def encrypt(plaintext, round_keys):
    pt = bytes_2_bit_array(plaintext)
    assert len(pt) == BLOCK_SIZE

    pt1 = pt[:WORD_SIZE]
    pt0 = pt[WORD_SIZE:]

    for i in range(ROUNDS):
        pt1, pt0 = pt0, bit_map_round_function(pt0, round_keys[i]) ^ pt1

    res = pt0 + pt1
    return res[WORD_SIZE:] + res[:WORD_SIZE]


def gf2elim(matrix):

    m, n = matrix.shape

    i = 0
    j = 0

    while i < m and j < n:
        k = np.argmax(matrix[i:, j]) + i

        temp = np.copy(matrix[k])
        matrix[k] = matrix[i]
        matrix[i] = temp

        aijn = matrix[i, j:]

        col = np.copy(matrix[:, j])

        col[i] = 0

        flip = np.outer(col, aijn)

        matrix[:, j:] = matrix[:, j:] ^ flip

        i += 1
        j += 1

    return matrix


def solve(key_bits, res):
    mat = []

    for i, elem in enumerate(key_bits.bits):
        if type(elem) == gf2_vec:
            mat.append(elem.vec + [elem.const ^ res[i]])

    m_np = np.array(mat, dtype=np.uint8)
    m_np = gf2elim(m_np)

    return [line[-1] for line in m_np]


def get_key_from_pair(pt, ct):
    key_bits = bit_map([gf2_vec([(1 if i == j else 0) for j in range(KEY_SIZE)], 0) for i in range(KEY_SIZE)])
    round_keys = get_key_schedule(key_bits)
    key = None

    idx = 0
    for arr in product(list(range(2)), repeat=16):
        if idx % 128 == 0:
            print(idx)
        idx += 1
        for i in range(ROUNDS):
            round_keys[i][7] = arr[2 * i]
            round_keys[i][3] = arr[2 * i + 1]

        key = solve(encrypt(pt, round_keys), bytes_2_bit_array(ct))
        if cipher(bit_array_2_bytes(key)).encrypt(pt) == ct:
            break

    return key


def test1():
    key = os.urandom(8)
    key_bit_array = bytes_2_bit_array(key)
    c = cipher(key)
    key_bits = bit_map([gf2_vec([(1 if i == j else 0) for j in range(KEY_SIZE)], 0) for i in range(KEY_SIZE)])
    round_keys = get_key_schedule(key_bits)

    for i in range(ROUNDS):
        round_keys[i][3] = c.round_keys[i][3]
        round_keys[i][7] = c.round_keys[i][7]

    for i in range(ROUNDS):
        print(round_keys[i].apply(key_bit_array) == c.round_keys[i])

    x = gf2_vec([1, 1, 0, 1, 0, 0], 0)
    print(x)
    print(x ^ 1)
    print(~x)

    y = bit_map([x, 1, 0, 1, x, x])

    print(y.apply(x.vec))


def test2():
    key = os.urandom(8)
    key_bit_array = bytes_2_bit_array(key)
    c = cipher(key)
    key_bits = bit_map([gf2_vec([(1 if i == j else 0) for j in range(KEY_SIZE)], 0) for i in range(KEY_SIZE)])
    round_keys = get_key_schedule(key_bits)

    round_keys[0][3] = c.round_keys[0][3]
    round_keys[0][7] = c.round_keys[0][7]

    pt = bytes_2_bit_array(b'gabi1010')
    ct = bytes_2_bit_array(c.encrypt(b'gabi1010'))
    print(c.round_keys[0] == round_keys[0].apply(key_bit_array))
    print((bit_map_round_function(pt[WORD_SIZE:], round_keys[0]) ^ pt[:WORD_SIZE]).apply(key_bit_array) == xor(pt[:WORD_SIZE], round_function(pt[WORD_SIZE:], c.round_keys[0])))


def test3():
    key = os.urandom(8)
    key_bit_array = bytes_2_bit_array(key)
    c = cipher(key)
    key_bits = bit_map([gf2_vec([(1 if i == j else 0) for j in range(KEY_SIZE)], 0) for i in range(KEY_SIZE)])
    round_keys = get_key_schedule(key_bits)

    for i in range(ROUNDS):
        round_keys[i][3] = c.round_keys[i][3]
        round_keys[i][7] = c.round_keys[i][7]

    pt = b'gabi1010'
    ct = bytes_2_bit_array(c.encrypt(pt))
    print(encrypt(pt, round_keys).apply(key_bit_array) == ct)


def test4():
    key = os.urandom(8)
    c = cipher(key)

    pt = b'gabi1010'
    ct = c.encrypt(pt)
    k1 = bytes_2_bit_array(key)
    k2 = get_key_from_pair(pt, ct)
    print(k1)
    print(k2)
    print(k1 == k2)


def test5():
    mat = np.array([[0, 1, 1, 1], [1, 1, 1, 0], [0, 0, 1, 0]], dtype=np.uint8)
    print(mat)
    print(gf2elim(mat))


def main():
    # test1()
    # test2()
    # test3()
    test4()
    # test5()


if __name__ == "__main__":
    main()
