import functools
import os
import sys
import string
import binascii
from constants import *
from proof_of_work import proof_of_work
from challenge import cipher


def intro_prompt(action_count):
    print("Randolf: Hey, so... Santa gave us a challenge and we did our best but we need help "
          "from someone experienced with cryptography to check our work!")
    print("Xixen: We need to see if you can encrypt any plaintext if we only give you one plaintext-ciphertext pair")
    print("Randolf: If you do so, hopefully you won't though :), we will reward you with one present from Santa's "
          "personal Christmas tree")
    print(f"Xixen: Now get to work! You'll also have to prove that the exploit is consistent so you will have to do it "
          f"{action_count} times. Good luck!\n")
    sys.stdout.flush()


def invalid_input():
    print("Invalid input, closing the connection\n\n")
    sys.stdout.flush()
    exit(0)


def step_prompt(step, action_count, pt, ct):
    print(f"Step #{step} of {action_count}\nplaintext = {binascii.hexlify(pt)}\nciphertext = {binascii.hexlify(ct)}\n")
    sys.stdout.flush()


def get_hexadecimal_input(input_name):
    print(f"Give me an integer in hexadecimal format\n{input_name} = ", end="")
    sys.stdout.flush()
    user_input = input()
    is_valid = functools.reduce(lambda x, y: x and y, map(lambda x: x in string.hexdigits, user_input))

    if not is_valid:
        invalid_input()

    return binascii.unhexlify(user_input)


def challenge_prompt(chall_pt):
    print(f"Please encrypt this plaintext for us:\n\nplaintext = {binascii.hexlify(chall_pt)}\n")


def main():
    if not proof_of_work(POW_NIBBLES):
        exit(0)

    intro_prompt(ACTION_CNT)

    for i in range(ACTION_CNT):
        key = os.urandom(8)
        chall_cipher = cipher(key)
        pt = os.urandom(8)
        ct = chall_cipher.encrypt(pt)

        step_prompt(i + 1, ACTION_CNT, pt, ct)
        chall_pt = os.urandom(8)
        chall_ct = chall_cipher.encrypt(chall_pt)

        challenge_prompt(chall_pt)
        input_ct = get_hexadecimal_input("ciphertext")

        if chall_ct != input_ct:
            print("Phew, so it's alright then, we were really worried that we messed something up!\n")
            sys.stdout.flush()
            exit(0)
        else:
            if i == ACTION_CNT - 1:
                print("Well done, Here is your special present!\n")
                sys.stdout.flush()
            else:
                print("Good, you can get to the next step!\n")
                sys.stdout.flush()

    print(f"Oh, look! This fell out of the Christmas tree: {FLAG}\n")
    sys.stdout.flush()


if __name__ == '__main__':
    main()
