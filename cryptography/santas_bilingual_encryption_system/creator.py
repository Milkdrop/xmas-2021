import sys
from PIL import Image
import numpy as np

def get_index(letter):
    return ord(letter) - ord('a')

def get_letter_alpha1(idx):
    global src
    y = idx // 8
    x = idx % 8
    return src.crop((128 * x, 128 * y, 128 * (x + 1), 128 * (y + 1)))

def get_letter_alpha2(idx):
    global src
    y = idx // 8 + 4 
    x = idx % 8
    return src.crop((128 * x, 128 * y, 128 * (x + 1), 128 * (y + 1)))

input_data = open(sys.argv[1]).read().split('\n')[:-1]
src = Image.open(sys.argv[2])

cnt = 0
for i, line in enumerate(input_data):
    print(f'{i + 1} / {len(input_data)}')
    dst = Image.new("L", (128 * len(line), 128))
    
    for j, letter in enumerate(line):
        if letter == ' ':
            dst.paste(get_letter_alpha1(26), (128 * j, 0))
        else:
            if cnt & 1 == 0:
                dst.paste(get_letter_alpha1(get_index(letter)), (128 * j, 0))
            else:
                dst.paste(get_letter_alpha2(get_index(letter)), (128 * j, 0))

            cnt += 1

    dst.save(f'output_image_line#{i}.png')
