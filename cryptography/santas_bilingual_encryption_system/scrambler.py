import sys
import string

def get_index(letter):
    return ord(letter) - ord('a')

def cycle_letter(letter, shift):
    return chr(ord('a') + (get_index(letter) + shift) % 26)

input_data = open(sys.argv[1], "r").read()
output_file = open(sys.argv[2], "w")

cnt = 0
for i in range(len(input_data)):
    if input_data[i] in "\n ":
        output_file.write(input_data[i])
    else:
        if cnt & 1 == 0:
            output_file.write(input_data[i])
        else:
            output_file.write(cycle_letter(input_data[i], get_index(input_data[i - 1])))
        cnt += 1

output_file.close()
