#!/usr/bin/env python3
from time import sleep
import requests

enc = bytearray(bytes.fromhex("6f4027fa347a96dc9ea1f9c55d3cb5c069070d2a3288000d5388d77999ded8f9531452d4809d78f5143df28b9d11065e068917c5064ce9758bc08b205da20f9841d9f92649141186efd0843a68e4e79b"))

session = requests.Session()

for i in range(256):
    enc[-17] = i
    # print(f'Sending {enc.hex()}')
    r = session.post('http://localhost:3000/storage', json={'store': enc.hex()})
    print(r.text)

r = session.get('http://localhost:3000/storage')
print(r.text)
