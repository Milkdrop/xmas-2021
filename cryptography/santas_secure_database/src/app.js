import createError from 'http-errors'
import session from 'express-session'
import express from 'express'
import cookieParser from'cookie-parser'
import sqlite3 from 'sqlite3'
import { open } from 'sqlite'
import bodyParser from'body-parser'
import aesjs from 'aes-js'
import crypto from 'crypto'

const BLOCK_SIZE = 16;

const aesKey = crypto.randomBytes(16);
const aesIv = crypto.randomBytes(16);

console.log(`${aesKey.toString('hex')}`)
console.log(`${aesIv.toString('hex')}`)

const FLAG = "X-MAS{7h3_0r4cl3_0f_64y_b335_15_pl3453d_50912jd1f}";

const concatArrays = (array1, array2) => {
  console.log(array1)
  var tmp = new Uint8Array(array1.byteLength + array2.byteLength);
  tmp.set(new Uint8Array(array1), 0);
  tmp.set(new Uint8Array(array2), array1.byteLength);
  return tmp;
}

const pkcs7PadBytes = (bytestring) => {
  if (bytestring.length === BLOCK_SIZE) {
    return bytestring;
  }

  const requiredPadding = 16 - bytestring.length;

  const paddingArray = new Uint8Array(Array.from({length: requiredPadding}, () => requiredPadding));

  return concatArrays(bytestring, paddingArray);
}

const padMessage = (message) => {
  if (message.length % BLOCK_SIZE === 0) {
    return concatArrays(message, new Uint8Array(Array.from({length: BLOCK_SIZE}, () => BLOCK_SIZE)));
  }

  const partialBlockOffset = Math.floor(message.length / BLOCK_SIZE) * BLOCK_SIZE;
  const partialBlock = message.slice(partialBlockOffset);
  return concatArrays(message.slice(0, partialBlockOffset), pkcs7PadBytes(partialBlock));
}

const generateEncFlag = () => {
  const flagBytes = padMessage(aesjs.utils.utf8.toBytes(FLAG));

  const aesCbc = new aesjs.ModeOfOperation.cbc(aesKey, aesIv);
  const encryptedBytes = aesCbc.encrypt(flagBytes);

  const encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);

  return encryptedHex;
}

const ENCRYPTED = generateEncFlag();
console.log(`Encrypted flag: ${aesjs.utils.hex.fromBytes(aesIv).concat(ENCRYPTED)}`)

class DevError extends Error {
  constructor(...args) {
    super(...args);
    Error.captureStackTrace(this, DevError);
  }
};

class DbError extends Error {
  constructor(...args) {
    super(...args);
    Error.captureStackTrace(this, DbError);
  }
};

const db = await open({filename: '/tmp/storage.db', driver: sqlite3.Database })
console.log('Connected to the database');

await db.run('DROP TABLE IF EXISTS keys');
await db.run('CREATE TABLE IF NOT EXISTS keys(session text, key text)');

const storeKey = async (session, key) => {
  await db.run(
    `INSERT INTO keys(session, key)
     VALUES(?, ?)`, [session, key]
  );
}

const getKeys = async (session) => {
  return await db.all(
    `SELECT key
     FROM keys
     WHERE session = ?`, session);
}

const sessionInitializer = async (req, res, next) => {
  if (req.session.hasFlag === undefined) {
    req.session.hasFlag = true;
    console.log(`Storing flag for ${req.sessionID}`)
    await storeKey(req.sessionID,
                   aesjs.utils.hex.fromBytes(aesIv).concat(ENCRYPTED));
  }
  next();
}

const app = express();

app.use(session({
  secret: process.env.COOKIE_SECRET,
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false, maxAge: 15 * 60 * 1000 }
}));

app.use(sessionInitializer);
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.json())

const router = express.Router();

const pkcs7UnpadBytes = (bytestring) => {
  const paddingLength = bytestring[bytestring.length - 1];

  if (paddingLength > BLOCK_SIZE || paddingLength == 0) {
    throw new DbError("Invalid data")
  }

  const padding = bytestring.slice(BLOCK_SIZE - paddingLength);

  for(const pad of padding) {
    if (pad != paddingLength) {
      throw new DbError("Invalid data")
    }
  }

  return bytestring.slice(0, BLOCK_SIZE - paddingLength);
}

const getPaddedBlockOffset = (message) => {
  if (message.length % BLOCK_SIZE === 0) {
    return message.length - BLOCK_SIZE;
  } else {
    return Math.floor(message.length / BLOCK_SIZE) * BLOCK_SIZE;
  }
}

const unpadMessage = (message) => {
  const partialBlockOffset = getPaddedBlockOffset(message);
  const partialBlock = message.slice(partialBlockOffset);
  return concatArrays(message.slice(0, partialBlockOffset), pkcs7UnpadBytes(partialBlock));
}

router.post('/storage', async (req, res, next) => {
  const store = String(req.body.store);

  if (store === undefined) {
    next(new DbError("Missing store"));
    return;
  }

  if (store.length < (BLOCK_SIZE * 4) || (store.length % (BLOCK_SIZE * 2)) != 0) {
    next(new DbError("Invalid length"));
    return;
  }

  const encryptedBytes = aesjs.utils.hex.toBytes(store);

  const storeIv = encryptedBytes.slice(0, BLOCK_SIZE);
  const storeData = encryptedBytes.slice(BLOCK_SIZE);

  const aesCbc = new aesjs.ModeOfOperation.cbc(aesKey, storeIv);
  const decryptedBytes = aesCbc.decrypt(storeData);

  try {
    unpadMessage(decryptedBytes)
    await storeKey(req.sessionID, store)
    res.send('OK');
  } catch (e) {
    if (e instanceof DbError) {
      next(e)
    } else {
      throw e;
    }
  }
});

router.get('/storage', async (req, res, next) => {
  const stored = await getKeys(req.sessionID);
  res.send(stored);
})

router.get('/', async (req, res, next) => {
    res.send(`<h1>Totally legitimate password manager©™ V2.</h1>
            <h3> So secure that the entire database is encrypted with a key only I, </h3>
            <h3> the greatest javascript programmer that has ever lived, know </h3>
            <br>
            <h3> This time I will not give you the source code. Everyone knows security through obscurity is best!</h3>
            <h3> You can attempt to hack this one, but all your attempts are futile, as you cannot even add entries </h3>
            <h3> to the database without knowing the key!11!111!1!!! </h3>
            <h3> Try a GET on /storage to view your current entries </h3>
            <h3> Try a POST on /storage with a json object formatted like this {'store': <pre>'&lt;hex encoded string&gt;'</pre>} to add an entry </h3>
    `)
});

app.use('/', router);

app.use((req, res, next) => {
  next(createError(404));
});

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  if (err instanceof DbError) {
    res.send(err.message)
  } else {
    res.send('Not found')
  }
});

app.listen(2000, () => {
  console.log("Listening on 2000")
})
