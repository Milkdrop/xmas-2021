from pwn import *
from Crypto.Util.number import inverse
from proof_of_work_solver import *
from functools import reduce

def read_menu():
    s.recvline()
    s.recvline()
    s.recvline()
    s.recvline()
    s.recvline()


def chinese_remainder(n, a):
    sum = 0
    prod = reduce(lambda a, b: a*b, n)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        sum += a_i * mul_inv(p, n_i) * p
    return sum % prod


def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1: return 1
    while a > 1:
        q = a // b
        a, b = b, a%b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0: x1 += b0
    return x1


def factor(n):
    factors = []
    i = 2
    while i <= n:
        if i * i > n:
            i = n

        if n % i == 0:
            factors.append([i, 0])

            while n % i == 0:
                n //= i
                factors[-1][-1] += 1

        i += 1

    return factors


def get_gen_size(x, n, phi, primes):
    p = 1
    for prime in primes:
        if pow(x, phi//prime, n) == 1:
            p*=prime
    return phi // p


def query(x, d):
    global counter
    counter += 1
    resp = None

    read_menu()

    if x == "k":
        s.sendline(b"2")
        s.recvline()
        s.sendline(str(d).encode())
        resp = s.recvline().strip().split()[-1][2:-1]
    else:
        s.sendline(b"1")
        s.recvline()
        s.sendline(str(x).encode())
        s.recvline()
        s.sendline(str(d).encode())
        resp = s.recvline().strip().split()[-1][2:-1]
    s.recvline()
    return resp


def birthday_attack(n, g, q):
    c = phi // q
    ek = query("k", c)
    if ek == identity:
        return 0

    Sg = {0:identity}
    Sk = {0:identity, 1: ek}

    for a in range(1, q):
        eg = query(g, c * a)
        if eg in Sk:
            return inverse(Sk[eg], q) * a % q
        else:
            Sg[eg] = a

        ek = query("k", c * a)
        if ek in Sg:
            return inverse(a, q) * Sg[ek] % q
        else:
            Sk[ek] = a

    return None


def send_guess(guess_k):
    read_menu()
    s.sendline("3".encode())
    s.recvline()
    s.sendline(str(guess_k).encode())
    s.recvline()
    s.recvline()


# Main code
s = remote('127.0.0.1', 1030)
x = s.recv().strip().split()[-1]
resp = pow_solver(x)
s.sendline(resp)

s.recvline()
s.recvline()
steps = int(s.recvline().strip().split()[5])
print(f"steps: {steps}")
s.recvline()

max_counter = 0

for i in range(steps):
    print(f"step #{i + 1}")
    counter = 0

    s.recvline()
    s.recvline()
    s.recvline()
    
    n = int(s.recvline().strip().split()[-1])
    print(f"n = {n}")
    phi = n - 1
    primes = [a[0] for a in factor(phi)]
    remainders = []
    gen = None
    i = 2
    while gen == None:
        if get_gen_size(i, n, phi, primes) == phi:
            gen = i
        i += 1
    print(f"gen = {gen}")

    k=s.recvline().strip().split()[-1][2:-1]
    s.recvline()
    print(f"k = {k}")

    identity = query(1, 1)


    for prime in primes:
        remainders.append(birthday_attack(n, gen, prime))
        print(f"d_{prime} = {remainders[-1]}")

    max_counter = max(max_counter, counter)
    print(f"queries done: {counter}")

    pw = chinese_remainder(primes, remainders)
    guess_k = pow(gen, pw, n)
    send_guess(guess_k)

print(f"max queries done: {max_counter}")
s.interactive()
