import os
import binascii
from hashlib import sha256


def pow_solver(suffix):
    suff_len = len(suffix)
    candidate = binascii.hexlify(os.urandom(16))
    while sha256(binascii.unhexlify(candidate)).hexdigest()[-suff_len:].encode() != suffix:
        candidate = binascii.hexlify(os.urandom(16))

    return candidate
