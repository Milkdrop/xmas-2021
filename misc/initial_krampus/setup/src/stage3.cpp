#include <stdint.h>
#include <utility>
#include <unistd.h>
#include <seccomp.h>




extern std::pair <int, int> bot1000000007(uint8_t board[8][8]);

uint8_t board[8][8];

int main() {
    scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_KILL);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 0);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 0);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit_group), 0);
    seccomp_load(ctx);

    read(888, board, 64);

    std::pair <int, int> indicated_coin = bot1000000007(board);

    int x = indicated_coin.first, y = indicated_coin.second;
    write(666, &x, sizeof(int));
    write(666, &y, sizeof(int));
    return 0;
}
