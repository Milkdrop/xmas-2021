<?php
if (!file_exists("canvas.png") || filemtime("canvas.png") < time() - 5) {
    $conn = new mysqli("localhost", 'HTsP', 'HTsPAccessKey', "paint_db");

    $buffer = "";

    $result = mysqli_query($conn, "SELECT x, y, r, g, b, team FROM pixels");
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $buffer .= implode(" ", $row) . "\n";
    }

    file_put_contents("pixels.txt", $buffer);
    exec("python3 create_canvas.py");
}
?>

<head>
    <title>P.A.I.N.T.</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="window-container">
        <div class="window">
            <div class="title">
                <img src="img/title_left.png"/>
                <img src="img/title_right.png"/>
            </div>

            <div class="canvas">
                <img style="margin:6px" src="canvas.png?t=<?php echo time(); ?>"/>
            </div>

            <div class="bottom">
                <img src="img/bottom_left.png"/>
                <img src="img/bottom_right.png"/>
            </div>
        </div>
    </div>
</body>