<?php

session_start();

function clamp_to_15bit($val) {
    return floor(floor(($val / 255) * 31) * 255 / 31);
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if ($_POST['action'] == "get_work") {
        $_SESSION['salt'] = bin2hex(random_bytes(32));
        $string = bin2hex(random_bytes(16));
        $_SESSION['hash_start'] = substr(md5($_SESSION['salt'] . $string), 0, 6);

        echo "Your salt is " . $_SESSION['salt'] . ". Find a string that starts with this salt and whose md5 hash starts with: " . $_SESSION['hash_start'];
    } else if ($_POST['action'] == 'paint') {
        if (!isset($_SESSION['salt'])) {
            echo "You haven't POSTed with action=get_work first!";
        } else if (!isset($_POST['x']) || !isset($_POST['y']) || !isset($_POST['color']) || !is_numeric($_POST['x']) || !is_numeric($_POST['y'])) {
            echo 'Invalid values for x, y or color';
        } else if (is_string($_POST['work']) && isset($_SESSION['salt']) && strcmp(substr(md5($_SESSION['salt'] . $_POST['work']), 0, 6), $_SESSION['hash_start']) === 0) {
            $x = intval($_POST['x']);
            $y = intval($_POST['y']);
            $color = $_POST['color'];
            
            if (isset($_POST['team']) && is_numeric($_POST['team']) && intval($_POST['team']) > 0 && intval($_POST['team']) < 100000) {
                $team = intval($_POST['team']);
            } else {
                $team = 0;
            }
            
            if ($x < 0 || $x >= 320 || $y < 0 || $y >= 240) {
                die('The image is 320x240!');
            }
            
            if (strlen($color) != 6) {
                die('Colors are in HEX format: 001122 !');
            }

            $r = clamp_to_15bit(hexdec(substr($color, 0, 2)));
            $g = clamp_to_15bit(hexdec(substr($color, 2, 2)));
            $b = clamp_to_15bit(hexdec(substr($color, 4, 2)));
            
            $conn = new mysqli("localhost", 'HTsP', 'HTsPAccessKey', "paint_db");
            $stmt = $conn->prepare("INSERT INTO pixels(x, y, r, g, b, team) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE r = ?, g = ?, b = ?, team = ?");
            $stmt->bind_param("iiiiiiiiii", $x, $y, $r, $g, $b, $team, $r, $g, $b, $team);
        
            if ($stmt->execute() === TRUE) {
                echo "Successfully set pixel ($x, $y) to the color ($r, $g, $b)! This pixel now belongs to team #$team!";
                
                $_SESSION['salt'] = '';
                $_SESSION['hash_start'] = '';
                session_unset();
            } else {
                echo "Error setting pixel.";
            }
        } else {
            echo "Work is invalid! You submitted: " . $_POST['work'] . "\n";
            echo "The md5 hash of \"" . $_SESSION['salt'] . "\" + work is \"" . md5($_SESSION['salt'] . $_POST['work']) . "\"\n";
            echo "... which does not start with: " . $_SESSION['hash_start'];
        }
    }
} else {
    echo "<h1>Hello! And welcome to the P.A.I.N.T. api.</h1>
        - The canvas is a <b>15-bit 320x240</b> image.<br>
        - In order to paint on it, you will first need to do a <b>POST</b> request to /api with only one POST parameter: <b>action=get_work</b>.<br><br>
        - That will give you a <b>salt</b> and an <b>md5 hash</b> saved in your PHP Session. You will need to find a <b>STRING</b> that starts with the salt and whose md5 hash starts with the characters indicated by the API.<br><br>

        - Then, to paint you will need to do a POST request to /api with the following parameters: <b>action=paint&work=STRING&x=X&y=Y&color=RRGGBB</b>
        to put a pixel with the color RRGGBB (in hex, gets converted to a 16bit color automatically) at coordinates X, Y in the image. The <b>work</b> parameter is the <b>STRING</b> you found after doing the work from the
        previous POST request. Don't forgot to have the same PHP SESSION ID in both requests!<br><br>
        
        Good luck :)<br><br>

        P.S. You can add a <b>team=</b> parameter to your action=paint POST request in order to assign your CTFx team ID to the pixel you drew (so we can know who drew what, for the art competition). Your CTFx team ID can be seen in the URL of your public profile.
    ";
}