CREATE DATABASE paint_db;
USE paint_db;

CREATE TABLE pixels(x INT, y INT, r INT, g INT, b INT, team INT, PRIMARY KEY(x, y));
GRANT SELECT, INSERT, UPDATE ON paint_db.pixels TO 'HTsP'@'localhost' identified by 'HTsPAccessKey';