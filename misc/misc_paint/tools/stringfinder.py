import random
import string, hashlib

salt = "efae9a918bf16f5a69bef8fecae21246ff7aa5ab10ffefd8128f0aefb28131ed"
target = "e4564b"

found = False
while not found:
    s = ''.join(random.choices(string.ascii_uppercase + string.digits, k=16))
    hsh = hashlib.md5((salt + s).encode()).hexdigest()
    if (hsh[:6] == target):
        print("FOUND! " + hsh + ", " + s)
        found = True