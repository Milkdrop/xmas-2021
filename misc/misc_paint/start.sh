service php7.4-fpm start

mysqld_safe &

while ! mysql -e "show databases;"; do # Wait for MariaDB
	sleep 0.25
done

mysql < /setup.sql

nginx

echo "Docker online!"
while true; do sleep 1000; done