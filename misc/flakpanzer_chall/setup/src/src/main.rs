#![feature(panic_info_message)]

use core::panic;
use rand;
use std::fmt::{self, Display};
use std::io::{self, Write};
use std::ops::{Add, Sub};
/// Constant used for determining wheter two vectors are `close enough`
/// If the distance between the `heads` of the vectors is lower than
/// this treshold, then they are considered to be `close enough` for us
/// In this challenge specifically this is wheter a high explosive shell
/// has hit an airplane, so this is fitting
const VEC_COMPARE_PRECISION: f64 = 2.0;

/// Vector of 3 64 bit floating point values used to represent
/// the position of actors in the world
#[derive(Debug, Clone, Copy, Default)]
struct Vector2 {
    x: f64,
    y: f64,
}

/// Implementation of `Display` trait for `Vector3`
impl Display for Vector2 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

/// `Overload` the `+` operator for Vector3
impl Add for Vector2 {
    type Output = Vector2;

    fn add(self, rhs: Self) -> Self::Output {
        Self::Output {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

/// `Overload` the `-` operator for Vector3
impl Sub for Vector2 {
    type Output = Vector2;

    fn sub(self, rhs: Self) -> Self::Output {
        Self::Output {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl Vector2 {
    /// Creates a new `Vector3` using the specified coordinates
    fn new(x: f64, y: f64) -> Self {
        Self { x, y }
    }

    /// Computes the `magnitude` (or `length`) of the vector
    fn magnitude(&self) -> f64 {
        (self.x * self.x + self.y * self.y).sqrt()
    }

    /// Returns a vector pointing from the current vector
    /// towards the `other` vector
    fn towards(&self, other: &Self) -> Self {
        *other - *self
    }

    /// Returns the distance between two vectors
    fn distance_to(&self, other: &Self) -> f64 {
        self.towards(other).magnitude()
    }

    /// Returns the `yaw` (rotation around the `Z` axis) of the vector
    /// in radians
    // fn yaw(&self) -> f64 {
    //     self.y.atan2(self.x)
    // }

    /// Returns a vector having the same direction as the current vector
    /// with a magnitude of 1
    fn direction(&self) -> Self {
        self.scaled(1.0 / self.magnitude())
    }

    /// Returns a copy of the current vector with all the coordinates scaled
    /// by the `scale` parameter
    fn scaled(&self, scale: f64) -> Self {
        Self {
            x: self.x * scale,
            y: self.y * scale,
        }
    }

    /// Arbitrary check if two vectors are close enough to each other
    fn close_enough(&self, other: &Self) -> bool {
        self.distance_to(other) < VEC_COMPARE_PRECISION
    }

    // fn close_precise(&self, other: &Self) -> bool {
    //     self.distance_to(other) < 0.0000001
    // }

    // fn x(&self, x: f64) -> Self {
    //     Self { x, y: self.y }
    // }

    // fn y(&self, y: f64) -> Self {
    //     Self { x: self.x, y }
    // }
}

/// `Plane` structure
/// Consists of a position, direction, and speed
#[derive(Debug, Clone, Copy)]
struct Plane {
    // Uniquely identifies the plane inside a level
    id: usize,

    // Position of the plane in the world
    pos: Vector2,

    // Direction the plane is heading
    dir: Vector2,

    // Speed of the plane
    speed: f64,

    // Target
    target: Vector2,
}

impl Plane {
    /// Creates a new plane with zero speed and no direction
    fn new(pos: Vector2, id: usize) -> Self {
        Self {
            id,
            pos,
            dir: Vector2::default(),
            speed: 0.0,
            target: Vector2::default(),
        }
    }

    /// Sets the speed of the plane
    fn speed(mut self, speed: f64) -> Self {
        self.speed = speed;

        self
    }

    /// Returns the position of the plane as if the plane had
    /// `dt` units of time to move
    fn speculate_position(&self, dt: f64) -> Vector2 {
        self.pos + self.dir.scaled(self.speed * dt)
    }

    fn target(mut self, target: Vector2) -> Self {
        self.dir = self.pos.towards(&target).direction();
        self.target = Vector2::new(target.x, target.y);

        self
    }
}

/// Specifies the type of the level, the amount of ammo given,
/// and the amount of planes that will be present in a level
#[derive(Debug)]
struct LevelValues {
    ammo: usize,
    attacking_planes: usize,
}

/// Master game struct
#[derive(Debug)]
struct Game {
    // Position of the player (should always be 0,0,0)
    player: Vector2,

    // Radius of the circle the planes are not allowed to
    // fly in, requiring to be shot before they reach this
    pole_area: f64,

    // Area in which the planes get set their target this should
    // be smaller than the pole area to avoid some weirdness
    target_area: f64,

    // Radius of the circle the planes spawn on, each plane will
    // spawn on the edge of the circle in a random position
    spawn_area: f64,

    // Vector holding the enemy planes
    enemies: Vec<Plane>,

    // Vector holding the level configurations
    levels: Vec<LevelValues>,

    // Maximum distance the bullets will reach
    bullet_range: f64,

    // Distance the bullet travels in one time unit
    // Bullets should be fast enough to hit any plane before
    // it reaches the pole zone
    bullet_speed: f64,
}

impl Game {
    /// Creates a new game
    fn new(
        player: Vector2,
        pole_area: f64,
        spawn_area: f64,
        levels: Vec<LevelValues>,
        bullet_range: f64,
    ) -> Self {
        let enemies = Vec::<Plane>::new();
        let bullet_speed = 900.0;
        let target_area = pole_area / 2.0;
        Self {
            player,
            pole_area,
            target_area,
            spawn_area,
            enemies,
            levels,
            bullet_range,
            bullet_speed,
        }
    }

    /// Generates a point inside the `pole_area` circle
    /// Used as targets for the planes
    fn gen_point_inside_area(&self) -> Vector2 {
        // Random angle in the interval [0, PI)
        let angle = rand::random::<f64>() * std::f64::consts::PI;

        // Random distance in the interval [0, target_area)
        let distance = rand::random::<f64>() * self.target_area;

        let point = Vector2::new(angle.cos(), angle.sin()).scaled(distance);

        // Sanity check that the point is actually inside the target area
        assert!(
            point.towards(&self.player).magnitude() < self.target_area + 0.1,
            "Error 64502. Alert Livian#4965 on Discord!"
        );

        point
    }

    /// Checks if there are any enemies left on the playing field
    /// Returns true if that that is the case
    fn check_enemies_left(&self) -> bool {
        self.enemies.len() > 0
    }

    /// Generates a random plane speed such that it is impossible
    /// for the plane to get in the range of the cannon in a unit of time
    fn get_random_speed(&self) -> f64 {
        rand::random::<f64>() * (self.spawn_area - self.bullet_range * 1.1) / 2.0
            + self.bullet_range * 0.1
    }

    /// Generates a point on the circle of radius `spawn_area`
    /// Used for the plane starting locations
    fn get_random_start_pos(&self) -> Vector2 {
        // Random angle in the interval [0, PI)
        let angle = rand::random::<f64>() * std::f64::consts::PI;

        let point = Vector2::new(angle.cos(), angle.sin()).scaled(self.spawn_area);

        // Sanity check that the point is actually on the circle
        assert!(
            point.towards(&self.player).magnitude() > (self.spawn_area - 0.01),
            "Error 19205. Alert Livian#4965 on Discord."
        );

        point
    }

    /// Prints to the screen the current position and the future position (after
    /// half a unit of time) of each plane in the level
    /// Both the current and future positions are required to determine the speed
    /// and direction of each plane
    fn announce_enemies(&self, lvl: usize) {
        // Print the amount of enemies for the level
        println!("No. of planes detected: {}", self.enemies.len());

        // Print the available ammo for the level
        println!("Available ammo: {}", self.levels[lvl].ammo);
        println!("Incoming plane positions:");

        for plane in &self.enemies {
            println!(
                "{}: {} -> {}",
                plane.id,
                plane.pos,
                plane.speculate_position(0.5)
            );
        }
        std::io::stdout().flush().expect("Failed to flush :(");
    }

    fn debug_solve_easy_level(&mut self) {
        let enemy_planes = self.enemies.clone();

        let speculation_time: f64 = 0.5;
        let shootdown_distance: f64 = self.pole_area + (self.bullet_range - self.pole_area) / 2.0;
        // let shootdown_distance: f64 = 1.1;

        for p in &enemy_planes {
            let pos = p.pos;
            let delta = p.pos.towards(&p.speculate_position(speculation_time));

            // Pinkie "Facem un artificiu" Pie1189
            let d = 4.0 * ((pos.x * delta.x + pos.y * delta.y) / speculation_time).powi(2)
                - 4.0
                    * ((delta.x / speculation_time).powi(2) + (delta.y / speculation_time).powi(2))
                    * (pos.x.powi(2) + pos.y.powi(2) - shootdown_distance.powi(2));

            let t1 = (-2.0 * ((pos.x * delta.x + pos.y * delta.y) / speculation_time) + d.sqrt())
                / (2.0
                    * ((delta.x / speculation_time).powi(2)
                        + (delta.y / speculation_time).powi(2)));

            let t2 = (-2.0 * ((pos.x * delta.x + pos.y * delta.y) / speculation_time) - d.sqrt())
                / (2.0
                    * ((delta.x / speculation_time).powi(2)
                        + (delta.y / speculation_time).powi(2)));

            let shoot_candidate1 = p.speculate_position(t1);
            let shoot_candidate2 = p.speculate_position(t2);

            // println!("{}", pos);
            // println!("{}", p.speculate_position(speculation_time));
            // println!("{}", shoot_candidate1);
            // println!("{}", shoot_candidate2);
            // println!("Times: {} {}", t1, t2);
            // println!("d: {}", d);

            let dist1 = self.player.distance_to(&shoot_candidate1);
            let dist2 = self.player.distance_to(&shoot_candidate2);
            assert!(
                dist1 < shootdown_distance + 0.0001 && dist1 > shootdown_distance - 0.0001,
                "Error 19205. Alert Livian#4965 on Discord."
            );

            assert!(
                dist2 < shootdown_distance + 0.0001 && dist2 > shootdown_distance - 0.0001,
                "Error 19205. Alert Livian#4965 on Discord."
            );

            assert!(
                dist1 < self.bullet_range && dist2 < self.bullet_range,
                "Error 84091. Alert Livian#4965 on Discord."
            );

            assert!(
                dist1 > self.pole_area && dist2 > self.pole_area,
                "Error 21784054. Alert Livian#4965 on Discord."
            );

            let shootown_position = if t1 > t2 {
                shoot_candidate2
            } else {
                shoot_candidate1
            };

            let dbg_aim_vec = self.player.towards(&shootown_position);
            // let dbg_yaw = dbg_aim_vec.yaw().to_degrees();
            let dbg_distance = dbg_aim_vec.magnitude();
            let dbg_delay = pos.towards(&shootown_position).magnitude() / p.speed
                - dbg_distance / self.bullet_speed;

            assert!(
                dbg_delay > 0.0,
                "Error 2385004. Alert Livian#4965 on Discord."
            );

            // println!("{} {} {}", dbg_yaw, dbg_distance, dbg_delay);
        }
    }

    /// Main level loop
    /// Takes the user input, parsing it into parameters for the cannon and
    /// destroying any planes caught in the blast of the shell
    fn level_loop(&mut self, lvl: usize) {
        // Loop over the number of shells
        for _ in 0..self.levels[lvl].ammo {
            // User input
            let mut user_input = String::new();

            // Check if reading input was successful
            io::stdin()
                .read_line(&mut user_input)
                .expect("Failed to read user input");

            // Parse the `yaw`, `distance` and `delay` from the user input
            // Those values should be given by the user in degrees
            let (yaw, distance, delay) =
                match &user_input.trim().split(" ").collect::<Vec<&str>>()[..] {
                    &[yaw, distance, delay] => (
                        yaw.parse::<f64>().expect("Invalid input").to_radians(),
                        distance.parse::<f64>().expect("Invalid input"),
                        delay.parse::<f64>().expect("Invalid input"),
                    ),
                    _ => {
                        panic!("Invalid input");
                    }
                };

            // Try to shoot down a plane
            let mut destroyed = self.try_shoot(yaw, distance, delay);

            // Sort the `destroyed` indices in decreasing order
            // so that removing the elements does not affect the
            // other indices
            destroyed.sort_by(|a, b| b.cmp(a));

            // Remove destroyed planes from the battlefield
            for idx in destroyed {
                self.enemies.swap_remove(idx);
            }

            // We do not break out of the loop because that would be
            // pretty annoying and unpredictable, making interacting
            // with the game programatically harder
        }
    }

    /// Spawn enemies for the easy levels
    /// Spawns separat planes having random targets,
    /// should have enough ammo to shoot each one down individually
    fn spawn_easy_enemies(&mut self, lvl: usize) {
        // Spawn the `real` planes to the array
        // These planes actually target Santa
        for i in 0..self.levels[lvl].attacking_planes {
            let bomber = Plane::new(self.get_random_start_pos(), i)
                .target(self.gen_point_inside_area())
                .speed(self.get_random_speed());

            self.enemies.push(bomber);
        }
    }

    /// Main game loop
    /// Iterates over all levels, spawns the required planes,
    /// prints the positions of the planes and enters the level loop
    fn play_levels(&mut self) {
        for i in 0..self.levels.len() {
            // Print out the current level
            println!("Level {} out of {}:", i, self.levels.len() - 1);

            // Check the level type and spawn the required enemies
            self.spawn_easy_enemies(i);

            // Check if the leve is actually solvable
            self.debug_solve_easy_level();

            // Print out the current and future positions of the planes
            self.announce_enemies(i);

            // Enter the level loop (read -> parse -> shoot)
            self.level_loop(i);

            // If there enemies left after we ran out of ammo we lose
            if self.check_enemies_left() {
                panic!("You failed :(");
            }
        }
        println!("You vanquished the enemy!");
        println!("Here is your prize: X-MAS{{4NY_PR0bl3m_c4n_B3_S0lv3d_W17h_4_b16_3n0u6H_C4NN0N_hj9jh98j94}}");
        std::io::stdout().flush().expect("Failed to flush :(");
    }

    fn try_shoot(&mut self, yaw: f64, distance: f64, delay: f64) -> Vec<usize> {
        let mut destroyed = Vec::<usize>::new();

        // Construct the vector representing the bullet's trajectory
        let aim_vec = Vector2::new(yaw.cos(), yaw.sin()).scaled(distance);

        // Move the origin of the aim vector into the player panzer position
        let aim_pos = aim_vec + self.player;

        if aim_pos.magnitude() < self.pole_area || aim_pos.magnitude() > self.bullet_range {
            return destroyed;
        }

        // Iterate over all planes
        for (i, plane) in (&self).enemies.iter().enumerate() {
            // Compute the required travel time of the bullet
            let travel_time = aim_pos.magnitude() / self.bullet_speed;

            // Check if the plane has been hit after speculating on the position
            // of the plane
            if plane
                .speculate_position(delay + travel_time)
                .close_enough(&aim_pos)
            {
                println!("Hit plane {}!", plane.id);
                std::io::stdout().flush().expect("Failed to flush :(");
                destroyed.push(i);
            }
        }

        destroyed
    }
}

fn main() {
    std::panic::set_hook(Box::new(move |info: &std::panic::PanicInfo| {
        if let Some(message) = info.payload().downcast_ref::<&str>() {
            println!("{}", message);
            std::io::stdout().flush().expect("Failed to flush :(");
        } else {
            println!("Error 69420. Alert Livian#4965 on Discord!");
            std::io::stdout().flush().expect("Failed to flush :(");
        }
    }));
    let mut _scratch_string = String::new();
    // Magic constants
    let pole_radius = 1000.0;
    let spawn_circle_radius = 2000.0;
    let cannon_reach_radius = 1300.0;

    // Print out a welcome message
    println!("You are enjoying the view of Lapland's vast snowy mountains...");
    println!("A very faint buzzing noise can be heard from every direction.");
    println!("The Elf appointed to you by Santa himself for this journey looks at you with a very worried face...");
    println!("His eye pupils grew to such a size that looking at them made you feel like being swallowed by the abyss...");
    println!("A single word was muttered from the elf's mouth...");
    println!("planes...");
    println!("You take a look at the sky, but because you forgot your hacker shades, you are unable to see much.");
    println!("You need to help the elf regain his composure.");
    println!("<Press the enter key to shake the elf>");
    std::io::stdout().flush().expect("Failed to flush :(");
    std::io::stdin()
        .read_line(&mut _scratch_string)
        .expect("Failed to read :(");

    println!("You put both of your hands on the elf's shoulders and you shake the living eggnog out of him...");
    println!("He did indeed recover after a good shaking.");
    println!("But that still has not solved all of you problems...");
    println!("You still have to deal with the fact that a ton of planes are approaching Lapland!");
    println!(
        "Due to plot reasons, both of you forgot your walkie talkies back at Santa's mansion..."
    );
    println!("And are too far away to run back there and alert everyone...");
    println!("You need to take this matter into your own hands!");
    println!("And that's also what the elf thought...");
    println!("He tells you to follow him and starts running...");
    println!("You start following him, and after a bit of running you arrive at a strange looking machine...");
    println!("The machine seems to have some sort of a control panel, but the elf is unable to reach it...");
    println!("<Press the enter key to pick up the elf>");
    std::io::stdout().flush().expect("Failed to flush :(");
    std::io::stdin()
        .read_line(&mut _scratch_string)
        .expect("Failed to read :(");
    println!("You pick up the elf and hold him up to the control panel...");
    println!("He types some commands and tells you to take a look...");
    println!("This machine is Santa's modified Flakpanzerkampfwagen...");
    println!(
        "It is capable of shooting down anything that flies, if you know how to control it..."
    );
    println!("A coordinate system appears on the display...");
    println!(
        r#"
                ┃+y
                ┃
                ┃
                ┃
                ┃
    -x          ┃(0,0)     +x
    ━━━━━━━━━━━━╋━━━━━━━━━━━━
                ┃
                ┃
                ┃
                ┃
                ┃
                ┃-y"#
    );
    println!("The elf explains to you that you are currently at the coordinates (0, 0).");
    println!("Coordinates are formatted of pairs of floating point values, the first being the X coordinate,");
    println!("and the second being the Y coordinate.");
    println!("This machine has powerful ElfTek UPU's (Universe Processing Unit), and is pretty much able to");
    println!("predict the future.");
    println!(
        "The screen turns red and the elf says some of the vilest swear words you ever heard..."
    );
    println!("The Elftek AutoTargeting module is broken. The machine cannot aim without external assitance.");
    println!("The elf informs you that he failed most of his classes in elf school, and asks you if you faired any better.");
    println!("<Press the enter key to say yes>");
    std::io::stdout().flush().expect("Failed to flush :(");
    std::io::stdin()
        .read_line(&mut _scratch_string)
        .expect("Failed to read :(");
    println!("'Great', the elf said, as he started to explain the inner workings of the targeting system...");
    println!("The machine will inform you about the number of enemy aircraft, your ammunition count, and the");
    println!("Present and FUTURE, yea you heard that right, FUTURE positions of the aircraft.");
    println!("The ElfTek UPU's will compute all parallel universes until they find the one in which 0.5 TU's (time units)");
    println!("have passed, and will report the positions of all the aircraft at that time, plus the positions of");
    println!("the aircraft in the current universe (in which 0 TU's have passed).");
    println!("The machine will report this information to you in this format:");
    println!("<integer id of plane>: (coordinates after 0 TU's) -> (coordinates after 0.5 TU's)");
    println!("This information will be relayed to you for each enemy aircraft.");
    println!("After the information for all the aircraft was displayed, the machine will wait four your commands.");
    println!("For each shell available to you, you will need to provide the follwing information:");
    println!("<yaw> <distance> <delay>");
    println!("Yaw reffers to cannon's rotation around the OZ axis, or the trigonometric angle your cannon makes with the positive side");
    println!("of the OX axis.");
    println!("Because this cannon works with timed fuses, you also need to provide the distance from you at which the shell should explode.");
    println!(
        "The shells have a *decent* blast radius, so you do not need to be pinpoint accurate."
    );
    println!(
        "You also need to provide the time the machine should wait before executing your command"
    );
    println!(
        "This delay is always calculated from the same point in time (after 0 TU's have passed)."
    );
    println!("The cannon will automagically reorder your commands and execute them in an order that makes sense...");
    println!("So all of your delays should be relative to timestamp 0 (when 0 TU's have passed)");
    println!("Inputting commands does not require any time.");
    println!("There are also a few limitations");
    println!("The shells have a maximum distance at which they can communicate with the cannon, and after that point they");
    println!("will no longer explode or cause any damage.");
    println!("The machines radar has also detected that the planes are also carying bombs.");
    println!("You will need to shoot them down before they reach Lapland.");
    println!(
        "The maximum distance a shell will travel is {} DU's (distance units)",
        cannon_reach_radius
    );
    println!(
        "Lapland is defined by a circle of radius {}, centered at your position (0,0)",
        pole_radius
    );
    println!("<Press enter when you are ready>");

    std::io::stdout().flush().expect("Failed to flush :(");
    std::io::stdin()
        .read_line(&mut _scratch_string)
        .expect("Failed to read :(");

    // Construct the levels
    let levels = (0..100)
        .map(|i| {
            let n_planes = if i == 0 {
                1
            } else {
                rand::random::<usize>() % 100 + 100 + 2 * i
            };

            LevelValues {
                ammo: n_planes,
                attacking_planes: n_planes,
            }
        })
        .collect::<Vec<LevelValues>>();

    // Construct the game
    let mut game = Game::new(
        Vector2::default(),
        pole_radius,
        spawn_circle_radius,
        levels,
        cannon_reach_radius,
    );

    // Start the game
    game.play_levels();
}
