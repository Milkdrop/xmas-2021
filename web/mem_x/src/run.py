import discord
import hashlib
import os
import time

client = discord.Client()

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')
    await client.change_presence (activity = discord.Game (name = "Use !help in my DMs!"))

@client.event
async def on_message(message):
    if not message.guild:
        message_tokens = message.content.split(" ")
        if message_tokens[0].startswith("!n"):
            if len(message_tokens) > 1:
                path = str(hashlib.md5(bytes(str(int(time.time() * 1000)), encoding="utf-8")).hexdigest())
                note_content = " ".join(m.replace("`", "") for m in message_tokens[1:])
                with open(path + ".txt", "w") as f:
                    f.write(note_content)
                    
                await message.channel.send(
                    "Your note has been saved! Here's its hash: `{}`\nUse that (`!remember {}`) to remember your note!".format(path, path),
                    reference=message, mention_author=True
                )
            else:
                await message.channel.send(
                    "You need to provide note content! `!note [note text]`",
                    reference=message, mention_author=True
                )
            
        elif message_tokens[0].startswith("!r"):
            if len(message_tokens) > 1:
                path = message_tokens[1] + ".txt"
                if os.path.exists(path):
                    with open(path, "r") as f:
                        note_content = f.read()
                        
                    await message.channel.send(
                        "Here's your note!\n```{}```".format(note_content),
                        reference=message, mention_author=True
                    )
                else:
                    await message.channel.send(
                        "We couldn't find a note called `{}`! :(".format(path.replace("`", "")),
                        reference=message, mention_author=True
                    )
            else:
                await message.channel.send(
                    "You need to provide a note hash! `!remember [notehash]`",
                    reference=message, mention_author=True
                )
                
        elif message_tokens[0].startswith("!h"):
            await message.channel.send(
                "```diff\n\n"
                "+ !note\n"
                "   = !note [note text]     | make a new note! you will never forget it.\n\n"
                "+ !remember\n"
                "   = !remember [notehash]  | remember a note! you'll need the hash you got when you made it."
                "```",
                reference=message, mention_author=True
            )
    else:
        if message.content == '!flag':
            await message.channel.send("Why won't you forget that?")

with open("token", "r") as f:
    token = f.read()

client.run(token)