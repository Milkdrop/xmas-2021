service php7.4-fpm start

mysqld_safe &

while ! mysql -e "show databases;"; do # Wait for MariaDB
	sleep 0.25
done

mysql < /setup1.sql
mysql < /setup2.sql
mysql < /setup3.sql

python3 /tools/qrgen.py > qr.sql
python3 /tools/manager_gen.py > manager.sql
mysql < qr.sql
mysql < manager.sql

nginx

echo "Docker online!"
while true; do
    sleep 5;
    echo "Refreshing db..."
    python3 /tools/qrgen.py > qr.sql
    python3 /tools/manager_gen.py > manager.sql
    mysql < qr.sql
    mysql < manager.sql
done