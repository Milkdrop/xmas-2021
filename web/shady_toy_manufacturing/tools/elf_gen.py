import random, string
import randomname
import unidecode

sekrit = "VEhJUyBJUyBBIEhJRERFTiBTVEFHSU5HIE1FU1NBR0UgW0NPTV9FTFZFU0BERVBUXzB4MkZdCgpJbiBjYXNlIHlvdSBoYXZlbid0IHNlZW4gdGhlIHBvc3QtaXQgbm90ZSBvbiB5b3VyIGRlc2ssIHRoZSBmdW5jdGlvbiBHRVRfUVVJQ0tfUkVTUE9OU0VfTUFQUElORyB0YWtlcyBwYXJhbWV0ZXJzICJMT0FETUFQMTIxMTExMzEyOGNoIiwgWU9VUl9ERVBBUlRNRU5UX0lELCBYLCBZLCBZT1VSX0RFUEFSVE1FTlRfUEFTU1dPUkQuIFlvdSBjYW4gZmV0Y2ggdGhlIHBhc3N3b3JkIGZyb20gdGhlIHRhYmxlIHdpdGggaW50ZXJuYWwgcGFzc3dvcmRzLiBXZSd2ZSBvbmx5IHRlc3RlZCB0aGlzIGZ1bmN0aW9uIG9uIG91ciBsb2NhbCBzZXJ2ZXJzLCBzbyBpZiB5b3UncmUgbm90IGZyb20gdGhpcyBkZXBhcnRtZW50LCBwbGVhc2UgZG9uJ3QgdHJ5IHRvIHJ1biB0aGF0IGZ1bmN0aW9uLgoKTWFuYWdlbWVudA=="
table_count = 1853

indexes = random.sample(range(table_count), k=len(sekrit))
indexes.sort()
sekrit_index = 0

table_indexes = list(range(table_count))
random.shuffle(table_indexes)

for i in table_indexes:
    district_id = ''.join(random.choices(string.ascii_lowercase + string.ascii_uppercase + string.digits, k=random.randint(5, 10)))
    district_id2 = ''.join(random.choices(string.ascii_lowercase + string.ascii_uppercase + string.digits, k=random.randint(2, 6)))
    table_name = f"toy_manufacturing_district_ID{district_id}_ORDER_ID{i}_SPEC{district_id2}"
    
    val = random.randint(32, 126)
    if i in indexes:
        val = ord(sekrit[indexes.index(i)])
    
    print(f"CREATE TABLE {table_name}(", end = "")
    types = []
    columns_chosen = []
    for k in range(val):
        col_name = unidecode.unidecode(randomname.get_name()).replace("-", "_")

        while col_name in columns_chosen:
            col_name = unidecode.unidecode(randomname.get_name()).replace("-", "_")

        columns_chosen.append(col_name)

        col_type = random.choice(["INT", "TEXT", "FLOAT", "DOUBLE"])
        types.append(col_type)
        print(f"{col_name} {col_type}", end = "")
        if k != val - 1:
            print(", ", end = "")
    
    print(") ENGINE=Aria;")

    print(f"GRANT SELECT ON santas_db.{table_name} TO 'Elf'@'localhost';")
    
    if i in indexes:
        print(f"INSERT INTO {table_name} VALUES (", end = "")

        for k in range(val):
            if (types[k] == "TEXT"):
                print(f"'LOL'", end = "")
            else:
                print("1", end = "")

            if k != val - 1:
                print(", ", end = "")
        
        print(");")
        