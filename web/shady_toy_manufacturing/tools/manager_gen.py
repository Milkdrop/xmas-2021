
import random, string
import pyminizip
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 

offset = 0

if random.random() >= 0.5:
    offset = random.randint(1, 24)
else:
    offset = random.randint(-24, -1)

xor_key = ''.join(random.choices(string.ascii_lowercase + string.ascii_uppercase, k=10))

caesar_xor_key = ""
for c in xor_key:
    distance = (ord('z') - ord('a') + 1)

    if c >= 'a' and c <= 'z':
        new_c = ord(c) - offset
        while (new_c > ord('z')): new_c -= distance
        while (new_c < ord('a')): new_c += distance
        caesar_xor_key += chr(new_c)

    if c >= 'A' and c <= 'Z':
        new_c = ord(c) - offset
        while (new_c > ord('Z')): new_c -= distance
        while (new_c < ord('A')): new_c += distance
        caesar_xor_key += chr(new_c)

art = f"""
                                         .
                                          `.

                                     ...
                                        `.
                                  ..
                                    `.
                            `.        `.
                         ___`.\\.//
                            `---.---
                           /     \\.--
                          /       \\-
                         |   /\\    \\
                         |\\==/\\==/  |
                         | `@'`@'  .--.
                  .--------.           )
                .'             .   `._/
               /               |     \\
              .               /       |
              |              /        |
              |            .'         |   .--.
             .'`.        .'_          |  /    \\
           .'    `.__.--'.--`.       / .'      |
         .'            .|    \\\\     |_/        |
       .'            .' |     \\\\               |
     .-`.           /   |      .      __       |
   .'    `.     \\   |   `           .'  )      \\
  /        \\   / \\  |            .-'   /       |
 (  /       \\ /   \\ |                 |        |
  \\/         (     \\/                 |        |
  (  /        )    /                 /   _.----|
   \\/   //   /   .'                  |.-'       `
   (   /(   /   /                    /      `.   |
    `.(  `-')  .---.                |    `.   `._/
       `._.'  /     `.   .---.      |  .   `._.'
              |       \\ /     `.     \\  `.___.'
              |        Y        `.    `.___.'
              |      . |          \\         \\
              |       `|           \\         |
              |        |       .    \\        |
              |        |        \\    \\       |
            .--.       |         \\           |
           /    `.  .----.        \\          /
          /       \\/      \\        \\        /
          |       |        \\       |       /
           \\      |    @    \\   `-. \\     /
            \\      \\         \\     \\|.__.'
             \\      \\         \\     |
              \\      \\         \\    |
               \\      \\         \\   |
                \\    .'`.        \\  |
                 `.-'    `.    _.'\\ |
                   |       `.-'    ||
              .     \\     . `.     ||      .'
               `.    `-.-'    `.__.'     .'
                 `.                    .'
             .                       .'
              `.
                                           .-'
                                        .-'
                        - {caesar_xor_key} -

      \\                 \\
       \\         ..      \\
        \\       /  `-.--.___ __.-.___
`-.      \\     /  #   `-._.-'    \\   `--.__
   `-.        /  ####    /   ###  \\        `.
________     /  #### ############  |       _|           .'
            |\\ #### ##############  \\__.--' |    /    .'
            | ####################  |       |   /   .'
            | #### ###############  |       |  /
            | #### ###############  |      /|      ----
          . | #### ###############  |    .'<    ____
        .'  | ####################  | _.'-'\\|
      .'    |   ##################  |       |
             `.   ################  |       |
               `.    ############   |       | ----
              ___`.     #####     _..____.-'     .
             |`-._ `-._       _.-'    \\\\\\         `.
          .'`-._  `-._ `-._.-'`--.___.-' \\          `.
        .' .. . `-._  `-._        ___.---'|   \\   \\
      .' .. . .. .  `-._  `-.__.-'        |    \\   \\
     |`-. . ..  . .. .  `-._|             |     \\   \\
     |   `-._ . ..  . ..   .'            _|
      `-._   `-._ . ..   .' |      __.--'
          `-._   `-._  .' .'|__.--'
              `-._   `' .'
                  `-._.'


                    - I hate this database -
          - The xor key above the PC is in caesar cipher, you will need to keep the CaSiNg -
          - To decode, """ + ("ADD" if (offset >= 0) else "SUB") + " " + str(abs(offset)) + " -"

# ''.join(random.choices(string.ascii_lowercase + string.digits, k=random.randint(10, 16)))

print("USE santas_db;")
maxline = 0
art_lines = art.split("\n")

for line in art_lines:
    if len(line) > maxline:
        maxline = len(line)

print("DROP TABLE IF EXISTS painting;")
print("CREATE TABLE painting(" + ",".join([f"x{i} CHAR" for i in range(maxline)]) + ");")
print("GRANT SELECT ON santas_db.painting TO 'Xmas_Manager_1412c124cx12'@'localhost';")
print("INSERT INTO painting VALUES ", end="")

for y in range(len(art_lines)):
    print("(", end="")

    for x in range(maxline):
        to_insert = ' '
        if (x < len(art_lines[y])):
            to_insert = art_lines[y][x]
        
            if to_insert == '\\':
                to_insert = '\\\\'

            if to_insert == "'":
                to_insert = '\\\''

        print(f"'{to_insert}'", end="")

        if x != maxline - 1:
            print(",", end="")
    
    print(")",end="")
    if y != len(art_lines) - 1:
        print(",")

print(";")
zippass_len = 60

usernames = []
passwords = []
zip_password = ''.join(random.choices(string.ascii_lowercase + string.digits + string.ascii_uppercase, k=zippass_len))

print("DROP TABLE IF EXISTS management_passwords;")
print("CREATE TABLE management_passwords(id INT PRIMARY KEY, user TEXT, pass TEXT);")
print("GRANT SELECT ON santas_db.management_passwords TO 'Xmas_Manager_1412c124cx12'@'localhost';")
#-- As manager, huge ascii art with a mythical creature or something, ADD / SUB OFFSET for a caesar cipher password
#--		which is the XOR key for a table with passwords on every row, and each password would be given to a function which returns pairs "c,2" character seq. number,
#--		which form another password which is for a zip file stored using binary columns (column2, column3, column5 are these bits set) 

print("INSERT INTO management_passwords VALUES ", end="")

def get_xored_pass(pwd):
    out = ''

    i = 0
    for c in pwd:
        out += chr(ord(c) ^ ord(xor_key[i % len(xor_key)]))
        i += 1

    return out

for i in range(zippass_len):
    uname = ''.join(random.choices(string.ascii_lowercase, k=random.randint(10, 20)))
    passwd = ''.join(random.choices(string.ascii_lowercase + string.digits + string.ascii_uppercase, k=random.randint(30, 60)))
    
    usernames.append(uname)
    passwords.append(get_xored_pass(passwd))

    print(f"({i}, '{uname}', '{passwd}')", end="")

    if i != zippass_len - 1:
        print(",", end="")

zip_pass_indexes = list(range(zippass_len))
random.shuffle(zip_pass_indexes)

print(";")

print("DELIMITER //")
for i in range(zippass_len):
    func_name = f"get_final_chunk_for_id_{i}"
    print(f"DROP FUNCTION IF EXISTS {func_name} //")
    print(f"CREATE FUNCTION {func_name} (pass TEXT) RETURNS TEXT BEGIN IF HEX(pass) = '{''.join([hex(ord(c))[2:].zfill(2) for c in passwords[i]]).upper()}' THEN RETURN \"{zip_pass_indexes[i]},'{zip_password[zip_pass_indexes[i]]}'\"; ELSE RETURN 'Wrong password.'; END IF; END //")
    print(f"GRANT EXECUTE ON FUNCTION santas_db.{func_name} TO 'Xmas_Manager_1412c124cx12'@'localhost' //")

print("DELIMITER ;")

max_bitfield_len = 80
for i in range(max_bitfield_len):
    table_name = f"get_final_bitfield_byte_{i}"
    print(f"DROP TABLE IF EXISTS {table_name};")

bitfield_pass = ''.join(random.choices(string.ascii_lowercase, k=random.randint(60, max_bitfield_len)))
bitfield_indexes = list(range(len(bitfield_pass)))
random.shuffle(bitfield_indexes)

for i in bitfield_indexes:
    table_name = f"get_final_bitfield_byte_{i}"

    tables = []
    value = ord(bitfield_pass[i])

    index = 0
    while value > 0:
        if (value & 1) == 1:
            tables.append(f"bit_{index}_set INT")
        index += 1
        value >>= 1

    if len(tables) == 0:
        tables.append("no_bits_set INT")

    print(f"CREATE TABLE {table_name}(" + ",".join(tables) + ");")
    print(f"GRANT SELECT ON santas_db.{table_name} TO 'Xmas_Manager_1412c124cx12'@'localhost';")
    

actual_zip_pass = 'GetTOtheFLAGalreadyPLEASE' + ''.join(random.choices(string.ascii_lowercase + string.digits + string.ascii_uppercase, k=16))
func_name = f"get_zip_password"
print("DELIMITER //")
print(f"DROP FUNCTION IF EXISTS {func_name} //")
print(f"CREATE FUNCTION {func_name} (final_pass TEXT, final_bitfield TEXT) RETURNS TEXT BEGIN IF final_pass != '{zip_password}' THEN RETURN 'Wrong final password.'; ELSEIF final_bitfield != '{bitfield_pass}' THEN RETURN 'Wrong bitfield password.'; ELSE RETURN '{actual_zip_pass}'; END IF; END //")
print(f"GRANT EXECUTE ON FUNCTION santas_db.{func_name} TO 'Xmas_Manager_1412c124cx12'@'localhost' //")
print("DELIMITER ;")

pyminizip.compress("flag.txt", None, "flag.zip", actual_zip_pass, 0)

zip_data = open("flag.zip", "rb").read()

zip_indexes = list(range(len(zip_data)))
random.shuffle(zip_indexes)

# This should clear leftover zip bytes up until 500
for i in range(500):
    table_name = f"get_zip_byte_{i}"
    print(f"DROP TABLE IF EXISTS {table_name};")
    
for i in zip_indexes:
    table_name = f"get_zip_byte_{i}"

    tables = []
    value = zip_data[i]

    index = 0
    while value > 0:
        if (value & 1) == 1:
            tables.append(f"bit_{index}_set INT")
        index += 1
        value >>= 1

    if len(tables) == 0:
        tables.append("no_bits_set INT")

    print(f"CREATE TABLE {table_name}(" + ",".join(tables) + ");")
    print(f"GRANT SELECT ON santas_db.{table_name} TO 'Xmas_Manager_1412c124cx12'@'localhost';")