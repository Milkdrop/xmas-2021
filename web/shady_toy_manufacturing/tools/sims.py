from bs4 import BeautifulSoup
import requests

page = requests.get("https://sims.fandom.com/wiki/Category:Objects_in_The_Sims_2").text
soup = BeautifulSoup(page, 'html.parser')
links = soup.find_all("a", attrs={"class":"category-page__member-link"})

print(f"""INSERT INTO toys_for_sale(name, price, visible, STAGING_COLUMN, description) VALUES """)

for link in links:
    page = requests.get("https://sims.fandom.com/" + link["href"]).text
    soup = BeautifulSoup(page, "html.parser")
    try:
        name = soup.find("h1", attrs={"id": "firstHeading"}).get_text().replace('"', '\\"').strip()
        description = soup.find("td", attrs={"class": "metainfobox-caption"}).get_text().strip()

        print(f"""("{name}", 200, 1, "", "{description}"),""")

    except Exception as e:
        pass