from PIL import Image
import random, string

im = Image.open('qr.png')
pix = im.load()

coordlist = []

for y in range(im.size[1]):
    for x in range(im.size[0]):
        coordlist.append((y, x))

random.shuffle(coordlist)

mapping = {}
i = 0
for y in range(im.size[1]):
    for x in range(im.size[0]):
        mapping[(y, x)] = coordlist[i]

        i += 1

print("USE santas_db;")
print(f"DROP TABLE IF EXISTS toy_quick_response;")
print("CREATE TABLE toy_quick_response (" + ",".join([f"x{i} INT" for i in range(im.size[0])]) + ");")
print("INSERT INTO toy_quick_response VALUES ", end="")

for y in range(im.size[1]):
    print("(", end="")
    for x in range(im.size[0]):
        coords = mapping[(y, x)]
        print("1" if pix[coords[1], coords[0]][0] == 0 else "0", end="")
        
        if x != im.size[0] - 1:
            print(",", end="")
    
    print(")",end="")
    if y != im.size[1] - 1:
        print(",")

print(";")

print(f"DROP TABLE IF EXISTS internal_passwords;")
print("CREATE TABLE internal_passwords(department_id INT, pass TEXT);")
print("INSERT INTO internal_passwords VALUES ", end='')

for i in range(0xFF):
    password = ''.join(random.choices(string.ascii_lowercase + string.digits, k=random.randint(10, 16)))
    print(f"({i}, '{password}')", end='')

    if (i == 0x2F):
        password_2F = password

    if (i != 0xFF - 1):
        print(",")

print(";")

print(f"DROP FUNCTION IF EXISTS GET_QUICK_RESPONSE_MAPPING;")
print(f"""DELIMITER //
CREATE FUNCTION GET_QUICK_RESPONSE_MAPPING (operation TEXT, department INT, unk0 INT, unk1 INT, unk2 TEXT) RETURNS TEXT
BEGIN
    DECLARE ret TEXT;
    IF operation != "LOADMAP1211113128ch" THEN
        RETURN "Invalid operation.";
    ELSEIF department != 0x2F THEN
        RETURN "Invalid department, or you do not have access to that department's data.";
    ELSEIF unk2 != "{password_2F}" THEN
        RETURN "WRONG PASSWORD. THIS INCIDENT WILL BE REPORTED.";
    END IF;
""")

for y in range(im.size[1]):
    for x in range(im.size[0]):
        coords = mapping[(y, x)]
        print(f"    IF unk0 = {x} AND unk1 = {y} THEN SET ret = '{coords[1]},{coords[0]}'; END IF;")

print("""    RETURN ret;
END //

DELIMITER ;""")

print("GRANT SELECT ON santas_db.toy_quick_response TO 'Elf'@'localhost';")
print("GRANT SELECT ON santas_db.internal_passwords TO 'Elf'@'localhost';")
print("GRANT EXECUTE ON FUNCTION santas_db.GET_QUICK_RESPONSE_MAPPING TO 'Elf'@'localhost';")
