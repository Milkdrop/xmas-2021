import randomname
import random, string

print("CREATE TABLE toys_for_sale2(", end = "")
print("id INT PRIMARY KEY", end = "")
print(", name TEXT", end = "")
print(", name_temp TEXT", end = "")
print(", name_staging TEXT", end = "")
print(", name2 TEXT", end = "")
print(", name_proposed TEXT", end = "")
print(", name_alternative TEXT", end = "")

for i in range(16):
    print(f", description_range_{i} TEXT", end = "")

print(", price_min INT", end = "")
print(", price_proposed INT", end = "")
print(", price_market INT", end = "")

for i in range(1, 6):
    print(f", staging_info_part_{i}_chunk TEXT", end = "")

print(");")

print("INSERT INTO toys_for_sale2 VALUES")

sekrit = "VEhJUyBJUyBBIEhJRERFTiBTVEFHSU5HIE1FU1NBR0UgW0NPTV9JTlRFUk5TX0xPV10NCg0KSU5URVJOUywgVGVtcG9yYXJpbHkgdXNlIHRoZSBFbGYgYWNjb3VudCB0byBhY2Nlc3MgdGhlIGRlZXAgZGF0YWJhc2Ugd2Ugd2lsbCB3b3JrIHdpdGggZm9yIHRoaXMgbW9udGguDQoNCk1hbmFnZW1lbnQ="
index = 0

indexes = random.sample(range(1000), k=10)

step_size = len(sekrit) // (len(indexes) * 5) + 1
index = 0

strings = []
for sql_i in range(1000):
    chunk = "\t"
    name = randomname.get_name().replace("-", " ")
    name2 = randomname.get_name()
    
    chunk += f'({sql_i}, "{name}", "{name}{random.randint(100, 400012)}", "{name2}", "{name2.replace("-", " ")}", "{name}_Final", "{name2}_Alt"'
    
    for j in range(16):
        desc = ''.join(random.choices(string.ascii_lowercase + string.ascii_uppercase + string.digits, k=random.randint(5, 30)))
        chunk += f', "{desc}"'
    
    price = random.randint(100, 2000)
    chunk += f', {price}'
    chunk += f', {price + random.randint(-25, 1000)}'
    chunk += f', {price + random.randint(-99, 10)}'

    for k in range(1, 6):
        if (sql_i in indexes):
            chunk += f', "{sekrit[index:(index + step_size)]}"'
            index += step_size
        else:
            chunk += f', ""'

    chunk += ')'
    strings.append(chunk)

print(",\n".join(strings))