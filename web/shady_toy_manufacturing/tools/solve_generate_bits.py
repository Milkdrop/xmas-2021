for i in range(256):
    binary = ""

    val = i
    bin_i = 0
    while val > 0:
        if (val & 1) == 1:
            binary += str(bin_i)
        
        bin_i += 1
        val >>= 1

    print("WHEN '" + binary + "' THEN " + str(i), end=' ')