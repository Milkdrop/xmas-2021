<?php
session_start();
if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = 'Guest';
    $_SESSION['pass'] = 'Guest';
}

if ($_SESSION['user'] == 'Xmas_Manager_1412c124cx12') {
    if (!isset($_POST['work'])) {
        echo "Hello! If you are an X-MAS Manager, you will have to supply a proof of work :^(\n\n";
        echo "I'm just an underpaid sysadmin, so you can't blame me, I don't make the rules.\n\n";
        echo "You will need to POST with a work= parameter equal to a string, such that when md5 hashed with the salt '" . $_SESSION['salt'] . "', it will give a hash which starts with the 6 characters: '" . $_SESSION['hash_start'] . '\'. The salt and the start change on every successful work submission';

        die();
    } else {
        if (is_string($_POST['work']) && isset($_SESSION['salt']) && strcmp(substr(md5($_SESSION['salt'] . $_POST['work']), 0, 6), $_SESSION['hash_start']) === 0) {
            $_SESSION['salt'] = bin2hex(random_bytes(32));
            $string = bin2hex(random_bytes(16));
            $_SESSION['hash_start'] = substr(md5($_SESSION['salt'] . $string), 0, 6);

            echo 'Work is fine, you may proceed :) Your new work is now: salt=' . $_SESSION['salt'] . ' hash_start=' . $_SESSION['hash_start'];
        } else {
            echo "I hashed " . $_SESSION['salt'] . $_POST['work'] . " and it gave " . md5($_SESSION['salt'] . $_POST['work']) . '. It doesn\'t start with ' . $_SESSION['hash_start'];
            die();
        }
    }
}

$conn = new mysqli("localhost", $_SESSION['user'], $_SESSION['pass'], "santas_db");

if (isset($_POST['goto'])) {
    $stmt = $conn->prepare("SET STATEMENT max_statement_time=10 FOR SELECT location FROM redirect_locations WHERE location='" . $_POST['goto'] . "'");
    $stmt->execute();
    $result = $stmt->get_result();
    $row = $result->fetch_assoc();

    if (isset($row)) {
        header("Location: /" . $row['location'] . ".html", true, 301);
        exit();
    } else {
        echo "Couldn't find that URL in our database!";
    }
}
?>