<?php
$conn = new mysqli("localhost", 'Guest', 'Guest', "santas_db");

if (!isset($_POST['name'])) {
    echo 'Where is your name?';
} else if (!isset($_POST['entry'])) {
    echo 'Where is your message?';
} else if (
        strpos(strtolower($_POST['entry']), "x-mas{") !== false
        || strpos(strtolower($_POST['entry']), "xmas{") !== false
        || strpos(strtolower($_POST['name']), "x-mas{") !== false
        || strpos(strtolower($_POST['name']), "xmas{") !== false
    ) {
    echo 'Don\'t even think about it.';
} else {
    $stmt = $conn->prepare("INSERT INTO guestbook_ips(ip) VALUES (?)");
    $ip = ip2long($_SERVER['REMOTE_ADDR']);
    $name = $_POST['name'];
    $entry = $_POST['entry'];

    $stmt->bind_param("i", $ip);
    $stmt->execute();

    $stmt = $conn->prepare("INSERT INTO guestbook(name,entry) VALUES (?, ?)");
    $stmt->bind_param("ss", $name, $entry);

    if ($stmt->execute() === TRUE) {
        echo "Successfully written in our guestbook: " . $entry;
    } else {
        echo "Error!";
    }
}

?>