<?php

if (!isset($_POST['sendername']) || !isset($_POST['senderemail']) || !isset($_POST['newname']) || !isset($_POST['newemail'])) {
    echo "Not enough arguments.";
} else {
    if (strpos($_POST['newname'], "'") !== false) {
        echo 'What kind of friend name is that?';
    } else {
        echo "Referred " . $_POST['newname'] . "!";
    }
}

?>