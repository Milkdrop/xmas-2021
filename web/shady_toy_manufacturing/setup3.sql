USE santas_db;

GRANT ALL PRIVILEGES ON santas_db.* TO 'Xmas_Manager_1412c124cx12'@'localhost' IDENTIFIED BY 'Pasduc18u9c813y3ci13cyc31';
REVOKE ALL PRIVILEGES ON santas_db.* FROM 'Xmas_Manager_1412c124cx12'@'localhost';
GRANT SELECT ON santas_db.toys_for_sale TO 'Xmas_Manager_1412c124cx12'@'localhost';
GRANT SELECT ON santas_db.redirect_locations TO 'Xmas_Manager_1412c124cx12'@'localhost';

CREATE TABLE sql_torture(help TEXT);
INSERT INTO sql_torture VALUES ("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
GRANT SELECT ON santas_db.sql_torture TO 'Xmas_Manager_1412c124cx12'@'localhost';

CREATE TABLE sql_torture_but_with_kind_explanations(help TEXT);
INSERT INTO sql_torture_but_with_kind_explanations VALUES ("Are you still alive? good. Because now the database gets insane. I had to quit my job over this, so good luck to the poor manager who supersedes me, haha. I'll still try to explain what we got here, just from the kindness of my heart. No need to thank me :) Basically, the painting contains a XOR Key, which you use to XOR every one of the management passwords. Then, every single xored management password is a parameter to its corresponding final_chunk function. These final chunks will tell you how to form a final password. You will then need to look into the final bitfield byte tables and build another final string, where every table is a character from that string (you can figure out the characters if you look at the column names :) ). Pass the final password and the final bitfield thing to the zip password getter. That's the password for the zip archive encoded in the same way as the final string. That's the zip where we store the juicy secrets, usually we have scripts for us that do all the busy work for us, but since you are a new manager I think IT hasn't had the time to send them to you. You'll need to write that yourself. Also, the entire database refreshes all the passwords once every few seconds, so you'd need to write everything in one query. Good luck! You need to be one MEAN SQL wizard to figure this shit out.");
GRANT SELECT ON santas_db.sql_torture_but_with_kind_explanations TO 'Xmas_Manager_1412c124cx12'@'localhost';