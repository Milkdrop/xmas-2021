<!DOCTYPE html>
<html>
	<head>
		<title>Toy Supply Tracking Database</title>
		<link rel="stylesheet" href="index.css">
	</head>

	<body>
		<div class=center_title>
			<h1>Toy Supply Tracking Database</h1>

			<script>function get_search() { document.location = '/?rgx=' + document.getElementById('input_form').value.replace(', ', '|').replace(',', '|') }</script>
			<form>
				<input id="input_form" placeholder="Doll, Building Blocks, ..."></input>
				<button type="submit" formaction="javascript:get_search();">Search</button>
			</form>
		</div>
		<br>
		<hr>

		<?php 
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

$information_names = [
	"Name&nbsp;&nbsp;&nbsp;&nbsp;",
	"Size&nbsp;&nbsp;&nbsp;&nbsp;",
	"Quota&nbsp;&nbsp;&nbsp;",
	"Produced",
	"Comments"
];

$flag = "X-MAS{4n_eV1L_Turn4b0ut__}";

$site_information = [
	["Building Blocks",             "Small",  "73,507", "14,982", ""],
	["Doll",                        "Medium", "3,159", "878", ""],
	["Sheet of Paper",              "Small",  "25,751", "5,852", ""],
	["Stockings",                   "Medium", "8,100", "1,122", ""],
	["Large Wooden Box",            "Large",  "139", "96", ""],
	["Gingerbread House",           "Large",  "524", "19", ""],
	["Chocolate Shapes",            "Small",  "71,868", "22,473", "Has anyone seen the batch of 1000 I made this morning? --Hannah<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| The supply page is not a forum. Ask somewhere else. --Dave"],
	["Teddy Bear",                  "Medium", "9,699", "195", ""],
	["Toy Racecar",                 "Medium", "5,612", "1,012", ""],
	["Lump of Coal",                "Small",  "145,376", "19,898", "Don't give these to the nice kids. --Dave"],
	["Snowman",                     "Large",  "1,065", "18", ""],
	["Pinecone",                    "Small",  "88,958", "16,855", ""],
	["Toy Soldier",                 "Small",  "52,906", "3,745", ""],
	["Stuffed Bunny",               "Medium", "2,879", "780", ""],
	["Laser Gun",                   "Large",  "378", "92", ""],
	["Watch",                       "Medium", "3,964", "719", ""],
	["Pencils",                     "Small",  "48,692", "20,600", ""],
	["Bells",                       "Small",  "174,170", "22,707", ""],
	["Bicycle",                     "Large",  "128", "50", ""],
	["Snowballs",                   "Small",  "211,860", "22,707", "Might melt. Check later. --Leah"],
	["Shovel",                      "Large",  "1,059", "29", ""],
	["Flower Seeds",                "Small",  "164,572", "20,366", ""],
	["Miscellaneous (CONTRACTING)", "N/A",    "127", "2", "Need to get these done. Call Santa ASAP. --Dave"],
	[$flag,   						"N/A",    "0", "0", "This absolutely needs to be hidden. --Dave"]
];

ini_set("pcre.backtrack_limit", 10000);

//$re_str = "/(SANTA|(a+)+b$)/";
if (isset($_GET['rgx'])) {
	$re_str_unformatted = $_GET['rgx'];
	$re_str = "/" . str_replace("/", "", $re_str_unformatted) . "/";

	// Test regex to make sure it is valid
	if (preg_match($re_str, null) === false) {
		echo "Entered regex has an error.";
	} else {
		// Break this with (test (.*)|Piss (.*)|abc (.*)|g (.*)|^HIDD[^E].*|((((((\w*)*)*)*)*)*)*\$$)
		// (url encoded)
		echo "<div class='items_container'>";
		for ($i=0; $i<count($site_information); $i++) {
			// Fake DoS here; check if the regex fails on the string.
			$old_error = error_reporting(0);
			$match = preg_match($re_str, $site_information[$i][0]);
			error_reporting($old_error);

			if ($match === false) {
				$error = preg_last_error();
				if ($error == PREG_BACKTRACK_LIMIT_ERROR || $error == PREG_RECURSION_LIMIT_ERROR) {
					sleep(1);
				}
			} else if ($match && $site_information[$i][0] != $flag) {
				echo "<div class='item'>";

				for ($j=0; $j<count($information_names); $j++) {
					echo "<span class='item_name'>" . $information_names[$j] . "</span> | <span class='item_text'>" . $site_information[$i][$j] . "</span><br>";
				}

				echo "</div>";
			}
		}
		echo "</div>";
	}
} else {
	echo "<div class='items_container'>";
	for ($i=0; $i<count($site_information); $i++) {
		if ($site_information[$i][0] != $flag) {
			echo "<div class='item'>";

			for ($j=0; $j<count($information_names); $j++) {
				echo "<span class='item_name'>" . $information_names[$j] . "</span> | <span class='item_text'>" . $site_information[$i][$j] . "</span><br>";
			}

			echo "</div>";
		}
	}
	echo "</div>";
}
		?>
	</body>
</html>
