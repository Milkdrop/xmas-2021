from PIL import Image

img = Image.new('RGBA', (320, 240), (255, 255, 255))
pixels = img.load()

f = open("pixels.txt").read().split("\n")

for line in f:
    if len(line) == 0:
        continue
    line = [int(i) for i in line.split(" ")]

    pixels[line[0], line[1]] = (line[2], line[3], line[4])

img = img.resize((640, 480), Image.NEAREST)
img.save("canvas.png")