<head>
    <title>P.A.I.N.T.</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="window-container">
        <div class="window">
            <div class="title">
                <img src="img/title_left.png"/>
                <img src="img/title_right.png"/>
            </div>

            <div class="canvas">
                <img style="margin:6px" src="canvas.png"/>
            </div>

            <div class="bottom">
                <img src="img/bottom_left.png"/>
                <img src="img/bottom_right.png"/>
            </div>
        </div>
    </div>
</body>