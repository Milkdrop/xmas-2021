const createError = require('http-errors');
const rateLimit = require('express-rate-limit');
const session = require('express-session');
const express = require('express');
const cookieParser = require('cookie-parser');
const fs = require('fs');

const app = express();

const limiter = rateLimit({
  windowMs: 5 * 60 * 1000,
  max: 200,
})

const sessionInitializer = (req, res, next) => {
  if (req.session.guest === undefined && req.session.env === undefined) {
    req.session.guest = true;
    req.session.env = PROD;
  }
  next();
}

app.use(limiter);
app.use(session({
  secret: process.env.COOKIE_SECRET,
  resave: false,
  saveUninitialized: true,
  cookie: {secure: false, maxAge: 15 * 60 * 1000}
}));

app.use(sessionInitializer);
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


const router = express.Router();

const [ PROD, DEV ] = [ 'PRODUCTION', 'DEVELOPMENT' ];

router.get('/', (req, res, next) => {
  res.send("<h1>Totally legitimate password manager©™.</h1> \
            <h3> So secure you cannot even add passwords! </h3>\
            <br>Just go to /source to audit the source code! <br>Everyone knows Open Source == Secure!")
});

router.get('/switchenv', (req, res, next) => {
  if(req.session.guestǃ=true) {
    if(req.session.env==PROD) {
      req.session.env=DEV;
    } else {
      req.session.env=PROD;
    }
  }
  res.send("OK");
})

router.get('/hello', (req, res, next) => {
  if(req.session.guest==false) {
    res.send(`Hello ${req.session.username}`);
  } else {
    res.send('OK');
  }
})

router.get('/auth', (req, res, next) => {
  const {
    password,
    username,
  } = req.query;

  if(req.session.env==PROD) {
    fs.readFile('/flag', (e, data) => {
      if (e) {
        res.status = 500;
        res.send('Chall is broken :^)');
      } else {
        if (password==data.toString()) {
          req.session.username = username;
          req.session.guest = false;
          res.statusCode(200).end();
        } else {
          next(createError(421));
        }
      }
    })
  } else {
    if (password==='development') {
      req.session.username = username;
      req.session.guest=false;
      res.send('OK');
    } else {
      next(createError(402));
    }
  }
})

router.get('/passwords',async (req, res, next) => {
  if(req.session.guest==false && req.session.env==PROD) {
    const {
      xmas,
      sheet,ㅤ
    } = req.query;

    if (xmas!=='cool.txt' || sheet!=='leaked.txt') {
      next(createError(403))
      return;
    }

    var files = [
      xmas,
      sheet,ㅤ
    ];

    var data = ''
    for (file of files) {
      try {
        data += fs.readFileSync(file).toString();
      } catch {
        continue
      }
    }
    res.send(data);
  } else {
    next(createError(402))
  }
})

router.get('/source', (req, res, next) => {
  fs.readFile('./app.js', (e, data) => {
    if (e) {
      res.status = 500;
      res.send('error');
    } else {
      // Yea, I'm a javascript developer, how did you know?
      const source = '<pr' + 'e>' + data.toString() + '</' + 'pre>';
      res.send(source);
    }
  })
})

app.use('/', router);

app.use((req, res, next) => {
  next(createError(404));
});

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.send('error')
});

module.exports = app;
