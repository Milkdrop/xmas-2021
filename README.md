# X-MAS CTF Challenge repository

## Repo Main Structure:
Category > Challenge Name

## How/Where to upload a challenge:

* Add your challenge to the sheet (title, category, author, description, etc.)
* Upload your public files to GDrive, create a public link and add it to the sheet (files column): `<filename>: <link>`.
* Place your challenge setup in `<category>/<challenge name>`. You should have a `Dockerfile` or a `build_docker.sh` script in challenge's root folder.
