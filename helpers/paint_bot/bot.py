import disnake, asyncio
import requests, os
from datetime import datetime

def get_pwd():
    return os.path.dirname(os.path.realpath(__file__))

client = disnake.Client()
url = "http://challs.xmas.htsp.ro:3002"

pixels_file = get_pwd() + "/pixels.txt"
canvas_file = get_pwd() + "/canvas.png"

@client.event
async def on_ready():
    print(client.user.name + " powered up!")

async def periodic_upload_paint_image():
    while True:
        if (not client.is_ready()):
            await asyncio.sleep(1)
            continue

        try:
            with open(pixels_file, "w") as f:
                f.write(requests.get(url + "/pixels.txt").text)

            with open(canvas_file, "wb") as f:
                f.write(requests.get(url + "/canvas.png").content)

            p = open(pixels_file)
            c = open(canvas_file, "rb")

            ch = await client.fetch_channel(918569589615525959)
            await ch.send(datetime.now().strftime("%d/%m/%Y, %H:%M:%S"), files = [disnake.File(p), disnake.File(c)])

            p.close()
            f.close()
        
        except Exception as e:
            print(e)

        await asyncio.sleep(60 * 60)

client.loop.create_task(periodic_upload_paint_image())
client.run(open(get_pwd() + "/token").read().strip())