#!/bin/bash

set -euo pipefail

build_challenge() {
    name="$1"
    solcv="${2:-}"

    tag="$name:latest"

    if [ ! -z "$solcv" ]; then
        pushd "$name/public"
        ROOT_DIR="$(cd .. && pwd)"
        SOLC_VERSION="$solcv" solc "private=$ROOT_DIR/private/" "public=$ROOT_DIR/public/contracts" --combined-json bin contracts/Setup.sol > deploy/compiled.bin
        sed -i.bak "s^${ROOT_DIR}^^g" deploy/compiled.bin && rm deploy/compiled.bin.bak
        popd
    fi

    docker build -t "$tag" "$name/public"

    if [ ! -z "${DEPLOY:-}" ]; then
        docker push "$tag"
    fi
}

(cd challenge_base && docker build -t ctf/challenge_base:latest .)
(cd eth_challenge_base && docker build -t ctf/eth_challenge_base:latest .)

declare -a chals=(
    "hello 0.8.0"
    "cookie 0.6.0"
    "iterator 0.6.0"
    "caramelpool 0.8.0"
)

for chal in "${chals[@]}"; do
    build_challenge $chal
done
