import random

pow2 = set([1 << i for i in range(24)])


def check_condition(mat):
    for i in range(len(mat)):
        for j in range(len(mat[0])):
            if j > 0:
                if not mat[i][j] ^ mat[i][j - 1] in pow2:
                    return False
            if i > 0:
                if not mat[i][j] ^ mat[i - 1][j] in pow2:
                    return False

    return True


def get_optimal(n, m):
    n -= 1
    m -= 1
    bit_n = len(bin(n)) - 2
    bit_m = len(bin(m)) - 2
    d = [[0 for j in range(bit_m + 1)] for i in range(bit_n + 1)]

    for i in range(1, bit_n + 1):
        d[i][0] = d[i - 1][0] + (((n >> (bit_n - i)) & 1) << (bit_m + bit_n - i))

    for i in range(1, bit_m + 1):
        d[0][i] = d[0][i - 1] + (((m >> (bit_m - i)) & 1) << (bit_m + bit_n - i))

    for i in range(1, bit_n + 1):
        for j in range(1, bit_m + 1):
            d[i][j] = min(d[i][j - 1] + (((m >> (bit_m - j)) & 1) << (bit_m + bit_n - i - j)), d[i - 1][j] + (((n >> (bit_n - i)) & 1) << (bit_m + bit_n - i - j)))

    return d[bit_n][bit_m]


def check_minimality(mat):
    return max([max(mat[i]) for i in range(len(mat))]) == get_optimal(len(mat), len(mat[0]))


def check_distinctness(mat):
    n = len(mat)
    m = len(mat[0])
    nums = set()

    for line in mat:
        for elem in line:
            nums.add(elem)


    return len(nums) == n * m

class TestGen:

    def __init__(self):
        self.rand_gen = random.SystemRandom()

    def get_test(self, n_max):
        n = self.rand_gen.randint(n_max // 2, n_max)
        m = self.rand_gen.randint(n_max // 2, n_max)

        p = self.rand_gen.randint(0, n - 1)
        q = self.rand_gen.randint(0, m - 1)

        return n, m, p, q

    @staticmethod
    def check(n, m, p, q, line, column):
        mat = [[column[i] ^ line[j] ^ line[q] for j in range(m)] for i in range(n)]

        if not check_condition(mat):
            return False

        if not check_distinctness(mat):
            return False

        if not check_minimality(mat):
            return False

        return True
