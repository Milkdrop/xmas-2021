from test_generator import TestGen
from constants import *
import threading
import time
import sys
import os


def die(timeout):
    time.sleep(timeout)
    print("Time is up! sorry...")
    os._exit(0)


def intro_prompt():
    print(f"You will have to solve {TESTS} tests in at most {MAX_TIME} seconds. Good luck!")
    sys.stdout.flush()


def step_prompt(step):
    print(f"Step: #{step} out of {TESTS}!")
    sys.stdout.flush()


def main():
    intro_prompt()

    thr = threading.Thread(target=die, args=(MAX_TIME,))
    thr.start()

    test_gen = TestGen()

    for i in range(TESTS):
        step_prompt(i + 1)

        curr_max_n = 10 + i * (N_MAX - 10) // (TESTS - 1)

        n, m, p, q = test_gen.get_test(curr_max_n)
        print(f"(N, M) = ({n}, {m})")
        print(f"(P, Q) = ({p}, {q})")
        sys.stdout.flush()

        user_response_line = None
        user_response_column = None
        try:
            user_response_line = input(f"Line P = ").replace(' ', '').replace('[', '').replace(']', '').split(',')
            user_response_line = [int(element) for element in user_response_line]
            user_response_column = input(f"Column Q = ").replace(' ', '').replace('[', '').replace(']', '').split(',')
            user_response_column = [int(element) for element in user_response_column]

            if len(user_response_line) != m or len(user_response_column) != n:
                raise Exception("Invalid length")
        except:
            print("Invalid input, exiting!")
            sys.stdout.flush()
            os._exit(0)

        if test_gen.check(n, m, p, q, user_response_line, user_response_column):
            print("Correct response!")
            sys.stdout.flush()
        else:
            print("Incorect response. Goodbye!")
            sys.stdout.flush()

            os._exit(0)

    print(f"Well done! Here's your flag: {FLAG}")
    sys.stdout.flush()
    os._exit(0)


if __name__ == "__main__":
    main()
