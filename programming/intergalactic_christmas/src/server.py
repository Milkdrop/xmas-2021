from test_generator import TestGen
from constants import *
from math import log
import threading
import time
import sys
import os


def die(timeout):
    time.sleep(timeout)
    print("Time is up! sorry...")
    os._exit(0)


def intro_prompt():
    print(f"You will have to solve {TESTS} tests in at most {MAX_TIME} seconds. Good luck!")
    sys.stdout.flush()


def step_prompt(step):
    print(f"Step: #{step} out of {TESTS}!")
    sys.stdout.flush()


def main():
    print("Loading drivers...")
    sys.stdout.flush()
    test_gen = TestGen()
    a = (N_MAX - 10) // (TESTS - 1) ** 2 + 10
    c = 10

    thr = threading.Thread(target=die, args=(MAX_TIME,))
    thr.start()
    
    intro_prompt()

    for i in range(TESTS):
        step_prompt(i + 1)

        curr_n = a * i ** 2 + c
        curr_primes_cnt = curr_n // 2
        curr_queries_cnt = int(log(1 + curr_n))

        curr_nums, curr_queries = test_gen.get_test(curr_n, curr_primes_cnt, curr_queries_cnt)
        print(f"N = {curr_n}")
        print(f"K = {curr_queries_cnt}")
        print(f"numbers = {curr_nums}")
        print(f"queries = {curr_queries}")
        sys.stdout.flush()

        user_response = None
        try:
            user_response = input(f"answers = ").replace(' ', '').replace('[', '').replace(']', '').split(',')
            user_response = [int(element) for element in user_response]
        except:
            print("Invalid input, exiting!")
            sys.stdout.flush()
            os._exit(0)

        if test_gen.check(curr_nums, curr_queries, user_response):
            print("All responses are correct!")
            sys.stdout.flush()
        else:
            print("Some responses are incorrect. Better luck next time!")
            sys.stdout.flush()

            os._exit(0)

    print(f"Well done! Here's your flag: {FLAG}")
    sys.stdout.flush()
    os._exit(0)


if __name__ == "__main__":
    main()
