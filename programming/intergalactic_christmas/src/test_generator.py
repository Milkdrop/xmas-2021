import random


def count_values(prime_nums, composite_nums, val):
    total = 0
    j = len(composite_nums) - 1
    for i in range(len(prime_nums)):
        while j >= 0 and prime_nums[i] + composite_nums[j] >= val:
            j -= 1

        if j < 0:
            break

        total += j + 1

    return total


def solve(prime_nums, composite_nums, query):
    start, end, last = prime_nums[0] + composite_nums[0], prime_nums[-1] + composite_nums[-1], -1

    while start <= end:
        mid = (start + end) // 2
        if count_values(prime_nums, composite_nums, mid) < query:
            start = mid + 1
            last = mid
        else:
            end = mid - 1

    return last


class TestGen:
    VAL_MAX = 5 * 10 ** 6 + 1

    def __init__(self):
        self.rand_gen = random.SystemRandom()
        self.primes = []
        self.composite = []
        self.sieve = [0 for i in range(TestGen.VAL_MAX + 1)]

        self.precalculate_sieve()

    def precalculate_sieve(self):
        self.sieve[0], self.sieve[1] = 1, 1
        self.composite.append(1)

        for i in range(2, TestGen.VAL_MAX + 1):
            if self.sieve[i] == 0:
                self.primes.append(i)
                for j in range(i * i, TestGen.VAL_MAX + 1, i):
                    self.sieve[j] = 1
            else:
                self.composite.append(i)

    def get_test(self, n, primes_cnt, queries_cnt):
        composite_cnt = n - primes_cnt
        max_k = primes_cnt * composite_cnt

        numbers = self.rand_gen.sample(self.primes, primes_cnt) + self.rand_gen.sample(self.composite, composite_cnt)
        self.rand_gen.shuffle(numbers)
        queries = [self.rand_gen.randint(1, max_k) for i in range(queries_cnt)]

        return numbers, queries

    def check(self, curr_nums, curr_queries, user_response):
        prime_nums = []
        composite_nums = []
        for num in curr_nums:
            if self.sieve[num] == 0:
                prime_nums.append(num)
            else:
                composite_nums.append(num)

        prime_nums.sort()
        composite_nums.sort()

        for query, response in zip(curr_queries, user_response):
            if solve(prime_nums, composite_nums, query) != response:
                return False

        return True

