#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <ostream>
#include <sched.h>
#include <type_traits>
#include <vector>
#include <cstdio>
#include <cstdint>
#include <utility>
#include <seccomp.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/syscall.h> 

#define INVALID_FLIP_STATUS -1
#define INVALID_COIN_STATUS -2
#define DOOM 666


// typedef struct position_t {
//     uint8_t x, y;
// } position;


typedef std::pair<int, int> position;


position generate_board(uint8_t *board) {
    FILE *rng = fopen("/dev/urandom", "r");
    uint64_t num;
    for (size_t i = 0; i < 8; i++) {
        for (size_t j = 0; j < 8; j++) {
            fread(&num, 1, sizeof(num), rng);
            board[8 * i + j] = num % 2;
        }
    }
    
    position magic_coin;
    fread(&num, 1, sizeof(num), rng);
    magic_coin.first = num % 8;
    fread(&num, 1, sizeof(num), rng);
    magic_coin.second = num % 8;
    fclose(rng);

    return magic_coin;
}


void print_board(std::vector<std::vector<uint8_t>> &board) {
    for (size_t i = 0; i < board.size(); i++) {
        for (size_t j = 0; j < board.size(); j++) {
            std::cout << board[i][j] << '\t';
        }
        std::cout << std::endl;
    }
}

void send_board(std::vector<std::vector<uint8_t>> &board, int fd) {
    for (size_t i = 0; i < board.size(); i++) {
        for (size_t j = 0; j < board.size(); j++) {
            write(fd, &board[i][j], sizeof(uint8_t));           
        }
    }
}

int main(int argc, char *argv[]) {
    shm_unlink(argv[1]);
    int fd = shm_open(argv[1], O_CREAT | O_RDWR, 0666);
    ftruncate(fd, 128);

    uint8_t *board  =  (uint8_t*) mmap(NULL, 64, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);


    int stage2_pipe[2];
    pipe(stage2_pipe);

    int magic_pipe[2];
    pipe(magic_pipe);

    // Generate the board and magic coin
    position magic_coin = generate_board(board);

    // Write magic coin to pipe
    write(magic_pipe[1], &magic_coin.first, sizeof(magic_coin.first));
    write(magic_pipe[1], &magic_coin.second, sizeof(magic_coin.second));

    int pid = fork();

    if (pid == 0) {
        // Run first bot
        dup2(stage2_pipe[1], 666);
        dup2(magic_pipe[0], 777);
        dup2(fd, 888);

        // Close all non-required fds 
        int fdlimit = (int)sysconf(_SC_OPEN_MAX);
        for (int i = 0; i < fdlimit; i++) {
            if (i != 666 && i != 777 && i != 888) {
                close(i);
            }
        }
        char *argv[] = {(char*) "./stage2", NULL};
        char *envp[] = {(char*) "SECCOMP_SYSCALL_ALLOW=write:read:exit_group", NULL};
        execve(
            "./stage2",
            argv,
            envp
        );
    } else {
        int wstatus;

        waitpid(pid, &wstatus, 0);
        lseek(fd, 0, SEEK_SET);

        if (WIFEXITED(wstatus)) {
            // Extra check, can be bypassed
            if (WEXITSTATUS(wstatus) != 0) {
                return DOOM;
            }

            int x = -1, y = -1;
            read(stage2_pipe[0], &x, sizeof(x));
            read(stage2_pipe[0], &y, sizeof(y));

            std::pair <int, int> flipped_coin = {x, y};


            if (flipped_coin.first >= 0 && flipped_coin.first < 8 && flipped_coin.second >= 0 && flipped_coin.second < 8) {
                board[8 * flipped_coin.first + flipped_coin.second] = 1 - board[8 * flipped_coin.first + flipped_coin.second];
            } else {
                return INVALID_FLIP_STATUS;
            }


            pid = fork();

            if (pid == 0) {
                // Run second bot
                dup2(fd, 888);
                dup2(stage2_pipe[1], 666);

                // Close all non-required fds 
                int fdlimit = (int)sysconf(_SC_OPEN_MAX);
                for (int i = 0; i < fdlimit; i++) {
                    if (i != 666 && i != 888) {
                        close(i);
                    }
                }

                char *argv[] = {(char*) "./stage3", NULL};
                char *envp[] = {(char*) "SECCOMP_SYSCALL_ALLOW=fstat:write:read:exit_group", NULL};
                execve(
                    "./stage3",
                    argv,
                    envp
                );
            } else {
                int wstatus;
                waitpid(pid, &wstatus, 0);

                if (WIFEXITED(wstatus)) {
                    if (WEXITSTATUS(wstatus) != 0) {
                        return DOOM;
                    }

                    std::pair <int, int> indicated_coin = {-1, -1};
                    read(stage2_pipe[0], &indicated_coin.first, sizeof(int));
                    read(stage2_pipe[0], &indicated_coin.second, sizeof(int));

                    if (magic_coin != indicated_coin) {
                        return INVALID_COIN_STATUS;
                    }

                    return 0;
                }
            }


        }
 
    }
    return DOOM;
}