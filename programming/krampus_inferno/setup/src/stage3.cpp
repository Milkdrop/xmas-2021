#include <stdint.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <utility>
#include <vector>
#include <iostream>



extern std::pair <int, int> bot1000000007(uint8_t board[8][8]);

uint8_t board[8][8];

int main() {
    read(888, board, 64);
    // for (int i = 0; i < 8; i++) {
    //     for (int j = 0; j < 8; j++) {
    //         printf("%d ", board [i][j]);
    //     }
    //     printf("\n");
    // }

    // fflush(stdout);

    std::pair <int, int> indicated_coin = bot1000000007(board);

    int x = indicated_coin.first, y = indicated_coin.second;

    // std::cout << x << " " << y << std::endl;

    write(666, &x, sizeof(int));
    write(666, &y, sizeof(int));
    return 0;
}
