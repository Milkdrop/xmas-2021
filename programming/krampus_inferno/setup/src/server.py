#!/usr/bin/env python -u
import time
import uuid
import sys
import os
import subprocess
import re
from time import sleep

player_uuid = ''

COMPILE_TIMEOUT = 2
RUN_TIMEOUT = 10
INCLUDES = '#include<cstdint>\n#include <utility>\n'

def read_code(bot_id):
    code = ''
    while True:
        line = input()
        if (line == ''):
            break
        else:
            code += line + '\n'

    pattern = re.compile('.*__|#.*')
    if (re.search(pattern, code)):
        print('You can\'t use your nasty words here!')
        return -1
    code = INCLUDES + code
    with open('bot{}.cpp'.format(bot_id), 'w') as f:
        f.write(code)
    return 0

def compile_code(bot_id):
    p = subprocess.Popen(['g++', '-Wall', '-Wextra', '-Werror', '-Wpedantic', '-c', 'bot{}.cpp'.format(bot_id)], stdout = subprocess.PIPE, stdin = subprocess.PIPE)
    p.wait(COMPILE_TIMEOUT)

    if (p.returncode != 0):
        print('Compile errors in bot {}'.format(bot_id))
        return -1
    print('Bot {} compiled successfully'.format(bot_id))
    return 0

def link_with_checker():
    linker_stage2 = subprocess.Popen(['g++', '-Wall', '-Wextra', '-Wpedantic', '../stage2.o', 'bot1.o', '-o', 'stage2', '-lseccomp'], stdout = subprocess.PIPE, stdin = subprocess.PIPE)
    linker_stage3 = subprocess.Popen(['g++', '-Wall', '-Wextra', '-Wpedantic', '../stage3.o', 'bot2.o', '-o', 'stage3', '-lseccomp'], stdout = subprocess.PIPE, stdin = subprocess.PIPE)

    linker_stage2.wait(COMPILE_TIMEOUT)
    linker_stage3.wait(COMPILE_TIMEOUT)

    patchelf_stage2 = subprocess.Popen(['patchelf', '--add-needed',  '../libsandbox.so', './stage2'], stdout = subprocess.PIPE, stdin = subprocess.PIPE)
    patchelf_stage3 = subprocess.Popen(['patchelf', '--add-needed',  '../libsandbox.so', './stage3'], stdout = subprocess.PIPE, stdin = subprocess.PIPE)

    patchelf_stage2.wait(COMPILE_TIMEOUT)
    patchelf_stage3.wait(COMPILE_TIMEOUT)


    if (linker_stage2.returncode != 0 or linker_stage3.returncode != 0 or patchelf_stage2.returncode != 0 or patchelf_stage3.returncode != 0):
        print('Couldn\'t link with checker')
        return -1
    print('Checker linked successfully')
    return 0


def run_checker(test_number):
    print('Running judgement {}/100'.format(test_number))
    INVALID_FLIP_STATUS = 256 - 1
    INVALID_COIN_STATUS = 256 - 2
    p = subprocess.Popen(['../checker', player_uuid], cwd = os.getcwd(), stdout = subprocess.PIPE, stdin = subprocess.PIPE)
    p.wait(RUN_TIMEOUT)
    if (p.returncode == INVALID_FLIP_STATUS):
        print('Bot 666013 performed an invalid flip!')
    elif (p.returncode == INVALID_COIN_STATUS):
        print('Bot 1000000007 showed me the wrong coin!')
    elif (p.returncode != 0):
        print('No shady business in my realm!')

    if (p.returncode == 0):
        print('You are good, for now...')
        return 0
    else:
        print('Death awaits you!')
        return -1


if __name__ == '__main__':
    player_uuid = str(uuid.uuid4())
    print('your uuid is {}'.format(player_uuid))
    os.mkdir('player_{}'.format(player_uuid))
    os.chdir('player_{}'.format(player_uuid))

    print('Elf Bot 666013: Where are we?')
    sleep(1)
    print('''Elf Bot 1000000007: I don't know, but my temperature sensors sense that this location is way hotter than Lapland.''')
    sleep(1)
    print('???: ID 666013, come in!')
    sleep(1)
    print('Elf Bot 1000000007: Someone ...or something has called you from behind that door, better not ignore it.')
    sleep(1)
    print('''Elf Bot 666013: Fine, if I die Santa will just need to generate a new prime number for the next's robot id.''')
    sleep(1)
    print('Krampus: Welcome to 1134, my domain. I have brought you here with the sole purpose of testing Dr. Kringle\'s, my rival\'s, intelligence.')
    sleep(1)
    print('**points to a chess board on the table full of shiny metal coins each showing heads or tails**')
    sleep(1)
    print('Take a look at this board. In a few seconds the LOGIC_CHIP14267 that Kringle programmed into you will fire off, making you choose a coin on this board.')
    sleep(1)
    print('The chosen coin will be flipped, then you will shutdown. Then I will summon 1000000007 and show him the new board.')
    sleep(1)
    print('His LOGIC_CHIP14268 will fire, making him choose some coin on this board.')
    sleep(1)
    print('If he successfully chooses this magic coin (points to some coin on the table), then stupid Kringle wins and you are free to go.')
    sleep(1)
    print('But if he chooses any other coin or either of you stumble in your decisions, then you will be stuck here forever!')
    sleep(1)
    print('And I will take Kringle\'s title as the perfect logician')
    sleep(1)

    print('LOGIC_CHIP14267 requires a function with the prototype std::pair <int, int> bot666013(uint8_t board[8][8], std::pair <int, int> magic_coin);')
    print('It returns the (x, y) coordinate (0 <= x, y < 8 of the coin that 666013 flips')
    print('LOGIC_CHIP14268 requires a function with the prototype std::pair <int, int> bot1000000007(uint8_t board[8][8]);')
    print('It returns the (x, y) coordinate (0 <= x, y < 8 of the coin that 1000000007 flips')
    print('Input code for the two chips (terminated by empty lines)')
    print('TIP: Krampus hates warnings (-Wall -Wextra -Wpedantic -Werror)')
    print('TIP: Krampus has also embraced simplicity. No further library includes are allowed (cstdint and utility are already included)')

    if (read_code(1) == -1):
        exit(-1)

    if (compile_code(1) == -1):
        exit(-1)

    if (read_code(2) == -1):
        exit(-1)

    if (compile_code(2) == -1):
        exit(-1)

    if (link_with_checker() == -1):
        exit(-1)

    for i in range(100):
        if (run_checker(i + 1) == -1):
            exit(-1)
    print('You have bested me')
    print(os.environ['FLAG'])


