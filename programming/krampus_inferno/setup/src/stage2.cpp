#include <stdint.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <utility>
#include <vector>
#include <iostream>

typedef std::pair<int, int> position;

extern std::pair <int, int> bot666013(uint8_t board[8][8], position magic_coin);

int main() {
    uint8_t board[8][8];
    position magic_coin;
    read(888, board, 64);
    read(777, &magic_coin.first, sizeof(int));
    read(777, &magic_coin.second, sizeof(int));
    // for (int i = 0; i < 8; i++) {
    //     for (int j = 0; j < 8; j++) {
    //         printf("%d ", board [i][j]);
    //     }
    //     printf("\n");
    // }

    // fflush(stdout);

    // std::cout << magic_coin.first << " " << magic_coin.second << std::endl;

    std::pair <int, int> flipped_coin = bot666013(board, magic_coin);

    int x = flipped_coin.first, y = flipped_coin.second;

    // std::cout << x << " " << y << std::endl;

    write(666, &x, sizeof(int));
    write(666, &y, sizeof(int));
    return 0;
}
