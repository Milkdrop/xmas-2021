from test_generator import TestGen
from constants import *
import threading
import time
import sys
import os


def die(timeout):
    time.sleep(timeout)
    print("Time is up! sorry...")
    os._exit(0)


def intro_prompt():
    print(f"You will have to solve {TESTS} tests in at most {MAX_TIME} seconds. Good luck!")
    sys.stdout.flush()


def step_prompt(step):
    print(f"Step: #{step} out of {TESTS}!")
    sys.stdout.flush()


def main():
    intro_prompt()

    thr = threading.Thread(target=die, args=(MAX_TIME,))
    thr.start()

    print("Loading drivers...")
    sys.stdout.flush()
    test_gen = TestGen()

    for i in range(TESTS):
        step_prompt(i + 1)

        curr_n = 10 + (i // 3) * N_MAX // (TESTS // 3 - 1)

        curr_nums, curr_g = test_gen.get_test(curr_n, i % 3)
        print(f"N = {curr_n}")
        print(f"numbers = {curr_nums}")
        sys.stdout.flush()

        user_commands = []
        try:
            total_updates = 0
            user_response_size = int(input("Number of riffs = "))
            for i in range(user_response_size):
                user_response = input(f"riffs #{i + 1} = ").replace(' ', '').replace('(', '').replace(')', '').split(',')
                user_response = [int(element) for element in user_response]

                if len(user_response) != 2:
                    raise Exception("Invalid length array")

                if user_response[0] < 0 or user_response[0] >= curr_n - 1 or user_response[1] < 0 or user_response[1] >= curr_n - 1:
                    raise Exception("Invalid indices")

                total_updates += abs(user_response[0] - user_response[1]) + 1

                if total_updates > 2 * curr_n:
                    raise Exception("Too many queries")

                user_commands.append(user_response)
        except:
            print("Invalid input, exiting!")
            sys.stdout.flush()
            os._exit(0)

        if test_gen.check(curr_nums, curr_g, user_commands):
            print("The response is correct!")
            sys.stdout.flush()
        else:
            print("The response is either invalid, or it doesn't use minimal operations!")
            sys.stdout.flush()

            os._exit(0)

    print(f"Well done! Here's your flag: {FLAG}")
    sys.stdout.flush()
    os._exit(0)


if __name__ == "__main__":
    main()
