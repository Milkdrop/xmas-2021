import random
from math import gcd
from constants import *
import math


def gcd_arr(arr):
    g = arr[0]
    for i in range(1, len(arr)):
        g = gcd(g, arr[i])
    return g


def multiply_p_q(numbers, p, q, x, y):
    if x > y:
        return
    for i in range(x, y):
        numbers[i] *= p * q
    numbers[y] *= q

    return numbers


class TestGen:

    def __init__(self):
        self.rand_gen = random.SystemRandom()
        self.primes = []
        self.sieve = [0 for i in range(VAL_MAX + 1)]
        self.fr = [0 for i in range(VAL_MAX + 1)]
        self.check_sieve = [[] for i in range(VAL_MAX + 1)]

        self.precalculate_sieve()
        self.precalculate_check_sieve()

    def precalculate_sieve(self):
        self.sieve[0], self.sieve[1] = 1, 1

        for i in range(2, VAL_MAX + 1):
            if self.sieve[i] == 0:
                self.primes.append(i)
                for j in range(i * i, VAL_MAX + 1, i):
                    self.sieve[j] = 1

    def precalculate_check_sieve(self):
        for i in range(2, VAL_MAX + 1):
            if len(self.check_sieve[i]) == 0:
                self.check_sieve[i] = [i]
                for j in range(2 * i, VAL_MAX + 1, i):
                    self.check_sieve[j].append(i)

    def get_test(self, n, test_type):
        l = 2
        if test_type == 0:
            l = int(math.log2(n))
        if test_type == 1:
            l = int(math.sqrt(n))

        numbers = [1 for i in range(n)]

        last_pos = -1
        length = l + self.rand_gen.randint(2, max(2, self.rand_gen.randint(2, max(2, int(math.log2(l)))))) - 1
        p, q = -1, -1

        while last_pos < n - 1:
            next_pos = min(n - 1, last_pos + 1 + length)
            if n - 1 - next_pos < l:
                next_pos = n - 1
            p = q

            act_max_val = min(int(math.sqrt(VAL_MAX / 4)), VAL_MAX // max(numbers[last_pos + 1: next_pos]))
            max_prime_idx = self.get_max_prime_index(act_max_val)
            if p == -1:
                p = self.primes[self.rand_gen.randint(0, max_prime_idx)]

            q = self.primes[self.rand_gen.randint(0, max_prime_idx)]
            while gcd(p, q) != 1:
                q = self.primes[self.rand_gen.randint(0, max_prime_idx)]

            multiply_p_q(numbers, p, q, last_pos + 1, next_pos)
            last_pos = next_pos
            length = l + self.rand_gen.randint(2, max(2, self.rand_gen.randint(2, max(2, int(math.log2(l)))))) - 1

        for i in range(n):
            act_max_val = VAL_MAX // numbers[i]
            if act_max_val < 2:
                continue
            max_prime_idx = self.get_max_prime_index(act_max_val)
            p = self.primes[self.rand_gen.randint(0, max_prime_idx)]
            numbers[i] *= p

        g = gcd_arr(numbers)

        if l == 2:
            x = n // 4 + self.rand_gen.randint(0, n // 2 - 1)
            max_prime_idx = self.get_max_prime_index(VAL_MAX // g)
            while gcd(numbers[x - 1], numbers[x]) != g:
                numbers[x] = g * self.primes[self.rand_gen.randint(0, max_prime_idx)]

        return numbers, g

    def check(self, nums, g, responses):
        updates = 0
        min_updates = self.get_min_updates(nums, g)
        for response in responses:
            x, y = response

            if x <= y:
                for i in range(x, y + 1):
                    updates += 1
                    act_g = gcd(nums[i], nums[i + 1])
                    nums[i] = act_g
                    nums[i + 1] = act_g
            else:
                for i in range(x, y - 1, -1):
                    updates += 1
                    act_g = gcd(nums[i], nums[i + 1])
                    nums[i] = act_g
                    nums[i + 1] = act_g

        if updates != min_updates:
            return False

        if sum(nums) != g * len(nums):
            return False

        return True

    def simulate(self, l, nums):
        response = -1
        cnt = 0
        for i in range(len(nums)):
            for divisor in self.check_sieve[nums[i]]:
                self.fr[divisor] += 1
                if self.fr[divisor] == l:
                    cnt += 1

            if i >= l:
                for divisor in self.check_sieve[nums[i - l]]:
                    self.fr[divisor] -= 1
                    if self.fr[divisor] == l - 1:
                        cnt -= 1

                if cnt == 0:
                    response = i - l + 1

        for i in range(len(nums) - l, len(nums)):
            for divisor in self.check_sieve[nums[i]]:
                self.fr[divisor] -= 1

        return response

    def get_min_len(self, nums):
        resp = -1
        lw, hi = 1, len(nums)
        while lw <= hi:
            mid = (lw + hi) // 2
            ans = self.simulate(mid, nums)
            if ans != -1:
                resp = mid
                hi = mid - 1
            else:
                lw = mid + 1

        return resp

    def get_min_updates(self, nums, g):
        nums = [num // g for num in nums]
        g = 1
        min_len = self.get_min_len(nums)
        if min_len >= 3:
            return len(nums) + min_len - 3
        else:
            g_cnt = 0
            pair_cnt = 0
            jump_next = False

            for i in range(len(nums)):
                if jump_next:
                    jump_next = False
                    continue

                if nums[i] == g:
                    g_cnt += 1
                elif i < len(nums) - 1 and nums[i] != g and nums[i + 1] != g and gcd(nums[i], nums[i + 1]) == g:
                    pair_cnt += 1
                    jump_next = True

            return len(nums) - g_cnt - pair_cnt

    def get_max_prime_index(self, x):
        if x < 2:
            return -1

        lw, hi, last = 0, len(self.primes) - 1, 0
        while lw <= hi:
            mid = (lw + hi) // 2
            if self.primes[mid] <= x:
                lw = mid + 1
                last = mid
            else:
                hi = mid - 1

        return last
